﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  DeleteMotionPathUndo.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using INV.Elearning.Core.Helper;
using INV.Elearning.Core.View;
using INV.Elearning.Core.ViewModel;
using System.Collections.Generic;

namespace INV.Elearning.Animations.UI
{
    public class DeleteMotionPathUndo : StepBase
    {
        public DeleteMotionPathUndo(object data)
        {
            Source = data;
        }

        public override void UndoExcute()
        {
            Global.BeginInit();
            if (Source is List<DeleteMotionPathUndoData> data)
            {
                foreach (DeleteMotionPathUndoData item in data)
                {
                    item.Undo();
                } 
            }
            Global.EndInit();
        }

        public override void RedoExcute()
        {
            Global.BeginInit();
            if (Source is List<DeleteMotionPathUndoData> data)
            {
                foreach (DeleteMotionPathUndoData item in data)
                {
                    item.Redo();
                }
            }
            Global.EndInit();
        }
    }

    public class DeleteMotionPathUndoData
    {
        public DeleteMotionPathUndoData(MotionPathObject motionPath, LayoutBase layer)
        {
            MotionPath = motionPath;
            Layer = layer;
        }

        public MotionPathObject MotionPath { get; set; }
        public LayoutBase Layer { get; set; }

        public void Undo()
        {
            if (MotionPath?.Owner is ObjectElement owner)
            {
                owner?.MotionPaths?.Add(MotionPath);
                if (Layer?.Children?.Contains(MotionPath) == false)
                {
                    Layer?.Children?.Add(MotionPath); 
                }
            }
        }

        public void Redo()
        {
            if (MotionPath?.Owner is ObjectElement owner)
            {
                owner?.MotionPaths?.Remove(MotionPath);
                Layer?.Children?.Remove(MotionPath);
            }
        }
    }
}
