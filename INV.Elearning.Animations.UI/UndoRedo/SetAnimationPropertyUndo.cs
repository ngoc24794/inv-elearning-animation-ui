﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  StepBase.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using INV.Elearning.Animations.UI.ViewModel;
using INV.Elearning.Core.Helper;
using INV.Elearning.Core.ViewModel;
using System.Collections.Generic;
using System.Windows;

namespace INV.Elearning.Animations.UI
{
    internal class SetAnimationPropertyUndo : StepBase
    {
        internal SetAnimationPropertyUndo(List<SetAnimationPropertyUndoData> data)
        {
            Source = data;
        }

        public override void UndoExcute()
        {
            Global.BeginInit();
            if (Source is List<SetAnimationPropertyUndoData> data)
            {
                foreach (SetAnimationPropertyUndoData item in data)
                {
                    item.SetOldValue();
                }
            }
            AnimationsViewModel.AnimationsProxy.OnPropertyChanged("ProxyAnimation");
            Global.EndInit();
        }

        public override void RedoExcute()
        {
            Global.BeginInit();
            if (Source is List<SetAnimationPropertyUndoData> data)
            {
                foreach (SetAnimationPropertyUndoData item in data)
                {
                    item.SetNewValue();
                }
            }
            AnimationsViewModel.AnimationsProxy.OnPropertyChanged("ProxyAnimation");
            Global.EndInit();
        }
    }

    internal class SetAnimationPropertyUndoData
    {
        public SetAnimationPropertyUndoData(DependencyObject source, DependencyProperty property, object oldValue, object newValue)
        {
            Source = source;
            Property = property;
            OldValue = oldValue;
            NewValue = newValue;
        }

        public DependencyObject Source { get; set; }
        public DependencyProperty Property { get; set; }
        public object OldValue { get; set; }
        public object NewValue { get; set; }

        public void SetOldValue()
        {
            Source?.SetValue(Property, OldValue);
        }

        public void SetNewValue()
        {
            Source?.SetValue(Property, NewValue);
        }
    }
}
