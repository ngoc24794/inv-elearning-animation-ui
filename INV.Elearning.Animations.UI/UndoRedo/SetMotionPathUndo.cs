﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  SetMotionPathUndo.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using INV.Elearning.Core.Helper;
using INV.Elearning.Core.View;
using INV.Elearning.Core.ViewModel;
using System.Collections.Generic;

namespace INV.Elearning.Animations.UI
{
    public class SetMotionPathUndo : StepBase
    {
        internal SetMotionPathUndo(List<SetMotionPathUndoData> data)
        {
            Source = data;
        }

        internal List<SetMotionPathUndoData> Data { get; set; }
        public override void UndoExcute()
        {
            Global.BeginInit();
            if (Source is List<SetMotionPathUndoData> data)
            {
                foreach (SetMotionPathUndoData item in data)
                {
                    item.Undo();
                }
            }
            Global.EndInit();
        }

        public override void RedoExcute()
        {
            Global.BeginInit();
            if (Source is List<SetMotionPathUndoData> data)
            {
                foreach (SetMotionPathUndoData item in data)
                {
                    item.Redo();
                }
            }
            Global.EndInit();
        }
    }

    internal class SetMotionPathUndoData
    {
        public SetMotionPathUndoData(ObjectElement source, LayoutBase layer, MotionPathObject motionPath)
        {
            Source = source;
            Layer = layer;
            MotionPath = motionPath;
        }

        public ObjectElement Source { get; set; }
        public LayoutBase Layer { get; set; }
        public MotionPathObject MotionPath { get; set; }

        public void Undo()
        {
            Source?.MotionPaths?.Remove(MotionPath);
            Layer?.Children?.Remove(MotionPath);
        }

        public void Redo()
        {
            Source?.MotionPaths?.Add(MotionPath);
            if (Layer?.Children?.Contains(MotionPath) == false)
            {
                Layer?.Children?.Add(MotionPath);
            }
        }
    }
}
