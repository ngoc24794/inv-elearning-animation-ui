﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  FileName.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using INV.Elearning.Animations.UI.ViewModel;
using INV.Elearning.Core.Helper;
using INV.Elearning.Core.ViewModel;
using System;
using System.Collections.Generic;

namespace INV.Elearning.Animations.UI
{
    public class DurationAnimationUndo : StepBase
    {
        public DurationAnimationUndo(object source)
        {
            Source = source;
        }

        public override void RedoExcute()
        {
            Global.BeginInit();
            if (Source is List<DurationAnimationUndoData> data)
            {
                foreach (DurationAnimationUndoData item in data)
                {
                    item.Redo();
                }
            }
            AnimationsViewModel.AnimationsProxy.OnPropertyChanged("DurationEntranceSelected");
            AnimationsViewModel.AnimationsProxy.OnPropertyChanged("DurationExitSelected");
            AnimationsViewModel.AnimationsProxy.OnPropertyChanged("DurationPathSelected");
            Global.EndInit();
        }

        public override void UndoExcute()
        {
            Global.BeginInit();
            if (Source is List<DurationAnimationUndoData> data)
            {
                foreach (DurationAnimationUndoData item in data)
                {
                    item.Undo();
                }
            }
            AnimationsViewModel.AnimationsProxy.OnPropertyChanged("DurationEntranceSelected");
            AnimationsViewModel.AnimationsProxy.OnPropertyChanged("DurationExitSelected");
            AnimationsViewModel.AnimationsProxy.OnPropertyChanged("DurationPathSelected");
            Global.EndInit();
        }
    }

    public class DurationAnimationUndoData
    {
        public DurationAnimationUndoData(IAnimation animation, TimeSpan oldValue, TimeSpan newValue)
        {
            Animation = animation;
            OldValue = oldValue;
            NewValue = newValue;
        }

        public IAnimation Animation { get; set; }
        public TimeSpan OldValue { get; set; }
        public TimeSpan NewValue { get; set; }

        public void Undo()
        {
            if (Animation != null)
            {
                Animation.Duration = OldValue;
            }
        }

        public void Redo()
        {
            if (Animation != null)
            {
                Animation.Duration = NewValue;
            }
        }
    }
}
