﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  SetPathEffectUndo.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using INV.Elearning.Animations.UI.ViewModel;
using INV.Elearning.Core.Helper;
using INV.Elearning.Core.View;
using INV.Elearning.Core.ViewModel;
using System.Collections.ObjectModel;

namespace INV.Elearning.Animations.UI
{
    public class SetPathEffectUndo : StepBase
    {
        public SetPathEffectUndo(object oldValue, object newValue, LayoutBase layer, ObservableCollection<ObjectElement> objects)
        {
            OldValue = oldValue;
            NewValue = newValue;
            Layer = layer;
            Objects = objects;
        }

        public LayoutBase Layer { get; set; }
        public ObservableCollection<ObjectElement> Objects { get; set; }

        public override void RedoExcute()
        {
            Global.BeginInit();
            if (NewValue != null && Layer != null && Objects != null)
            {
                AnimationsViewModel.SetPathEffect(NewValue, Layer, Objects, true);
            }
            Global.EndInit();
        }

        public override void UndoExcute()
        {
            Global.BeginInit();
            if (OldValue != null && Layer != null && Objects != null)
            {
                AnimationsViewModel.SetPathEffect(OldValue, Layer, Objects, true);
            }
            Global.EndInit();
        }
    }
}
