﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.Animations.UI.Model
{
    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: AnimationModel.cs
    // Description: Thuộc tính của hiệu ứng 
    // Develope by : Ta Khanh Thien
    // History:
    // 09/02/2018 : Create
    //---------------------------------------------------------------------------
    class AnimationModel
    {
        private string _animationName;

        public string AnimationName
        {
            get { return _animationName; }
            set { _animationName = value; }
        }

        private TimeSpan _duration;

        public TimeSpan Duration
        {
            get { return _duration; }
            set { _duration = value; }
        }


    }
}
