﻿using INV.Elearning.Core.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.Animations.UI.Model
{
    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: AnimationModel.cs
    // Description: Các loại thuộc tính của hiệu ứng 
    // Develope by : Ta Khanh Thien
    // History:
    // 09/02/2018 : Create
    //---------------------------------------------------------------------------

    public class AnimationObject : ObjectElement, IAnimationableObject
    {
        public AnimationObject(IAnimation animationEntrance, IAnimation animationExit, List<IMotionPathObject> animationPath)
        {
            EntranceAnimation = animationEntrance;
            ExitAnimation = animationExit;
            //  AnimationPath.RemoveAll;
            foreach (var item in animationPath)
            {

                MotionPaths.Add(item);
            }

        }
        public AnimationObject()
        {
        }
        private IAnimation _animationEntrance;

        public IAnimation EntranceAnimation
        {
            get { return _animationEntrance ?? new NoneAnimation(TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Spin.png", INV.Elearning.Animations.Enums.eAnimationType.Entrance, false); }
            set { _animationEntrance = value; }
        }
        private IAnimation _animationExit;

        public IAnimation ExitAnimation
        {
            get { return _animationExit ?? new NoneAnimation(TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Spin.png", INV.Elearning.Animations.Enums.eAnimationType.Exit, false); }
            set { _animationExit = value; }
        }
        private IList<IMotionPathObject> _animationPath;

        public IList<IMotionPathObject> MotionPaths
        {
            get { return _animationPath ?? (_animationPath = new List<IMotionPathObject>()); }
            set { _animationPath = value; }
        }
        private Animation _animationPathCheck;

        public Animation AnimationPath
        {
            get { return _animationPathCheck ?? new NoneAnimation(TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Spin.png", INV.Elearning.Animations.Enums.eAnimationType.Exit, false); }
            set { _animationPathCheck = value; }
        }

        public bool IsHasAnimation { get; set; }
    }
}
