﻿using INV.Elearning.Controls.Helpers;
using INV.Elearning.Core.ViewModel;
using System;
using System.Windows;
using System.Windows.Input;

namespace INV.Elearning.Animations.UI
{
    public class RenameMotionPathWindowViewModel : RootViewModel
    {
        private string _title;

        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                OnPropertyChanged("Title");
            }
        }

        private string _caption;

        public string Caption
        {
            get { return _caption; }
            set
            {
                _caption = value;
                OnPropertyChanged("Caption");
            }
        }

        private string _name;

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        public bool IsHittedOkButton { get; set; }

        private ICommand _okCommand;

        public ICommand OkCommand
        {
            get { return _okCommand ?? (_okCommand = new RelayCommand(x => OK(x), c => !string.IsNullOrEmpty(Name))); }
        }

        private void OK(object x)
        {
            if (x is Window window)
            {
                IsHittedOkButton = true;
                window.Close();
            }
        }
    }
}
