﻿using INV.Elearing.Controls.Shapes;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Animations.UI.Model;
using INV.Elearning.Core.Helper;
using INV.Elearning.Core.View;
using INV.Elearning.Core.ViewModel;
using INV.Elearning.Text.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
namespace INV.Elearning.Animations.UI.ViewModel
{
    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: AnimationsViewModel.cs
    // Description: Thực thi các điều khiển thay đổi hiệu ứng đối tượng
    // Develope by : Ta Khanh Thien
    // History:
    // 15/02/2018 : Create
    //---------------------------------------------------------------------------
    public class AnimationsViewModel : RootViewModel
    {
        private static AnimationsProxy _animationsProxy;
        /// <summary>
        /// Thuộc tính lưu trữ các giá trị ủy nhiệm (trung gian)
        /// </summary>
        public static AnimationsProxy AnimationsProxy
        {
            get { return _animationsProxy ?? (_animationsProxy = new AnimationsProxy()); }
        }
        #region Dữ liệu
        public static ObservableCollection<Animation> _dataAnimate;
        /// <summary>
        /// Dữ liệu Animation
        /// </summary>
        public static ObservableCollection<Animation> DataAnimate
        {
            get
            {
                {
                    var _randombar = new RandomBarsAnimation(TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/RandomBar.png", eAnimationType.Entrance, true);
                    if (_randombar.EffectOptions.Count() == 2)
                    {
                        _randombar.EffectOptions.Add(new SequenceOption());
                    }

                    var _zoom = new ZoomAnimation("Zoom", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Zoom.png", eAnimationType.Entrance, true);
                    if (_zoom.EffectOptions.Count() == 1)
                    {
                        _zoom.EffectOptions.Add(new SequenceOption());
                    }
                    return _dataAnimate ?? (_dataAnimate = new ObservableCollection<Animation>()
                {
                   new NoneAnimation("None", TimeSpan.FromSeconds(0),"/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/None.png" ),
                   new FadeAnimation("Fade", TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Fade.png",eAnimationType.Entrance,true),
                   new GrowAnimation("Grow", TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Grow.png" ,eAnimationType.Entrance,true),
                   new FlyAnimation("FlyIn", TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/FlyIn.png",eAnimationType.Entrance,true ),
                   new FloatAnimation("FloatIn", TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/FloatIn.png",eAnimationType.Entrance,true ),
                   new SplitAnimation("Split", TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Split.png" ,eAnimationType.Entrance,true),
                   new WipeAnimation("Wipe", TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Wipe.png" ,eAnimationType.Entrance,true),
                   new ShapeAnimation("Shape", TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Shape.png" ,eAnimationType.Entrance,true),
                   new WheelAnimation("Wheel", TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Wheel.png" ,eAnimationType.Entrance,true),
                   _randombar,
                   new SpinAnimation("Spin", TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Spin.png" ,eAnimationType.Entrance,true),
                   new SpinGrowAnimation("Spin&Grow", TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/SpinGrow.png" ,eAnimationType.Entrance,true),
                   new GrowSpinAnimation("Grow&Spin", TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/GrowSpin.png" ,eAnimationType.Entrance,true),
                   _zoom,
                   new SwivelAnimation("Swivel", TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Swivel.png" ,eAnimationType.Entrance,true),
                   new BounceAnimation("Bounce", TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Bounce.png" ,eAnimationType.Entrance,true),

                });
                }
            }
        }

        /// <summary>
        /// Vị trí của animation trong data
        /// </summary>
        /// <param name="data"></param>
        /// <param name="animation"></param>
        /// <returns></returns>

        public static int SetIndexAnimation(ObservableCollection<Animation> data, Animation animation)
        {
            if (animation is MultiAnimation) return -1;
            int index = data.IndexOf(data.First(x => x.Name == animation.Name));
            return index;
        }



        public static ObservableCollection<Animation> _dataAnimateExit;
        /// <summary>
        /// Dữ liệu Animation Exit
        /// </summary>
        public static ObservableCollection<Animation> DataAnimateExit
        {
            get
            {
                {
                    var _randombar = new RandomBarsAnimation(TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/ExitAnimations/RandomBars.png", eAnimationType.Exit, true);
                    if (_randombar.EffectOptions.Count() == 2)
                    {
                        _randombar.EffectOptions.Add(new SequenceOption());
                    }
                    var _zoom = new ZoomAnimation(TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/ExitAnimations/Zoom.png", eAnimationType.Exit, true);
                    if (_zoom.EffectOptions.Count() == 1)
                    {
                        _zoom.EffectOptions.Add(new SequenceOption());
                    }
                    return _dataAnimateExit ?? (_dataAnimateExit = new ObservableCollection<Animation>()
                        {
                           new NoneAnimation(TimeSpan.FromSeconds(0),"/INV.Elearning.Animations.UI;component/Images/Animations/ExitAnimations/None.png" ),
                           new FadeAnimation(TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/ExitAnimations/Wheel.png", eAnimationType.Exit ,true),
                           new GrowAnimation(TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/ExitAnimations/Shrink.png",eAnimationType.Exit,true ),
                           new FlyAnimation(TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/ExitAnimations/FlyOut.png" ,eAnimationType.Exit,true ),
                           new FloatAnimation(TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/ExitAnimations/FloatOut.png",eAnimationType.Exit ,true ),
                           new SplitAnimation(TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/ExitAnimations/Split.png",eAnimationType.Exit ,true ),
                           new WipeAnimation(TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/ExitAnimations/Wipe.png" ,eAnimationType.Exit,true ),
                           new ShapeAnimation(TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/ExitAnimations/Shape.png",eAnimationType.Exit ,true ),
                           new WheelAnimation(TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/ExitAnimations/Wheel.png" ,eAnimationType.Exit,true ),
                           _randombar,
                           new SpinAnimation(TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/ExitAnimations/Spin.png",eAnimationType.Exit ,true ),
                           new SpinGrowAnimation(TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/ExitAnimations/SpinShrink.png",eAnimationType.Exit,true  ),
                           new GrowSpinAnimation(TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/ExitAnimations/ShrinkTurn.png",eAnimationType.Exit ,true ),
                           _zoom,
                           new SwivelAnimation(TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/ExitAnimations/Swivel.png" ,eAnimationType.Exit,true ),
                           new BounceAnimation(TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/ExitAnimations/Bounce.png" ,eAnimationType.Exit,true ),

                        });
                }
            }
        }


        public static ObservableCollection<Animation> _dataAnimatePath;
        /// <summary>
        /// Dữ liệu Animation Path
        /// </summary>
        public static ObservableCollection<Animation> DataAnimatePath
        {
            get
            {
                return _dataAnimatePath ?? (_dataAnimatePath = new ObservableCollection<Animation>()
                {
                   new LinesAnimation(TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Lines.png" ),
                   new ArcsAnimation(TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Arcs.png" ),
                   new TurnsAnimation(TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Turns.png"),
                   new CircleAnimation("Circle",TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Circle.png"),
                   new SquareAnimation(TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Square.png" ),
                   new EqualTriangleAnimation("Equal Triangle",TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/EqualTriangle.png"),
                   new TrapezoidAnimation(TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Trapezoid.png" ),
                   new FreeformAnimation(TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Freeform.png" ),
                   new ScribbleAnimation(TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Scribble.png" ),
                   new CurveAnimation(TimeSpan.FromSeconds(1),"/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Curve.png" ),

                });
            }
        }

        public static ObservableCollection<Transiton> _dataTransition;
        /// <summary>
        /// Dữ liệu Transition
        /// </summary>
        public static ObservableCollection<Transiton> DataTransition
        {
            get
            {
                {
                    return _dataTransition ?? (_dataTransition = new ObservableCollection<Transiton>()
                {
                   new FadeTransition("Fade",TimeSpan.FromSeconds(2),"/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Lines.png" ),
                   new FadeTransition("Fade",TimeSpan.FromSeconds(2),"/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Lines.png" ),
                   new PushTransition("Push",TimeSpan.FromSeconds(2),"/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Arcs.png" ),
                   new SplitTransition("",TimeSpan.FromSeconds(2),"/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Turns.png"),
                   new RandomBarsTransition("",TimeSpan.FromSeconds(2),"/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Circle.png"),
                   new CircleTransition("",TimeSpan.FromSeconds(2),"/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Square.png" ),
                   new DiamondTransition("Equal Triangle",TimeSpan.FromSeconds(2),"/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/EqualTriangle.png"),
                   new PlusTransition("",TimeSpan.FromSeconds(2),"/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Trapezoid.png" ),
                   new InTransition("",TimeSpan.FromSeconds(2),"/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Freeform.png" ),
                   new OutTransition("",TimeSpan.FromSeconds(2),"/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Scribble.png" ),
                   new UnCoverTransition("",TimeSpan.FromSeconds(2),"/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Curve.png" ),
                   new CoverTransition("",TimeSpan.FromSeconds(2),"/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Lines.png" ),
                   new NewsFlashTransition("",TimeSpan.FromSeconds(2),"/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Arcs.png" ),
                   new DissolveTransition("",TimeSpan.FromSeconds(2),"/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Turns.png"),
                   new CheckerboardTransition("",TimeSpan.FromSeconds(2),"/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Circle.png"),
                   new BlindsTransition("",TimeSpan.FromSeconds(2),"/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Square.png" ),
                   new ClockTransition("Equal Triangle",TimeSpan.FromSeconds(2),"/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/EqualTriangle.png"),
                   new ZoomTransition("",TimeSpan.FromSeconds(2),"/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Trapezoid.png" ),


                });
                }

            }
        }


        #endregion

        #region Thực hiện các Command Animation

        private static ICommand _animationEntranceCommand;
        /// <summary>
        /// Lệnh thực thi thay đổi hiêu ứng đối tượng
        /// </summary>
        public static ICommand AnimationEntranceCommand
        {
            get
            {
                return _animationEntranceCommand ?? (_animationEntranceCommand = new RelayCommand(param => SetAnimationEntranceCommand(param)));
            }

        }
        /// <summary>
        /// Thực thi thay đổi hiêu ứng đối tượng
        /// </summary>
        /// <param name="para"></param>
        public static void SetAnimationEntranceCommand(Object para)
        {
            if (!(para is Animation)) return;
            //Thay đổi giá trị para về mặc định
            para = CloneAnimation.GetAnimation((para as Animation).Name, (para as Animation).IsSequence);
            GetCommandEntrance(para as Animation, eAnimationType.Entrance);
            var _testControl = (Application.Current as IAppGlobal).SelectedElements;

            int _countObject = 0;
            _countObject = _testControl.Count();
            //Gán giá trị animation cho đối tượng
            for (int i = 0; i < _countObject; i++)
            {
                if (_testControl.ElementAt(i) is MotionPathObject)
                {
                    (_testControl.ElementAt(i) as MotionPathObject).Owner.EntranceAnimation = para as Animation;

                }
                else if (_testControl.ElementAt(i) is ObjectElement)
                {
                    (_testControl.ElementAt(i) as ObjectElement).EntranceAnimation = para as Animation;

                    //Xoá ngôi sao
                    if (para is NoneAnimation)
                    {
                        if (AdornerLayer.GetAdornerLayer((_testControl.ElementAt(i) as ObjectElement)) is AdornerLayer layer)
                        {
                            List<Adorner> animationAdorners = new List<Adorner>();
                            Adorner[] adorners = layer.GetAdorners(_testControl.ElementAt(i) as ObjectElement);
                            if (adorners != null)
                                foreach (Adorner adorner in adorners)
                                {
                                    if (adorner is AnimationAdorner)
                                    {
                                        animationAdorners.Add(adorner);
                                    }
                                }
                            if (animationAdorners.Count > 0)
                            {
                                //---------------------------------------------------------------------------
                                // Xóa tất cả các 'ngôi sao hiệu ứng' 
                                //---------------------------------------------------------------------------
                                (_testControl.ElementAt(i) as ObjectElement).IsHasAnimation = false;                   // đặt lại cờ hiệu ứng

                                //foreach (Adorner adorner in animationAdorners)
                                //{
                                //    layer.Remove(adorner);
                                //}
                            }
                        }
                    }

                }
            }





            Animation _animationSelected = null;
            _animationSelected = AnimationsViewModel.SetAnimationSelected(para as Animation, _animationSelected, AnimationsViewModel.DataAnimate);
            //Gán giá trị animation đang được lựa chọn
            AnimationsViewModel.AnimationsProxy._animation = _animationSelected;
            AnimationsViewModel.AnimationsProxy.AnimationSelected = _animationSelected;
            if (para is NoneAnimation)
            {
                AnimationsViewModel.AnimationsProxy.DurationEntranceSelected = 0;
            }
            else
            {
                AnimationsViewModel.AnimationsProxy.DurationEntranceSelected = 1;
            }


        }

        private static ICommand _animationExitCommand;
        /// <summary>
        /// Lệnh thực thi thay đổi hiêu ứng Exit 
        /// </summary>
        public static ICommand AnimationExitCommand
        {
            get
            {
                return _animationExitCommand ?? (_animationExitCommand = new RelayCommand(param => SetAnimationExitCommand(param)));
            }

        }
        /// <summary>
        /// Thực thi thay đổi hiêu ứng Exit
        /// </summary>
        /// <param name="para"></param>
        public static void SetAnimationExitCommand(Object para)
        {
            if (!(para is Animation)) return;
            var _testControl = (Application.Current as IAppGlobal).SelectedElements;
            //Thay đổi giá trị para về mặc định
            para = CloneAnimation.GetAnimation((para as Animation).Name, (para as Animation).IsSequence);
            if (para is Animation)
            {
                (para as Animation).AnimationType = eAnimationType.Exit;
            }
            GetCommandEntrance(para as Animation, eAnimationType.Exit);

            //

            int _countObject = 0;
            _countObject = _testControl.Count();
            //Gán giá trị animation cho đối tượng
            for (int i = 0; i < _countObject; i++)
            {
                if (_testControl.ElementAt(i) is MotionPathObject)
                {
                    (_testControl.ElementAt(i) as MotionPathObject).Owner.ExitAnimation = para as Animation;
                    if ((_testControl.ElementAt(i) as MotionPathObject).Owner is TextEditor)
                    {
                        (_testControl.ElementAt(i) as MotionPathObject).Owner.ExitAnimation.IsSequence = true;
                    }
                }
                else if (_testControl.ElementAt(i) is ObjectElement)
                {
                    //  (_testControl.ElementAt(i) as TestControl).AnimationObject = new AnimationObject();
                    //  if ((_testControl.ElementAt(i) as TestControl).AnimationObject.AnimationEntrance.Name == (para as Animation).Name) continue;
                    (_testControl.ElementAt(i) as ObjectElement).ExitAnimation = para as Animation;
                    if (_testControl.ElementAt(i) is TextEditor)
                    {
                        (_testControl.ElementAt(i) as ObjectElement).ExitAnimation.IsSequence = true;
                    }
                    //Xoá ngôi sao
                    if (para is NoneAnimation)
                    {
                        if (AdornerLayer.GetAdornerLayer((_testControl.ElementAt(i) as ObjectElement)) is AdornerLayer layer)
                        {
                            List<Adorner> animationAdorners = new List<Adorner>();
                            Adorner[] adorners = layer.GetAdorners(_testControl.ElementAt(i) as ObjectElement);
                            if (adorners != null)
                                foreach (Adorner adorner in adorners)
                                {
                                    if (adorner is AnimationAdorner)
                                    {
                                        animationAdorners.Add(adorner);
                                    }
                                }
                            if (animationAdorners.Count > 0)
                            {
                                //---------------------------------------------------------------------------
                                // Xóa tất cả các 'ngôi sao hiệu ứng' 
                                //---------------------------------------------------------------------------
                                (_testControl.ElementAt(i) as ObjectElement).IsHasAnimation = false;                   // đặt lại cờ hiệu ứng

                                //foreach (Adorner adorner in animationAdorners)
                                //{
                                //    layer.Remove(adorner);
                                //}
                            }
                        }
                    }

                }
            }


            Animation _animationSelected = null;
            _animationSelected = AnimationsViewModel.SetAnimationSelected(para as Animation, _animationSelected, AnimationsViewModel.DataAnimateExit);
            //Gán giá trị animation đang được lựa chọn
            AnimationsViewModel.AnimationsProxy._animationExit = _animationSelected;
            AnimationsViewModel.AnimationsProxy.AnimationExitSelected = _animationSelected;
            if (para is NoneAnimation)
            {
                AnimationsViewModel.AnimationsProxy.DurationExitSelected = 0;
            }
            else
            {
                AnimationsViewModel.AnimationsProxy.DurationExitSelected = 1;
            }



        }

        private static ICommand _animationPathCommand;
        /// <summary>
        /// Lệnh thực thi thay đổi hiêu ứng Path 
        /// </summary>
        public static ICommand AnimationPathCommand
        {
            get
            {
                return _animationPathCommand ?? (_animationPathCommand = new RelayCommand(param => SetAnimationPathCommand(param)));
            }

        }
        /// <summary>
        /// Thực thi thay đổi hiêu ứng Path
        /// </summary>
        /// <param name="para"></param>
        public static void SetAnimationPathCommand(Object para)
        {

            if (!(para is Animation)) return;
            //Thay đổi giá trị para về mặc định
            para = CloneAnimation.GetAnimation((para as Animation).Name, (para as Animation).IsSequence);
            GetCommandMontionPath(para as Animation);



            ////Danh sách các đối tượng đang được chọn
            //var _testControl = (Application.Current as IAppGlobal).SelectedElements;

            ////Số lượng các đối tượng đang được chọn
            //int _countObject = 0;
            //_countObject = _testControl.Count();

            ////Gán giá trị animation cho đối tượng
            //for (int i = 0; i < _countObject; i++)
            //{
            //    if (_testControl.ElementAt(i) is TestControl)
            //    {
            //        Animation a = para as Animation;
            //        (_testControl.ElementAt(i) as TestControl).MotionPaths.Add(para as Animation);
            //    }
            //}


            var _testControl = (Application.Current as IAppGlobal).SelectedElements;
            Animation _animationSelected = null;
            _animationSelected = AnimationsViewModel.SetAnimationSelected(para as Animation, _animationSelected, AnimationsViewModel.DataAnimatePath);
            //Gán giá trị animation đang được lựa chọn
            AnimationsViewModel.AnimationsProxy._animationPath = _animationSelected;
            AnimationsViewModel.AnimationsProxy.AnimationPathSelected = _animationSelected;



        }

        #endregion

        #region Thực hiện các Command EffectOption
        private static ICommand _entranceEffectCommand;
        /// <summary>
        /// Lệnh thực thi thay đổi hiêu ứng đối tượng
        /// </summary>
        public static ICommand EntranceEffectCommand
        {
            get
            {
                return _entranceEffectCommand ?? (_entranceEffectCommand = new RelayCommand(param => SetEntranceEffectCommand(param)));
            }

        }
        /// <summary>
        /// Thực thi thay đổi hiêu ứng đối tượng
        /// </summary>
        /// <param name="para"></param>
        public static void SetEntranceEffectCommand(Object para)
        {
            //if (!(para is Animation)) return;
            var _testControl = (Application.Current as IAppGlobal).SelectedElements;
            //
            int _countObject = 0;
            _countObject = _testControl.Count();
            List<SetAnimationPropertyUndoData> undoData = new List<SetAnimationPropertyUndoData>();
            //Gán giá trị animation cho đối tượng
            for (int i = 0; i < _countObject; i++)
            {
                if (_testControl.ElementAt(i) is MotionPathObject)
                {

                    Animation _animation = CloneAnimation.GetAnimation((AnimationsProxy.ProxyAnimation as Animation).Name, (AnimationsProxy.ProxyAnimation as Animation).IsSequence);
                    CloneAnimation.CopyAnimation(_animation, AnimationsProxy.ProxyAnimation as Animation);
                    foreach (var item in _animation.EffectOptions)
                    {
                        if (item.GroupName == (para as EffectOption).GroupName)
                        {
                            if (item.Values[0] is EffectOptionAdapter)
                            {
                                for (int j = 0; j < (item.Values[0] as EffectOptionAdapter).Values.Count; j++)
                                {
                                    if (((item.Values[0] as EffectOptionAdapter).Values[j] as EffectOption).Value == (para as EffectOption).Value)
                                        (item.Values[0] as EffectOptionAdapter).Values[j].IsSelected = true;
                                    else (item.Values[0] as EffectOptionAdapter).Values[j].IsSelected = false;
                                }
                            }
                            else
                            {
                                for (int j = 0; j < item.Values.Count; j++)
                                {
                                    if (item.Values[j].Name == (para as EffectOption).Name)
                                    {
                                        item.Values[j].IsSelected = true;
                                    }
                                    else item.Values[j].IsSelected = false;
                                    //  item.Values[j].IsSelected = (para as EffectOptionGroup).Values[j].IsSelected;
                                }
                            }
                        }
                    }

                    if ((_testControl.ElementAt(i) as MotionPathObject).Owner is ObjectElement objectElement)
                    {
                        undoData.Add(new SetAnimationPropertyUndoData(objectElement, ObjectElement.EntranceAnimationProperty, objectElement.EntranceAnimation, _animation));
                    }

                    (_testControl.ElementAt(i) as MotionPathObject).Owner.EntranceAnimation = _animation;
                }
                else if (_testControl.ElementAt(i) is ObjectElement)
                {
                    //Animation _animation = CloneAnimation.GetAnimation((para as Animation).Name);
                    // CloneAnimation.CopyAnimation(_animation, para as Animation);

                    Animation _animation = CloneAnimation.GetAnimation((AnimationsProxy.ProxyAnimation as Animation).Name, (AnimationsProxy.ProxyAnimation as Animation).IsSequence);
                    CloneAnimation.CopyAnimation(_animation, AnimationsProxy.ProxyAnimation as Animation);
                    foreach (var item in _animation.EffectOptions)
                    {
                        if (item.GroupName == (para as EffectOption).GroupName)
                        {
                            if (item.Values[0] is EffectOptionAdapter)
                            {
                                for (int j = 0; j < (item.Values[0] as EffectOptionAdapter).Values.Count; j++)
                                {
                                    if (((item.Values[0] as EffectOptionAdapter).Values[j] as EffectOption).Value == (para as EffectOption).Value)
                                        (item.Values[0] as EffectOptionAdapter).Values[j].IsSelected = true;
                                    else (item.Values[0] as EffectOptionAdapter).Values[j].IsSelected = false;
                                }
                            }
                            else
                            {
                                for (int j = 0; j < item.Values.Count; j++)
                                {
                                    if (item.Values[j].Name == (para as EffectOption).Name)
                                    {
                                        item.Values[j].IsSelected = true;
                                    }
                                    else item.Values[j].IsSelected = false;
                                    //  item.Values[j].IsSelected = (para as EffectOptionGroup).Values[j].IsSelected;
                                }
                            }
                        }
                    }
                    if (_testControl.ElementAt(i) is ObjectElement objectElement)
                    {
                        undoData.Add(new SetAnimationPropertyUndoData(objectElement, ObjectElement.EntranceAnimationProperty, objectElement.EntranceAnimation, _animation));
                    }
                   (_testControl.ElementAt(i) as ObjectElement).EntranceAnimation = _animation;
                    //  (_testControl.ElementAt(i) as TestControl).AnimationEntrance.EffectOptions.Add((IEffectOptionGroup)para);


                    //(_testControl.ElementAt(i) as TestControl).AnimationEntrance=new FadeAnimation("Fade", TimeSpan.FromSeconds(2), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Fade.png");
                }
            }

            Global.PushUndo(new SetAnimationPropertyUndo(undoData));

        }

        private static ICommand _exitEffectCommand;
        /// <summary>
        /// Lệnh thực thi thay đổi hiêu ứng đối tượng Exit
        /// </summary>
        public static ICommand ExitEffectCommand
        {
            get
            {
                return _exitEffectCommand ?? (_exitEffectCommand = new RelayCommand(param => SetExitEffectCommand(param)));
            }

        }
        /// <summary>
        /// Thực thi thay đổi hiêu ứng đối tượng Exit
        /// </summary>
        /// <param name="para"></param>
        public static void SetExitEffectCommand(Object para)
        {
            //if (!(para is Animation)) return;
            var _testControl = (Application.Current as IAppGlobal).SelectedElements;
            //
            List<SetAnimationPropertyUndoData> dataUndo = new List<SetAnimationPropertyUndoData>();

            int _countObject = 0;
            _countObject = _testControl.Count();
            //Gán giá trị animation cho đối tượng
            for (int i = 0; i < _countObject; i++)
            {
                if (_testControl.ElementAt(i) is MotionPathObject)
                {
                    Animation _animation = CloneAnimation.GetAnimation((AnimationsProxy.ProxyAnimationExit as Animation).Name, (AnimationsProxy.ProxyAnimationExit as Animation).IsSequence);
                    CloneAnimation.CopyAnimation(_animation, AnimationsProxy.ProxyAnimationExit as Animation);
                    foreach (var item in _animation.EffectOptions)
                    {
                        if (item.GroupName == (para as EffectOption).GroupName)
                        {
                            if (item.Values[0] is EffectOptionAdapter)
                            {
                                for (int j = 0; j < (item.Values[0] as EffectOptionAdapter).Values.Count; j++)
                                {
                                    if (((item.Values[0] as EffectOptionAdapter).Values[j] as EffectOption).Value == (para as EffectOption).Value)
                                        (item.Values[0] as EffectOptionAdapter).Values[j].IsSelected = true;
                                    else (item.Values[0] as EffectOptionAdapter).Values[j].IsSelected = false;
                                }
                            }
                            else
                            {
                                for (int j = 0; j < item.Values.Count; j++)
                                {
                                    if (item.Values[j].Name == (para as EffectOption).Name)
                                    {
                                        item.Values[j].IsSelected = true;
                                    }
                                    else item.Values[j].IsSelected = false;
                                    //  item.Values[j].IsSelected = (para as EffectOptionGroup).Values[j].IsSelected;
                                }
                            }
                        }
                    }
                    if ((_testControl.ElementAt(i) as MotionPathObject).Owner is ObjectElement objectElement)
                    {
                        dataUndo.Add(new SetAnimationPropertyUndoData(objectElement, ObjectElement.ExitAnimationProperty, objectElement.ExitAnimation, _animation));
                    }
                    (_testControl.ElementAt(i) as MotionPathObject).Owner.ExitAnimation = _animation;

                }
                else if (_testControl.ElementAt(i) is ObjectElement)
                {

                    Animation _animation = CloneAnimation.GetAnimation((AnimationsProxy.ProxyAnimationExit as Animation).Name, (AnimationsProxy.ProxyAnimationExit as Animation).IsSequence);
                    CloneAnimation.CopyAnimation(_animation, AnimationsProxy.ProxyAnimationExit as Animation);
                    foreach (var item in _animation.EffectOptions)
                    {
                        if (item.GroupName == (para as EffectOption).GroupName || (item.GroupName == "Enter" && (para as EffectOption).GroupName == "Exit"))
                        {
                            if (item.Values[0] is EffectOptionAdapter)
                            {
                                for (int j = 0; j < (item.Values[0] as EffectOptionAdapter).Values.Count; j++)
                                {
                                    if (((item.Values[0] as EffectOptionAdapter).Values[j] as EffectOption).Value == (para as EffectOption).Value)
                                        (item.Values[0] as EffectOptionAdapter).Values[j].IsSelected = true;
                                    else (item.Values[0] as EffectOptionAdapter).Values[j].IsSelected = false;
                                }
                            }
                            else
                            {
                                for (int j = 0; j < item.Values.Count; j++)
                                {
                                    if (item.Values[j].Name == (para as EffectOption).Name)
                                    {
                                        item.Values[j].IsSelected = true;
                                    }
                                    else item.Values[j].IsSelected = false;
                                    //  item.Values[j].IsSelected = (para as EffectOptionGroup).Values[j].IsSelected;
                                }
                            }
                        }


                    }
                    if ((_testControl.ElementAt(i) is ObjectElement objectElement))
                    {
                        dataUndo.Add(new SetAnimationPropertyUndoData(objectElement, ObjectElement.ExitAnimationProperty, objectElement.ExitAnimation, _animation));
                    }
                   (_testControl.ElementAt(i) as ObjectElement).ExitAnimation = _animation;

                    //  (_testControl.ElementAt(i) as TestControl).AnimationEntrance.EffectOptions.Add((IEffectOptionGroup)para);

                    //(_testControl.ElementAt(i) as TestControl).AnimationEntrance=new FadeAnimation("Fade", TimeSpan.FromSeconds(2), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Fade.png");
                }
            }

            Global.PushUndo(new SetAnimationPropertyUndo(dataUndo));
        }


        private static ICommand _pathEffectCommand;
        /// <summary>
        /// Lệnh thực thi thay đổi EffectOption của MotionPath
        /// </summary>
        public static ICommand PathEffectCommand
        {
            get
            {
                return _pathEffectCommand ?? (_pathEffectCommand = new RelayCommand(param => SetPathEffectCommand(param)));
            }

        }
        /// <summary>
        /// Thực thi thay đổi EffectOption của MotionPath
        /// </summary>
        /// <param name="para"></param>
        public static void SetPathEffectCommand(Object para)
        {

            if (para is EffectOption option && AnimationsViewModel.AnimationsProxy.AnimationPathSelected.EffectOptions is List<IEffectOptionGroup> options)
            {
                object oldValue = null;
                bool check = true;
                foreach (IEffectOptionGroup group in options)
                {
                    check = false;
                    if ((group.GroupName == (para as EffectOption).GroupName) || (group.GroupName == "Easing" && ((para as EffectOption).GroupName == "Speed" || (para as EffectOption).GroupName == "DirectionEasingPath")))
                    {
                        if (group.Values[0] is EffectOptionAdapter)
                        {
                            foreach (IEffectOption opt in group.Values)
                            {
                                //Thay đổi giá trị IsSelected của các EffectOptions
                                if (opt is EffectOptionAdapter)
                                {
                                    if (opt.Name == (para as EffectOption).GroupName)
                                    {
                                        foreach (IEffectOption effectOption in (opt as EffectOptionAdapter).Values)
                                        {
                                            if (effectOption.IsSelected)
                                            {
                                                oldValue = effectOption;
                                                break;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (opt.IsSelected)
                                    {
                                        oldValue = opt;
                                    }
                                }
                            }
                        }
                        else
                        {
                            foreach (IEffectOption opt in group.Values)
                            {
                                if ((opt.IsSelected && group.GroupName != "Path") || (group.GroupName == "Path") && opt.Name == (para as EffectOption).Name)
                                {
                                    oldValue = opt;
                                    break;
                                }
                            }
                        }
                    }
                }
                ObservableCollection<ObjectElement> objects = new ObservableCollection<ObjectElement>();
                foreach (ObjectElement item in (Application.Current as IAppGlobal).SelectedElements)
                {
                    objects.Add(item);
                }
                Global.PushUndo(new SetPathEffectUndo(oldValue, para, (Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout, objects));
            }
            SetPathEffect(para, (Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout, (Application.Current as IAppGlobal).SelectedElements);
        }

        public static void SetPathEffect(Object para, LayoutBase layer, ObservableCollection<ObjectElement> objects, bool isShouldUndo = false)
        {
            //#region Ngọc
            ////---------------------------------------------------------------------------
            //// Lấy option được chọn
            ////---------------------------------------------------------------------------
            //EffectOption option = para as EffectOption;
            //eObjectAnimationOption value = (eObjectAnimationOption)Enum.Parse(typeof(eObjectAnimationOption), option.Value);
            //if (option.GroupName.ToUpper() == "Direction".ToUpper())
            //{
            //    GenerationAnimationViewModel.ObjectsToRemove = new List<ObjectElement>();
            //    GenerationAnimationViewModel.ObjectsToAdd = new List<ObjectElement>();
            //    GenerationAnimationViewModel.RememberSelectedObject = new List<ObjectElement>();

            //    //---------------------------------------------------------------------------
            //    // Duyệt các đối tượng trên Canvas
            //    //---------------------------------------------------------------------------
            //    foreach (var item in layer.Elements)
            //    {
            //        if (item is IObjectElement)
            //        {
            //            // chỉ xét đối tượng được chọn
            //            if ((item as IObjectElement).IsSelected)
            //                if (item is IAnimationableObject obj)                                   // đối tượng là IAnimationableObject
            //                {
            //                    foreach (IMotionPathObject motionPath in obj.MotionPaths)
            //                    {
            //                        SetPathOption(motionPath as MotionPathObject, option, value);
            //                        if (motionPath is ObjectElement element)
            //                        {
            //                            element.IsSelected = false;
            //                            GenerationAnimationViewModel.RememberSelectedObject?.Add(element);
            //                        }
            //                    }
            //                }
            //        }
            //    }

            //    foreach (var item in layer.Children)
            //    {
            //        if (item is MotionPathObject pathObj && pathObj.IsSelected)                             // đối tượng là IMotionPathObject
            //        {
            //            SetPathOption(pathObj, option, value);
            //        }
            //    }


            //    //---------------------------------------------------------------------------
            //    // Thêm MotionPathObject mới vào Canvas
            //    //---------------------------------------------------------------------------
            //    if (GenerationAnimationViewModel.ObjectsToAdd != null)
            //    {
            //        foreach (var item in GenerationAnimationViewModel.ObjectsToAdd)
            //        {
            //            if (item is MotionPathObject pathObj)
            //            {
            //                pathObj.Owner.MotionPaths.Add(pathObj);
            //                (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Children.Add(pathObj);
            //            }

            //        }
            //        GenerationAnimationViewModel.ObjectsToAdd.Clear();
            //    }


            //    //---------------------------------------------------------------------------
            //    // Xóa MotionPathObject cũ khỏi Canvas
            //    //---------------------------------------------------------------------------
            //    if (GenerationAnimationViewModel.ObjectsToRemove != null)
            //    {
            //        foreach (var item in GenerationAnimationViewModel.ObjectsToRemove)
            //        {
            //            if (item is MotionPathObject pathObj)
            //            {
            //                pathObj.Owner.MotionPaths.Remove(pathObj);
            //                GenerationAnimationViewModel.SetShouldRemove(pathObj, true);
            //                (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Children.Remove(pathObj);
            //            }
            //        }
            //        GenerationAnimationViewModel.ObjectsToRemove.Clear();
            //    }


            //    //---------------------------------------------------------------------------
            //    // Chọn lại các đối tượng đã được lưu
            //    //---------------------------------------------------------------------------
            //    if (GenerationAnimationViewModel.RememberSelectedObject != null)
            //    {
            //        foreach (var item in GenerationAnimationViewModel.RememberSelectedObject)
            //        {
            //            if (item is IObjectElement)
            //                (item as IObjectElement).IsSelected = true;
            //        }

            //        GenerationAnimationViewModel.RememberSelectedObject.Clear();
            //    }
            //}
            //#endregion

            //Danh sách các đối tượng đang được chọn
            var _testControl = objects;
            int _countObject = 0;
            _countObject = _testControl.Count();

            //Gán giá trị animation cho đối tượng
            for (int i = 0; i < _countObject; i++)
            {
                if (_testControl.ElementAt(i) is MotionPathObject)
                {
                    Animation _animation = CloneAnimation.GetAnimation((AnimationsProxy.ProxyAnimationPath as Animation).Name, (AnimationsProxy.ProxyAnimationPath as Animation).IsSequence);
                    CloneAnimation.CopyAnimation(_animation, AnimationsProxy.ProxyAnimationPath as Animation);
                    // chạy undo
                    if (isShouldUndo)
                    {
                        if (_testControl.ElementAt(i) is MotionPathObject motionPath)
                        {
                            _animation = motionPath.Animation as Animation;
                        }
                    }
                    bool check = true;
                    if (_animation?.EffectOptions != null)
                    {
                        foreach (var item in _animation.EffectOptions)
                        {
                            if ((item.GroupName == (para as EffectOption).GroupName) || (item.GroupName == "Easing" && ((para as EffectOption).GroupName == "Speed" || (para as EffectOption).GroupName == "DirectionEasingPath")))
                            {
                                check = false;
                                if (item.Values[0] is EffectOptionAdapter)
                                {
                                    foreach (var effectOptionAdapter in item.Values)
                                    {
                                        //Thay đổi giá trị IsSelected của các EffectOptions
                                        if (effectOptionAdapter is EffectOptionAdapter)
                                        {


                                            if (effectOptionAdapter.Name == (para as EffectOption).GroupName)
                                            {



                                                foreach (var effectOption in (effectOptionAdapter as EffectOptionAdapter).Values)
                                                {
                                                    if (effectOption is EffectOption && (effectOption as EffectOption).GroupName != (para as EffectOption).GroupName) break;
                                                    if ((effectOption as EffectOption).Name == (para as EffectOption).Name)
                                                    {
                                                        (effectOption as EffectOption).IsSelected = true;
                                                    }
                                                    else
                                                    {
                                                        (effectOption as EffectOption).IsSelected = false;
                                                    }
                                                }
                                            }
                                            if ((para as EffectOption).GroupName == "DirectionEasingPath" && (para as EffectOption).Name == "None" && effectOptionAdapter.Name == "Speed")
                                            {
                                                foreach (var effectOption in (effectOptionAdapter as EffectOptionAdapter).Values)
                                                {
                                                    (effectOption as EffectOption).IsSelected = false;
                                                }
                                            }
                                        }

                                    }

                                }
                                else
                                {
                                    //Thay đổi giá trị IsSelected của các EffectOptions
                                    if (item.GroupName == "Path")
                                    {
                                        for (int j = 0; j < item.Values.Count; j++)
                                        {
                                            if (item.Values[j].Name == (para as EffectOption).Name)
                                            {
                                                item.Values[j].IsSelected = !item.Values[j].IsSelected;
                                            }

                                        }
                                    }
                                    else
                                    {
                                        for (int j = 0; j < item.Values.Count; j++)
                                        {
                                            if (item.Values[j].Name == (para as EffectOption).Name)
                                            {
                                                item.Values[j].IsSelected = true;
                                            }
                                            else item.Values[j].IsSelected = false;
                                        }
                                    }

                                }
                            }


                        }
                    }
                    Animation _animationPath = null;

                    //   for (int j = 0; j < (_testControl.ElementAt(i) as MotionPathObject).MotionPaths.Count; j++)
                    {
                        _animationPath = CloneAnimation.GetAnimation((AnimationsProxy.ProxyAnimationPath as Animation).Name, (AnimationsProxy.ProxyAnimationPath as Animation).IsSequence);
                        CloneAnimation.CopyAnimation(_animationPath, _animation);
                        //    (_testControl.ElementAt(i) as MotionPathObject).Animation = _testControl.ElementAt(i) as MotionPathObject;
                        CloneAnimation.CopyAnimation((_testControl.ElementAt(i) as MotionPathObject).Animation as Animation, _animationPath);

                        //  (_testControl.ElementAt(i) as TestControl).MotionPaths[i].Animation = _animationPath 
                    }

                    MotionPathObject motionPathObject = _testControl.ElementAt(i) as MotionPathObject;
                    if (motionPathObject != null)
                    {
                        (motionPathObject?.Content as ShapeBase).InvalidateVisual();
                    }
                }
                else if (_testControl.ElementAt(i) is ObjectElement)
                {
                    Animation _animation = CloneAnimation.GetAnimation((AnimationsProxy.ProxyAnimationPath as Animation).Name, (AnimationsProxy.ProxyAnimationPath as Animation).IsSequence);
                    CloneAnimation.CopyAnimation(_animation, AnimationsProxy.ProxyAnimationPath as Animation);
                    //_animation.Duration = new TimeSpan(0, 0, 0, System.Convert.ToInt32(Math.Min(59.0, (double)(AnimationsProxy.DurationPathSelected - 0.5 + 0.0001))), System.Convert.ToInt32(((double)AnimationsProxy.DurationPathSelected * 1000) % 1000));;
                    bool check = true;
                    foreach (var item in _animation.EffectOptions)
                    {
                        if ((item.GroupName == (para as EffectOption).GroupName && !(item.Values[0] is EffectOptionAdapter) && CheckEffecOptionInDiretionPath(item as EffectOptionGroup, (para as EffectOption).Value)) || (check && (item.Values[0] is EffectOptionAdapter) && ((item.Values[0] as EffectOptionAdapter).EffectOptionGroup.GroupName == ((para as EffectOption).GroupName) || (item.Values[1] as EffectOptionAdapter).EffectOptionGroup.GroupName == (para as EffectOption).GroupName)))
                        {
                            check = false;
                            if (item.Values[0] is EffectOptionAdapter)
                            {
                                //foreach (var effectOptionAdapter in item.Values)
                                //{
                                //    //Thay đổi giá trị IsSelected của các EffectOptions
                                //    for (int j = 0; j < (item.Values[0] as EffectOptionAdapter).Values.Count; j++)
                                //    {
                                //        if (((effectOptionAdapter as EffectOptionAdapter).Values[j] as EffectOption).Value == (para as EffectOption).Value)
                                //            (effectOptionAdapter as EffectOptionAdapter).Values[j].IsSelected = true;
                                //        else (effectOptionAdapter as EffectOptionAdapter).Values[j].IsSelected = false;
                                //    }
                                //}
                                foreach (var effectOptionAdapter in item.Values)
                                {
                                    //Thay đổi giá trị IsSelected của các EffectOptions
                                    if (effectOptionAdapter is EffectOptionAdapter)
                                    {
                                        foreach (var effectOption in (effectOptionAdapter as EffectOptionAdapter).Values)
                                        {
                                            if (effectOption is EffectOption && (effectOption as EffectOption).GroupName != (para as EffectOption).GroupName) break;
                                            if ((effectOption as EffectOption).Name == (para as EffectOption).Name)
                                            {
                                                (effectOption as EffectOption).IsSelected = true;
                                            }
                                            else
                                            {
                                                (effectOption as EffectOption).IsSelected = false;
                                            }
                                        }
                                    }
                                }
                            }

                            else
                            {
                                //Thay đổi giá trị IsSelected của các EffectOptions
                                if (item.GroupName == "Path")
                                {
                                    for (int j = 0; j < item.Values.Count; j++)
                                    {

                                        if (item.Values[j].Name == (para as EffectOption).Name)
                                        {

                                            item.Values[j].IsSelected = !item.Values[j].IsSelected;
                                        }

                                    }
                                }
                                else
                                {
                                    for (int j = 0; j < item.Values.Count; j++)
                                    {

                                        if (item.Values[j].Name == (para as EffectOption).Name)
                                        {

                                            item.Values[j].IsSelected = true;
                                        }
                                        else item.Values[j].IsSelected = false;

                                    }
                                }

                            }
                        }


                    }
                    Animation _animationPath = null;

                    for (int j = 0; j < (_testControl.ElementAt(i) as ObjectElement).MotionPaths.Count; j++)
                    {
                        _animationPath = CloneAnimation.GetAnimation((AnimationsProxy.ProxyAnimationPath as Animation).Name, (AnimationsProxy.ProxyAnimationPath as Animation).IsSequence);
                        CloneAnimation.CopyAnimation(_animationPath, _animation);
                        (_testControl.ElementAt(i) as ObjectElement).MotionPaths[j].Owner = _testControl.ElementAt(i) as ObjectElement;
                        CloneAnimation.CopyAnimation((_testControl.ElementAt(i) as ObjectElement).MotionPaths[j].Animation as Animation, _animationPath);

                        //  (_testControl.ElementAt(i) as TestControl).MotionPaths[i].Animation = _animationPath 
                    }

                    ObjectElement owner = _testControl.ElementAt(i) as ObjectElement;
                    if (owner != null && owner.MotionPaths != null)
                        foreach (MotionPathObject item in owner.MotionPaths)
                        {
                            (item?.Content as ShapeBase).InvalidateVisual();
                        }
                    // (_testControl.ElementAt(i) as TestControl).AnimationObject.AnimationPath = _animation;



                    //  (_testControl.ElementAt(i) as TestControl).AnimationEntrance.EffectOptions.Add((IEffectOptionGroup)para);


                    //(_testControl.ElementAt(i) as TestControl).AnimationEntrance=new FadeAnimation("Fade", TimeSpan.FromSeconds(2), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Fade.png");
                }
            }

            //Gán lại giá trị đang lựa chọn
            AnimationObject animationObject = AnimationsViewModel.GetAnimationProperty();
            if (isShouldUndo)
            {
                animationObject = GetAnimationProperty(objects, true);
            }
            if (animationObject != null)
            {
                if (animationObject.AnimationPath is NoneAnimation)
                {
                    AnimationsViewModel.AnimationsProxy._animationPath = new MultiAnimation(TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Multiple.png");
                    AnimationsViewModel.AnimationsProxy.AnimationPathSelected = new MultiAnimation(TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Multiple.png");

                }
                Animation _animationSelected = null;
                _animationSelected = AnimationsViewModel.SetAnimationSelected(animationObject.AnimationPath as Animation, _animationSelected, AnimationsViewModel.DataAnimatePath);
                //Gán giá trị animation đang được lựa chọn
                AnimationsViewModel.AnimationsProxy._animationPath = _animationSelected;
                AnimationsViewModel.AnimationsProxy.AnimationPathSelected = _animationSelected;


            }
        }

        private static void SetPathOption(MotionPathObject motionPath, EffectOption option, eObjectAnimationOption value)
        {
            MotionPathAnimation animation = motionPath.Animation as MotionPathAnimation;
            eObjectAnimationOption type = option.Type;
            switch (type)
            {
                case eObjectAnimationOption.Direction:
                    (animation as IDirectionableAnimation).Direction = value;
                    GenerationAnimationViewModel.ChangePathDirectionCommand.Execute(motionPath);
                    break;
                case eObjectAnimationOption.Origin:
                    animation.Origin = value;
                    break;
                case eObjectAnimationOption.EasingDirection:
                    animation.EasingPathDirection = value;
                    break;
                case eObjectAnimationOption.Speed:
                    animation.Speed = value;
                    break;
                case eObjectAnimationOption.ReversePathDirection:
                    animation.ReversePathDirection = !animation.ReversePathDirection;
                    break;
                case eObjectAnimationOption.RelativeStartPoint:
                    animation.RelativeStartPoint = !animation.RelativeStartPoint;
                    break;
                case eObjectAnimationOption.OrientShapeToPath:
                    animation.OrientShapeToPath = !animation.OrientShapeToPath;
                    break;
            }

            //---------------------------------------------------------------------------
            // Cập nhật đường cong hiệu ứng
            //---------------------------------------------------------------------------
            (motionPath?.Content as CustomPath).InvalidateVisual();
        }

        #endregion

        #region Thực hiện các Command Duration
        private static ICommand _durationEntranceCommand;
        /// <summary>
        /// Lệnh thực thi thay đổi Duration đối tượng
        /// </summary>
        public static ICommand DurationEntranceCommand
        {
            get
            {
                return _durationEntranceCommand ?? (_durationEntranceCommand = new RelayCommand(param => SetDurationEntranceCommand(param)));
            }

        }
        /// <summary>
        /// Thực thi thay đổi Duration đối tượng
        /// </summary>
        /// <param name="para"></param>
        public static void SetDurationEntranceCommand(Object para)
        {

            if (!(para is Double) || (double)para < 0) return;


            var _testControl = (Application.Current as IAppGlobal).SelectedElements;
            List<DurationAnimationUndoData> dataUndo = new List<DurationAnimationUndoData>();
            int _countObject = 0;
            _countObject = _testControl.Count();
            //Gán giá trị animation cho đối tượng
            for (int i = 0; i < _countObject; i++)
            {
                if (_testControl.ElementAt(i) is StandardElement)
                {
                    TimeSpan duration = new TimeSpan((int)para / (3600 * 24), (int)para / 3600, (int)para / 60, (int)para / 1, (int)para * 60 / 1);
                    if (_testControl.ElementAt(i) is ObjectElement objectElement)
                    {
                        dataUndo.Add(new DurationAnimationUndoData(objectElement.EntranceAnimation, objectElement.EntranceAnimation.Duration, duration));
                        objectElement.EntranceAnimation.Duration = duration;
                    }
                }

            }
            Global.PushUndo(new DurationAnimationUndo(dataUndo));
        }
        #endregion

        /// <summary>
        /// Trạng thái hiên thị hiệu ứng MotionPath
        /// </summary>
        /// <returns></returns>
        public static object IsEnabledEffectOptionPath()
        {
            var a = GetAnimationProperty().AnimationPath;
            if (GetAnimationProperty().AnimationPath is MultiAnimation)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Lấy danh sách các hiệu ứng đang được chọn
        /// </summary>
        /// <param name="objects"></param>
        /// <param name="isShouldUndo"></param>
        /// <returns></returns>
        public static AnimationObject GetAnimationProperty(ObservableCollection<ObjectElement> objects = null, bool isShouldUndo = false)
        {
            try
            {
                // thay doi
                //Danh sách các đối tượng đang được chọn
                var _listObject = (Application.Current as IAppGlobal)?.SelectedElements;
                if (isShouldUndo && objects != null)
                {
                    _listObject = objects;
                }
                if (_listObject == null) return null;

                AnimationObject _animationObject = new AnimationObject();

                if (_listObject != null && _listObject.Count > 0)
                {

                    if (_listObject[0] is MotionPathObject)
                    {
                        if ((_listObject[0] as MotionPathObject)?.Owner?.EntranceAnimation == null)
                        {
                            (_listObject[0] as MotionPathObject).Owner.EntranceAnimation = new NoneAnimation("None", TimeSpan.FromSeconds(0));
                        }
                        if ((_listObject[0] as MotionPathObject)?.Owner?.ExitAnimation == null)
                        {
                            (_listObject[0] as MotionPathObject).Owner.ExitAnimation = new NoneAnimation("None", TimeSpan.FromSeconds(0));
                        }
                        //Khởi tạo giá trị AnimationEntrance
                        _animationObject.EntranceAnimation = CloneAnimation.GetAnimation((_listObject[0] as MotionPathObject).Owner.EntranceAnimation.Name, (_listObject[0] as MotionPathObject).Owner.EntranceAnimation.IsSequence);
                        // Sao chép thuộc tính AnimationEntrance của đối tượng đang được chọn sang _animationObject
                        CloneAnimation.CopyAnimation(_animationObject.EntranceAnimation as Animation, (_listObject[0] as MotionPathObject).Owner.EntranceAnimation as Animation);

                        //Khởi tạo giá trị AnimationExit
                        _animationObject.ExitAnimation = CloneAnimation.GetAnimation((_listObject[0] as MotionPathObject).Owner.ExitAnimation.Name, (_listObject[0] as MotionPathObject).Owner.ExitAnimation.IsSequence);
                        // Sao chép thuộc tính AnimationExit của đối tượng đang được chọn sang _animationObject
                        CloneAnimation.CopyAnimation(_animationObject.ExitAnimation as Animation, (_listObject[0] as MotionPathObject).Owner.ExitAnimation as Animation);

                        //CHƯA XỬ LÝ               
                        //if ((_listObject[0] as MotionPathObject).Owner.MotionPaths.Count > 0)
                        //{
                        //    //    MotionPathObject _motionPathObject = new MotionPathObject();

                        //    //   CloneAnimation.CopyAnimation(_motionPathObject.Animation as Animation, (_listObject[0] as MotionPathObject).Animation as Animation);
                        //    //     _animationObject.MotionPaths.Add(_motionPathObject);

                        //   // CloneAnimation.CopyAnimation(_animationObject.AnimationPath as Animation ,(_listObject[0] as MotionPathObject).Animation as Animation);
                        //    _animationObject.AnimationPath = (_listObject[0] as MotionPathObject).Animation;
                        //}

                        _animationObject.AnimationPath = CloneAnimation.GetAnimation((_listObject[0] as MotionPathObject).Animation.Name, (_listObject[0] as MotionPathObject).Animation.IsSequence);
                        CloneAnimation.CopyAnimation(_animationObject.AnimationPath, (_listObject[0] as MotionPathObject).Animation as Animation);



                    }
                    else if (_listObject[0] is ObjectElement)
                    {


                        if ((_listObject[0] as ObjectElement).EntranceAnimation == null)
                        {
                            (_listObject[0] as ObjectElement).EntranceAnimation = new NoneAnimation("None", TimeSpan.FromSeconds(0));
                        }

                        if ((_listObject[0] as ObjectElement).ExitAnimation == null)
                        {
                            (_listObject[0] as ObjectElement).ExitAnimation = new NoneAnimation("None", TimeSpan.FromSeconds(0));
                        }
                        //Khởi tạo giá trị AnimationEntrance
                        _animationObject.EntranceAnimation = CloneAnimation.GetAnimation((_listObject[0] as ObjectElement).EntranceAnimation.Name, (_listObject[0] as ObjectElement).EntranceAnimation.IsSequence);

                        // Sao chép thuộc tính AnimationEntrance của đối tượng đang được chọn sang _animationObject
                        CloneAnimation.CopyAnimation(_animationObject.EntranceAnimation as Animation, (_listObject[0] as ObjectElement).EntranceAnimation as Animation);

                        //Khởi tạo giá trị AnimationExit
                        _animationObject.ExitAnimation = CloneAnimation.GetAnimation((_listObject[0] as ObjectElement).ExitAnimation.Name, (_listObject[0] as ObjectElement).ExitAnimation.IsSequence);
                        // Sao chép thuộc tính AnimationExit của đối tượng đang được chọn sang _animationObject
                        CloneAnimation.CopyAnimation(_animationObject.ExitAnimation as Animation, (_listObject[0] as ObjectElement).ExitAnimation as Animation);

                        //CHƯA XỬ LÝ               
                        if ((_listObject[0] as ObjectElement).MotionPaths.Count > 0)
                        {
                            _animationObject.MotionPaths.Add((_listObject[0] as ObjectElement).MotionPaths[0]);
                            // CloneAnimation.CopyAnimation(_animationObject.AnimationPath as Animation, (_listObject[0] as TestControl).MotionPaths[0].Animation as Animation);
                            _animationObject.AnimationPath = CloneAnimation.GetAnimation((_listObject[0] as ObjectElement).MotionPaths[0].Animation.Name, (_listObject[0] as ObjectElement).MotionPaths[0].Animation.IsSequence);
                            CloneAnimation.CopyAnimation(_animationObject.AnimationPath, (_listObject[0] as ObjectElement).MotionPaths[0].Animation as Animation);
                        }
                        else
                        {
                            _animationObject.AnimationPath = new MultiAnimation(TimeSpan.FromSeconds(-1), "/INV.Elearning.Animations.UI;component/Images/Multiple.png");
                        }

                    }
                }
                else
                {
                    _animationObject.EntranceAnimation = new MultiAnimation();
                    _animationObject.ExitAnimation = new MultiAnimation();
                    return _animationObject;
                }
                //Gán giá trị Animation Entrance
                foreach (var item in _listObject)
                {
                    if ((item is MotionPathObject))
                    {
                        if ((item as MotionPathObject).Owner.EntranceAnimation.Name != _animationObject.EntranceAnimation.Name)
                        {
                            _animationObject.EntranceAnimation = new MultiAnimation(TimeSpan.FromSeconds(-1), "/INV.Elearning.Animations.UI;component/Images/Multiple.png");
                        }
                        else
                        {
                            //Trả về giá trị EffectOption
                            _animationObject.EntranceAnimation = GetEffectOptionSelected(_animationObject.EntranceAnimation as Animation, (item as MotionPathObject).Owner.EntranceAnimation as Animation);
                        }

                        if ((item as MotionPathObject).Owner.EntranceAnimation.Duration != _animationObject.EntranceAnimation.Duration)
                        {
                            _animationObject.EntranceAnimation.Duration = TimeSpan.FromMilliseconds(-1);
                        }


                    }
                    else if ((item is ObjectElement))
                    {
                        if ((item as ObjectElement).EntranceAnimation.Name != _animationObject.EntranceAnimation.Name)
                        {
                            _animationObject.EntranceAnimation = new MultiAnimation(TimeSpan.FromSeconds(-1), "/INV.Elearning.Animations.UI;component/Images/Multiple.png");

                        }
                        else
                        {
                            //Trả về giá trị EffectOption
                            _animationObject.EntranceAnimation = GetEffectOptionSelected(_animationObject.EntranceAnimation as Animation, (item as ObjectElement).EntranceAnimation as Animation);
                        }
                        if ((item as ObjectElement).EntranceAnimation.Duration != _animationObject.EntranceAnimation.Duration)
                        {
                            _animationObject.EntranceAnimation.Duration = TimeSpan.FromMilliseconds(-1);
                        }

                    }
                }

                //Gán giá trị Animation Exit
                foreach (var item in _listObject)
                {
                    if ((item is MotionPathObject))
                    {
                        if ((item as MotionPathObject).Owner.ExitAnimation.Name != _animationObject.ExitAnimation.Name)
                        {
                            _animationObject.ExitAnimation = new MultiAnimation(TimeSpan.FromSeconds(-1), "/INV.Elearning.Animations.UI;component/Images/Multiple.png");
                        }
                        else
                        {
                            //Trả về giá trị EffectOption
                            _animationObject.ExitAnimation = GetEffectOptionSelected(_animationObject.ExitAnimation as Animation, (item as MotionPathObject).Owner.ExitAnimation as Animation);
                        }
                        if ((item as MotionPathObject).Owner.ExitAnimation.Duration != _animationObject.ExitAnimation.Duration)
                        {
                            _animationObject.ExitAnimation.Duration = TimeSpan.FromMilliseconds(-1);
                        }

                    }
                    else
                    if ((item is ObjectElement))
                    {
                        if ((item as ObjectElement).ExitAnimation.Name != _animationObject.ExitAnimation.Name)
                        {
                            _animationObject.ExitAnimation = new MultiAnimation(TimeSpan.FromSeconds(-1), "/INV.Elearning.Animations.UI;component/Images/Multiple.png");
                        }
                        else
                        {
                            //Trả về giá trị EffectOption
                            _animationObject.ExitAnimation = GetEffectOptionSelected(_animationObject.ExitAnimation as Animation, (item as ObjectElement).ExitAnimation as Animation);
                        }
                        if ((item as ObjectElement).ExitAnimation.Duration != _animationObject.ExitAnimation.Duration)
                        {
                            _animationObject.ExitAnimation.Duration = TimeSpan.FromMilliseconds(-1);
                        }

                    }
                }

                //Gán giá trị Animation Path
                foreach (var item in _listObject)
                {
                    if ((item is MotionPathObject))
                    {
                        //foreach (var animationPath in (item as MotionPathObject).MotionPaths)
                        //{
                        //    if (_animationObject.MotionPaths.Count > 0 && animationPath.Name != _animationObject.MotionPaths[0].Name)
                        //    {
                        //        CloneAnimation.CopyAnimation(_animationObject.MotionPaths[0].Animation as Animation, new MultiAnimation());
                        //    }
                        //    else if (_animationObject.MotionPaths.Count > 0)
                        //    {
                        //        CloneAnimation.CopyAnimation(_animationObject.MotionPaths[0].Animation as Animation, GetEffectOptionSelected(_animationObject.MotionPaths[0].Animation as Animation, (item as TestControl).MotionPaths[0].Animation as Animation));
                        //    }
                        //}

                        //foreach (var animationPath in (item as TestControl).MotionPaths)
                        {
                            var animationPath = (item as MotionPathObject).Animation;
                            if (_animationObject.AnimationPath != null && animationPath.Name != _animationObject.AnimationPath.Name)
                            {
                                // CloneAnimation.CopyAnimation(_animationObject.MotionPaths[0].Animation as Animation, new MultiAnimation());
                                _animationObject.AnimationPath = new MultiAnimation(TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Multiple.png");
                            }
                            else
                            {
                                CloneAnimation.CopyAnimation(_animationObject.AnimationPath, GetEffectOptionSelected(_animationObject.AnimationPath, (item as MotionPathObject).Animation as Animation));
                            }

                            if ((item as MotionPathObject).Animation.Duration != _animationObject.AnimationPath.Duration)
                            {
                                _animationObject.AnimationPath.Duration = TimeSpan.FromMilliseconds(-1);
                            }
                        }
                    }
                    else
                    if ((item is ObjectElement))
                    {
                        foreach (var animationPath in (item as ObjectElement).MotionPaths)
                        {
                            //if (_animationObject.MotionPaths.Count > 0 && animationPath.Name != _animationObject.MotionPaths[0].Name)
                            //{
                            //    CloneAnimation.CopyAnimation(_animationObject.MotionPaths[0].Animation as Animation, new MultiAnimation());
                            // //   _animationObject.AnimationPath = new MultiAnimation(TimeSpan.FromSeconds(2), "/INV.Elearning.Animations.UI;component/Images/Multiple.png");
                            //    // _animationObject.MotionPaths[0].Animation = new MultiAnimation();
                            //}
                            //else if (_animationObject.MotionPaths.Count > 0)
                            //{
                            //    CloneAnimation.CopyAnimation(_animationObject.MotionPaths[0].Animation as Animation, GetEffectOptionSelected(_animationObject.MotionPaths[0].Animation as Animation, (item as TestControl).MotionPaths[0].Animation as Animation));
                            //    //      _animationObject.MotionPaths[0].Animation = GetEffectOptionSelected(_animationObject.MotionPaths[0].Animation as Animation, (item as TestControl).MotionPaths[0].Animation as Animation);
                            //}

                            if (_animationObject.AnimationPath != null && animationPath.Animation.Name != _animationObject.AnimationPath.Name)
                            {
                                //   CloneAnimation.CopyAnimation(_animationObject.MotionPaths[0].Animation as Animation, new MultiAnimation());
                                _animationObject.AnimationPath = new MultiAnimation(TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Multiple.png");

                            }
                            else if (_animationObject.MotionPaths.Count > 0)
                            {
                                //   CloneAnimation.CopyAnimation(_animationObject.MotionPaths[0].Animation as Animation, GetEffectOptionSelected(_animationObject.MotionPaths[0].Animation as Animation, (item as TestControl).MotionPaths[0].Animation as Animation));
                                //      _animationObject.MotionPaths[0].Animation = GetEffectOptionSelected(_animationObject.MotionPaths[0].Animation as Animation, (item as TestControl).MotionPaths[0].Animation as Animation);
                                //         _animationObject.AnimationPath = CloneAnimation.GetAnimation((_listObject[0] as TestControl).MotionPaths[0].Animation.Name);
                                _animationObject.AnimationPath = GetEffectOptionSelected(_animationObject.AnimationPath, animationPath.Animation as Animation);

                            }

                            if (animationPath.Animation.Duration != _animationObject.AnimationPath.Duration)
                            {
                                _animationObject.AnimationPath.Duration = TimeSpan.FromMilliseconds(-1);
                            }
                        }
                    }
                }
                return _animationObject;
            }
            catch (Exception e)
            {
                
            }
            return null;
        }
        /// <summary>
        /// So sánh hai Animation
        /// </summary>
        /// <param name="animation1"></param>
        /// <param name="animation2"></param>
        /// <returns></returns>
        public static bool CompareAnimation(Animation animation1, Animation animation2)
        {
            if (animation1.Name != animation2.Name || animation1.Image != animation2.Image) return false;
            return true;
        }
        //  public static void ResetAnimation(Animation)

        /// <summary>
        /// Trả về giá trị Animation đang được chọn với giá trị EffectOption thích hợp
        /// </summary>
        /// <param name="animation1"></param>
        /// <param name="animation2"></param>
        /// <returns></returns>
        public static Animation GetEffectOptionSelected(Animation animation1, Animation animation2)
        {
            if (animation1.EffectOptions == null || animation2.EffectOptions == null)
            {
                return animation1;
            }
            foreach (var effectOptionItem in animation1.EffectOptions)
            {
                if (effectOptionItem is EffectOptionGroup)
                {
                    //Xét trường hợp EffectOptionGroup có 2 cấp lựa chọn là Enter
                    if ((effectOptionItem as EffectOptionGroup).GroupName == "Enter" && (effectOptionItem as EffectOptionGroup)?.Values[0] is EffectOptionAdapter)
                    {
                        if ((effectOptionItem as EffectOptionGroup)?.Values[0] is EffectOptionAdapter)
                        {
                            var _listEffectOption = ((effectOptionItem as EffectOptionGroup)?.Values[0] as EffectOptionAdapter).Values;
                            EffectOptionGroup _effectOption = effectOptionItem as EffectOptionGroup;
                            foreach (var effectOptionSelectedItem in animation2.EffectOptions)
                            {
                                //EffectOption đang được lựa chọn
                                EffectOptionGroup _effectOptionSelected = effectOptionSelectedItem as EffectOptionGroup;
                                if (_effectOption.GroupName == _effectOptionSelected.GroupName)
                                {
                                    CloneAnimation.CompareEffectOptionSelected(_effectOption, _effectOptionSelected);
                                    break;
                                }
                            }
                        }
                    }
                    //Xét trường hợp EffectOptionGroup chỉ có 1 cấp lựa chọn
                    else
                    {
                        EffectOptionGroup _effectOption = effectOptionItem as EffectOptionGroup;
                        foreach (var effectOptionSelectedItem in animation2.EffectOptions)
                        {
                            EffectOptionGroup _effectOptionSelected = effectOptionSelectedItem as EffectOptionGroup;
                            if (_effectOption.GroupName == _effectOptionSelected.GroupName)
                            {
                                CloneAnimation.CompareEffectOptionSelected(_effectOption, _effectOptionSelected);
                                break;
                            }
                        }
                    }
                }
            }

            return animation1;
        }

        /// <summary>
        /// Trả về giá trị của dữ liệu Animation đang được lựa chọn
        /// </summary>
        /// <param name="animationObject"> Gía trị animation của đối tượng đang được lựa chọn</param>
        /// <param name="_animationSelected"> Gía trị của dữ liệu (tải lên giao diện) đang được lựa chọn</param>
        /// <returns></returns>
        public static Animation SetAnimationSelected(Animation animationObject, Animation _animationSelected, ObservableCollection<Animation> Data)
        {
            if (animationObject is MultiAnimation)
            {
                //AnimationsViewModel.AnimationsProxy._animation = _animationSelected;
                //AnimationsViewModel.AnimationsProxy.AnimationSelected = _animationSelected;
                //animationProxy = _animationSelected;
                //animationProxySelected = _animationSelected;
                return new MultiAnimation(TimeSpan.FromSeconds(2), "/INV.Elearning.Animations.UI;component/Images/Multiple.png");
            }

            _animationSelected = Data[AnimationsViewModel.SetIndexAnimation(Data, animationObject)];
            //if (animationObject.IsSequence)
            //{
            //    _animationSelected.EffectOptions.Add(new SequenceOption());
            //}
            //else
            //{
            //    if (_animationSelected.EffectOptions?.Last().GroupName == "Sequence")
            //    {
            //        _animationSelected.EffectOptions.Remove(_animationSelected.EffectOptions.Last());
            //    }
            //}
            //Set Selected EffectOption
            int i = 0;
            if (animationObject.EffectOptions != null)
                foreach (var effectOptionItem in animationObject.EffectOptions)
                {
                    if (effectOptionItem is EffectOptionGroup)
                    {
                        //Xét trường hợp EffectOptionGroup có 2 cấp lựa chọn là Enter
                        if ((effectOptionItem as EffectOptionGroup).GroupName == "Enter" && (effectOptionItem as EffectOptionGroup)?.Values[0] is EffectOptionAdapter)
                        {
                            if ((effectOptionItem as EffectOptionGroup)?.Values[0] is EffectOptionAdapter)
                            {
                                var _listEffectOption = ((effectOptionItem as EffectOptionGroup)?.Values[0] as EffectOptionAdapter).Values;
                                EffectOptionGroup _effectOption = effectOptionItem as EffectOptionGroup;
                                foreach (var effectOptionSelectedItem in _animationSelected.EffectOptions)
                                {
                                    //EffectOption đang được lựa chọn
                                    EffectOptionGroup _effectOptionSelected = effectOptionSelectedItem as EffectOptionGroup;
                                    if (_effectOption.GroupName == _effectOptionSelected.GroupName || (_effectOption.GroupName == "Enter" && _effectOptionSelected.GroupName == "Exit"))
                                    {
                                        CloneAnimation.CopySelectedEffectOption(_effectOption, _effectOptionSelected);
                                        break;
                                    }
                                }
                            }
                        }
                        //Xét trường hợp EffectOptionGroup chỉ có 1 cấp lựa chọn
                        else
                        {
                            EffectOptionGroup _effectOption = effectOptionItem as EffectOptionGroup;
                            foreach (var effectOptionSelectedItem in _animationSelected.EffectOptions)
                            {
                                EffectOptionGroup _effectOptionSelected = effectOptionSelectedItem as EffectOptionGroup;
                                if (_effectOption.GroupName == _effectOptionSelected.GroupName || (_effectOption.GroupName == "Enter" && _effectOptionSelected.GroupName == "Exit"))
                                {
                                    CloneAnimation.CopySelectedEffectOption(_effectOption, _effectOptionSelected);
                                    break;
                                }
                            }
                        }
                    }
                    i++;
                }
            return _animationSelected;
        }
        public static bool CheckEffecOptionInDiretionPath(EffectOptionGroup effectOptionDirection, string name)
        {
            foreach (var item in effectOptionDirection.Values)
            {
                if (item.Value == name) return true;
            }
            return false;
        }
        public static void GetCommandMontionPath(Animation animation)
        {
            switch (animation.Name)
            {
                case "Lines":
                    GenerationAnimationViewModel.LineAnimationCommand.Execute(null);
                    break;
                case "Arcs":
                    GenerationAnimationViewModel.ArcsAnimationCommand.Execute(null);
                    break;
                case "Turns":
                    GenerationAnimationViewModel.TurnsAnimationCommand.Execute(null);
                    break;
                case "Circle":
                    GenerationAnimationViewModel.CircleAnimationCommand.Execute(null);
                    break;
                case "Square":
                    GenerationAnimationViewModel.SquareAnimationCommand.Execute(null);
                    break;
                case "Equal Triangle":
                    GenerationAnimationViewModel.EqualTriangleAnimationCommand.Execute(null);
                    break;
                case "Trapezoid":
                    GenerationAnimationViewModel.TrapezoidAnimationCommand.Execute(null);
                    break;
                case "Freeform":
                    GenerationAnimationViewModel.FreeformAnimationCommand.Execute(null);
                    break;
                case "Scribble":
                    GenerationAnimationViewModel.ScribbleAnimationCommand.Execute(null);
                    break;
                case "Curve":
                    GenerationAnimationViewModel.CurveAnimationCommand.Execute(null);
                    break;
                default:
                    break;
            }
            //  GenerationAnimationViewModel.SplitAnimationCommand.Execute(null);
        }

        public static void GetCommandEntrance(Animation animation, eAnimationType type)
        {
            switch (animation.Name)
            {
                case "None":
                    GenerationAnimationViewModel.NoneAnimationCommand.Execute(type);
                    break;
                case "Fade":
                    GenerationAnimationViewModel.FadeAnimationCommand.Execute(type);
                    break;
                case "Grow":
                    GenerationAnimationViewModel.GrowAnimationCommand.Execute(type);
                    break;
                case "FlyIn":
                    GenerationAnimationViewModel.FlyAnimationCommand.Execute(type);
                    break;
                case "FloatIn":
                    GenerationAnimationViewModel.FloatAnimationCommand.Execute(type);
                    break;
                case "Split":
                    GenerationAnimationViewModel.SplitAnimationCommand.Execute(type);
                    break;
                case "Wipe":
                    GenerationAnimationViewModel.WipeAnimationCommand.Execute(type);
                    break;
                case "Shape":
                    GenerationAnimationViewModel.ShapeAnimationCommand.Execute(type);
                    break;
                case "Wheel":
                    GenerationAnimationViewModel.WheelAnimationCommand.Execute(type);
                    break;
                case "RandomBars":
                    GenerationAnimationViewModel.RandomBarsAnimationCommand.Execute(type);
                    break;
                case "Spin":
                    GenerationAnimationViewModel.SpinAnimationCommand.Execute(type);
                    break;
                case "SpinGrow":
                    GenerationAnimationViewModel.SpinGrowAnimationCommand.Execute(type);
                    break;
                case "GrowSpin":
                    GenerationAnimationViewModel.GrowSpinAnimationCommand.Execute(type);
                    break;
                case "Zoom":
                    GenerationAnimationViewModel.ZoomAnimationCommand.Execute(type);
                    break;
                case "Swivel":
                    GenerationAnimationViewModel.SwivelAnimationCommand.Execute(type);
                    break;
                case "Bounce":
                    GenerationAnimationViewModel.BounceAnimationCommand.Execute(type);
                    break;
                default:
                    break;

            }
        }
        /// <summary>
        /// Chuyển tên đa ngôn ngữ
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetNameViewTransition(string name)
        {
            string header = "ANIMATION_" + name.Replace(" ", "");
            header = (Application.Current.TryFindResource(header)) as string;
            return header;
        }

    }


    public class AnimateModel
    {
        public string Image { get; set; }
        public string Name { get; set; }
        public List<IEffectOptionGroup> ListIEffectOptionGroup { get; set; }

    }





}
