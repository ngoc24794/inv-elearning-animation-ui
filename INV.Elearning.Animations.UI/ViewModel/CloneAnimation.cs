﻿using INV.Elearning.Animations.Enums;
using INV.Elearning.Core.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.Animations.UI.ViewModel
{
    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: CloneAnimation.cs
    // Description: Nhân bản hiệu ứng
    // Develope by : Ta Khanh Thien
    // History:
    // 20/02/2018 : Create
    //---------------------------------------------------------------------------
    public static class CloneAnimation
    {
        /// <summary>
        /// Sao chép dữ liệu từ hiệu ứng đối tượng 
        /// </summary>
        /// <param name="objectElement"></param>
        /// <param name="animationTarget"></param>
        /// <param name="typeAnimation"></param>
        public static void CopyObjectToAnimation(ObjectElement objectElement, Animation animationTarget, string typeAnimation)
        {
            if (typeAnimation == "Entrance")
            {
                if (objectElement == null || objectElement.EntranceAnimation == null) return;
                animationTarget = CloneAnimation.GetAnimation(objectElement.EntranceAnimation.Name, true);
                CopyAnimation(animationTarget, objectElement.EntranceAnimation as Animation);
            }
            else if (typeAnimation == "Exit")
            {
                if (objectElement == null || objectElement.ExitAnimation == null) return;
                animationTarget = CloneAnimation.GetAnimation(objectElement.ExitAnimation.Name, true);
                CopyAnimation(animationTarget, objectElement.ExitAnimation as Animation);
            }
        }

        /// <summary>
        /// Sao chép từ hiệu ứng có sẵn sang hiệu ứng đối tượng
        /// </summary>
        /// <param name="animationSource"></param>
        /// <param name="objectElement"></param>
        /// <param name="typeAnimation"></param>
        public static void CopyAnimationToObject(Animation animationSource, ObjectElement objectElement, string typeAnimation)
        {
            if (typeAnimation == "Entrance")
            {
                if (animationSource == null || objectElement == null) return;
                objectElement.EntranceAnimation = CloneAnimation.GetAnimation(objectElement.EntranceAnimation.Name, true);
                CopyAnimation(objectElement.EntranceAnimation as Animation, animationSource);
            }
            else if (typeAnimation == "Exit")
            {
                if (animationSource == null || objectElement == null) return;
                objectElement.EntranceAnimation = CloneAnimation.GetAnimation(objectElement.ExitAnimation.Name, true);
                CopyAnimation(objectElement.ExitAnimation as Animation, animationSource);
            }

        }

        /// <summary>
        /// Sao chép tất cả các hiệu ứng
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <param name="typeAnimation"></param>
        public static void CloneAnimationFull(Animation source, Animation target, string typeAnimation)
        {
            if (source == null) return;
            if (typeAnimation == "Entrance")
            {
                target = CloneAnimation.GetAnimation(source.Name, true);
                if (target?.Name != source.Name) return;
                CopyAnimation(target, source);
            }
            else if (typeAnimation == "Exit")
            {
                target = CloneAnimation.GetAnimation(source.Name, true);
                if (target?.Name != source.Name) return;
                CopyAnimation(target, source);
            }


        }

        /// <summary>
        /// Sao chép hiệu ứng
        /// </summary>
        /// <param name="animationObject"></param>
        /// <param name="animationSelected"></param>
        public static void CopyAnimation(Animation animationObject, Animation animationSelected)
        {
            animationObject.Image = animationSelected.Image;
            animationObject.Duration = animationSelected.Duration;
            animationObject.Name = animationSelected.Name;
            animationObject.IsSequence = animationSelected.IsSequence;
            //CopyEffectOption(animationObject.EffectOptions, animationSelected.EffectOptions);
            //Copy Danh sách EffectOption
            if (animationObject.EffectOptions != null && animationSelected.EffectOptions != null)
                for (int i = 0; i < animationObject.EffectOptions.Count; i++)
                {
                    CopyEffectOptionGroup(animationObject.EffectOptions[i] as EffectOptionGroup, animationSelected.EffectOptions[i] as INV.Elearning.Animations.EffectOptionGroup);
                }
            animationObject.AnimationType = animationSelected.AnimationType;
        }

        /// <summary>
        /// Sao chép các tùy chỉnh trong nhóm hiệu ứng
        /// </summary>
        /// <param name="effectOptionObject"></param>
        /// <param name="effectOptionSelected"></param>
        public static void CopyEffectOptionGroup(EffectOptionGroup effectOptionObject, EffectOptionGroup effectOptionSelected)
        {
            int i = 0;
            foreach (var item in effectOptionObject.Values)
            {
                if (item is EffectOptionAdapter)
                {
                    int j = 0;
                    foreach (var effectOptionAdapter in (item as EffectOptionAdapter).Values)
                    {
                        effectOptionAdapter.IsSelected = (effectOptionSelected.Values[i] as EffectOptionAdapter).Values[j].IsSelected;
                        j++;
                    }
                }
                else
                {
                    item.IsSelected = effectOptionSelected.Values[i].IsSelected;
                }
                i++;
            }
        }

        /// <summary>
        /// Xét các tùy chọn đang được lựa chọn cho hiệu ứng
        /// </summary>
        /// <param name="animation"></param>
        /// <param name="nameGroupEffectOption"></param>
        /// <param name="namEffectOptionSelected"></param>
        /// <returns></returns>
        public static Animation SetEffectOption(Animation animation, string nameGroupEffectOption, string namEffectOptionSelected)
        {
            if (animation?.EffectOptions != null)
            {
                foreach (var groupEffectOption in animation.EffectOptions)
                {
                    if (groupEffectOption.GroupName == nameGroupEffectOption || (groupEffectOption.GroupName == "Easing" && (nameGroupEffectOption == "DirectionEasingPath" || nameGroupEffectOption == "Speed")))
                    {
                        foreach (var effectOption in groupEffectOption.Values)
                        {

                            if (effectOption is EffectOptionAdapter)
                            {
                                if (effectOption.Name == nameGroupEffectOption)
                                    foreach (var effectOptionAdapter in (effectOption as EffectOptionAdapter).Values)
                                    {
                                        if (effectOptionAdapter.Name == namEffectOptionSelected)
                                            effectOptionAdapter.IsSelected = true;
                                        else
                                            effectOptionAdapter.IsSelected = false;
                                    }
                            }
                            else
                            {
                                if (effectOption.Name == namEffectOptionSelected)
                                    effectOption.IsSelected = true;
                                else
                                    effectOption.IsSelected = false;
                            }
                        }
                        break;
                    }
                }
            }
            return animation;
        }

        /// <summary>
        /// Sao chép các tùy chỉnh đang được lựa chọn trong nhóm hiệu ứng
        /// </summary>
        /// <param name="effectOptionObject"></param>
        /// <param name="effectOptionSelected"></param>
        public static void CopySelectedEffectOption(EffectOptionGroup effectOptionObject, EffectOptionGroup effectOptionSelected)
        {
            int i = 0;
            foreach (var item in effectOptionObject.Values)
            {
                if (item is EffectOptionAdapter)
                {
                    int j = 0;
                    foreach (var effectOptionAdapter in (item as EffectOptionAdapter).Values)
                    {
                        (effectOptionSelected.Values[i] as EffectOptionAdapter).Values[j].IsSelected = effectOptionAdapter.IsSelected;
                        j++;
                    }
                }
                else
                {
                    effectOptionSelected.Values[i].IsSelected = item.IsSelected;
                }
                i++;
            }
        }

        /// <summary>
        /// Trả về giá trị EffectOption khi so sánh 2 EffectOption
        /// </summary>
        /// <param name="effectOptionObject"></param>
        /// <param name="effectOptionSelected"></param>
        public static void CompareEffectOptionSelected(EffectOptionGroup effectOptionObject, EffectOptionGroup effectOptionSelected)
        {
            int i = 0;
            foreach (var item in effectOptionObject.Values)
            {

                if (item is EffectOptionAdapter)
                {
                    int j = 0;
                    foreach (var effectOptionAdapter in (item as EffectOptionAdapter).Values)
                    {
                        effectOptionAdapter.IsSelected = effectOptionAdapter.IsSelected && (effectOptionSelected.Values[i] as EffectOptionAdapter).Values[j].IsSelected;
                        j++;
                    }
                }
                else
                {
                    item.IsSelected = item.IsSelected && effectOptionSelected.Values[i].IsSelected;
                }

                i++;
            }
        }

        /// <summary>
        /// Lấy hiệu ứng theo tên
        /// </summary>
        /// <param name="name"></param>
        /// <param name="isSequence"></param>
        /// <returns></returns>
        public static Animation GetAnimation(string name, bool isSequence)
        {
            isSequence = true;
            switch (name)
            {
                case "None":
                    return new NoneAnimation("None", TimeSpan.FromSeconds(0), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/None.png");
                case "Fade":
                    return new FadeAnimation("Fade", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Fade.png", eAnimationType.Entrance, isSequence);
                case "Grow":
                    return new GrowAnimation("Grow", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Grow.png", eAnimationType.Entrance, isSequence);
                case "FlyIn":
                    return new FlyAnimation("FlyIn", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/FlyIn.png", eAnimationType.Entrance, isSequence);
                case "FloatIn":
                    return new FloatAnimation("FloatIn", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/FloatIn.png", eAnimationType.Entrance, isSequence);
                case "FlyOut":
                    return new FlyAnimation("FlyOut", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/FlyIn.png", eAnimationType.Entrance, isSequence);
                case "FloatOut":
                    return new FloatAnimation("FloatOut", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/FloatIn.png", eAnimationType.Entrance, isSequence);
                case "Split":
                    return new SplitAnimation("Split", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Split.png", eAnimationType.Entrance, isSequence);
                case "Wipe":
                    return new WipeAnimation("Wipe", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Wipe.png", eAnimationType.Entrance, isSequence);
                case "Shape":
                    return new ShapeAnimation("Shape", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Shape.png", eAnimationType.Entrance, isSequence);
                case "Wheel":
                    return new WheelAnimation("Wheel", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Wheel.png", eAnimationType.Entrance, isSequence);
                case "RandomBars":
                    var t = new RandomBarsAnimation("RandomBars", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/RandomBars.png", eAnimationType.Entrance, isSequence);
                    if (t.EffectOptions.Count() <= 2)
                    {
                        t.EffectOptions.Add(new SequenceOption());
                    }
                    return t;
                case "Spin":
                    return new SpinAnimation("Spin", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Spin.png", eAnimationType.Entrance, isSequence);
                case "Spin&Grow":
                    return new SpinGrowAnimation("Spin&Grow", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/SpinGrow.png", eAnimationType.Entrance, isSequence);
                case "Grow&Spin":
                    return new GrowSpinAnimation("Grow&Spin", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/GrowSpin.png", eAnimationType.Entrance, isSequence);
                case "Zoom":
                    var zoom = new ZoomAnimation("Zoom", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Zoom.png", eAnimationType.Entrance, isSequence);
                    if (zoom.EffectOptions.Count() <= 1)
                    {
                        zoom.EffectOptions.Add(new SequenceOption());
                    }
                    return zoom;

                case "Swivel":
                    return new SwivelAnimation("Swivel", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Swivel.png", eAnimationType.Entrance, isSequence);
                case "Bounce":
                    return new BounceAnimation("Bounce", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Bounce.png", eAnimationType.Entrance, isSequence);
                case "Lines":
                    return new LinesAnimation(TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Lines.png", eAnimationType.Entrance, isSequence);
                case "Arcs":
                    return new ArcsAnimation(TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Arcs.png", eAnimationType.Entrance, isSequence);
                case "Turns":
                    return new TurnsAnimation(TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Turns.png", eAnimationType.Entrance, isSequence);
                case "Circle":
                    return new CircleAnimation("Circle", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Circle.png");
                case "Square":
                    return new SquareAnimation(TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Square.png", eAnimationType.Entrance, isSequence);
                case "Equal":
                    return new EqualTriangleAnimation("Equal Triangle", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/EqualTriangle.png");
                case "Equal Triangle":
                    return new EqualTriangleAnimation(TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Trapezoid.png", eAnimationType.Entrance, isSequence);
                case "Trapezoid":
                    return new TrapezoidAnimation(TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Trapezoid.png", eAnimationType.Entrance, isSequence);
                case "Freeform":
                    return new FreeformAnimation(TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Freeform.png", eAnimationType.Entrance, isSequence);
                case "Scribble":
                    return new ScribbleAnimation(TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Scribble.png", eAnimationType.Entrance, isSequence);
                case "Curve":
                    return new CurveAnimation(TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Curve.png", eAnimationType.Entrance, isSequence);
                default:
                    return new MultiAnimation(TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Multiple.png", eAnimationType.Entrance, isSequence);
            }


        }
    }
}
