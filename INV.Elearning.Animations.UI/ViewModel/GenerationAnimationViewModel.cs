﻿using INV.Elearing.Controls.Shapes;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Core.Helper;
using INV.Elearning.Core.View;
using INV.Elearning.Core.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace INV.Elearning.Animations.UI
{
    public class GenerationAnimationViewModel : RootViewModel
    {


        public static bool GetShouldRemove(DependencyObject obj)
        {
            return (bool)obj.GetValue(ShouldRemoveProperty);
        }

        public static void SetShouldRemove(DependencyObject obj, bool value)
        {
            obj.SetValue(ShouldRemoveProperty, value);
        }

        // Using a DependencyProperty as the backing store for ShouldRemove.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShouldRemoveProperty =
            DependencyProperty.RegisterAttached("ShouldRemove", typeof(bool), typeof(GenerationAnimationViewModel), new PropertyMetadata(true));


        #region Properties
        public static IList<ObjectElement> RememberSelectedObject { get; set; }
        public static IList<ObjectElement> ObjectsToRemove { get; set; }
        public static IList<ObjectElement> ObjectsToAdd { get; set; }
        public static bool IsShowMotionPaths { get; private set; }
        public static GenerationAnimationFactoryBase GenerationAnimationFactory { get => _generationAnimationFactory ?? (_generationAnimationFactory = new GenerationAnimationFactory()); private set => _generationAnimationFactory = value; }
        #endregion

        #region Commands

        #region MotionPathVisibleCommand
        private static RelayCommand _motionPathVisibleCommand;
        public static RelayCommand MotionPathVisibleCommand
        {
            get { return _motionPathVisibleCommand ?? (_motionPathVisibleCommand = new RelayCommand(x => SetMotionPathsVisible(x), c => true)); }
            set { _motionPathVisibleCommand = value; }
        }

        private static void SetMotionPathsVisible(object obj)
        {
            if (obj is bool value)
            {
                IsShowMotionPaths = value == true ? true : false;

                if (IsShowMotionPaths == true)                                                              // Hiện Motion Paths
                {

                    //---------------------------------------------------------------------------
                    // Chọn lại các đối tượng 
                    //---------------------------------------------------------------------------
                    //if (RememberSelectedObject != null)
                    //{
                    //    foreach (ObjectElement item in RememberSelectedObject)
                    //    {
                    //        item.IsSelected = true;
                    //    }
                    //}

                    foreach (var item in (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Children)
                    {
                        if (item is MotionPathObject motionPathObject)
                        {
                            //---------------------------------------------------------------------------
                            // Hiện Motion Path
                            //---------------------------------------------------------------------------
                            motionPathObject.Visibility = Visibility.Visible;
                        }
                    }
                }
                else                                                                                         // Ẩn Motion Paths
                {

                    //---------------------------------------------------------------------------
                    // Tạo danh sách lưu nếu chưa có
                    //---------------------------------------------------------------------------
                    //if (RememberSelectedObject == null)
                    //    RememberSelectedObject = new List<ObjectElement>();
                    //RememberSelectedObject.Clear();

                    foreach (var item in (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Children)
                    {
                        if (item is MotionPathObject motionPathObject)
                        {

                            //---------------------------------------------------------------------------
                            // Ẩn Motion Path
                            //---------------------------------------------------------------------------
                            motionPathObject.Visibility = Visibility.Hidden;
                            //if (motionPathObject.IsSelected)
                            //{
                            //    RememberSelectedObject.Add(motionPathObject);
                            //}
                            motionPathObject.IsSelected = false;
                        }
                    }
                }
            }
        }
        #endregion

        #region PathAnimationCommands
        #region LineAnimationCommand
        private static RelayCommand _lineAnimationCommand;
        public static RelayCommand LineAnimationCommand
        {
            get { return _lineAnimationCommand ?? (_lineAnimationCommand = new RelayCommand(x => { GenerationAnimationFactory.CreateLineAnimationPath(); }, c => true)); }
            set { _lineAnimationCommand = value; }
        }

        #endregion

        #region ArcsAnimationCommand
        private static RelayCommand _arcsAnimationCommand;
        public static RelayCommand ArcsAnimationCommand
        {
            get { return _arcsAnimationCommand ?? (_arcsAnimationCommand = new RelayCommand(x => { GenerationAnimationFactory.CreateArcsAnimationPath(); }, c => true)); }
            set { _arcsAnimationCommand = value; }
        }

        #endregion

        #region TurnsAnimationCommand
        private static RelayCommand _turnsAnimationCommand;
        public static RelayCommand TurnsAnimationCommand
        {
            get { return _turnsAnimationCommand ?? (_turnsAnimationCommand = new RelayCommand(x => { GenerationAnimationFactory.CreateTurnsAnimationPath(); }, c => true)); }
            set { _turnsAnimationCommand = value; }
        }

        #endregion

        #region FreeformAnimationCommand
        private static RelayCommand _freeformAnimationCommand;
        public static RelayCommand FreeformAnimationCommand
        {
            get { return _freeformAnimationCommand ?? (_freeformAnimationCommand = new RelayCommand(x => { GenerationAnimationFactory.CreateFreeformAnimationPath(); }, c => true)); }
            set { _freeformAnimationCommand = value; }
        }

        #endregion

        #region ScribbleAnimationCommand
        private static RelayCommand _scribbleAnimationCommand;
        public static RelayCommand ScribbleAnimationCommand
        {
            get { return _scribbleAnimationCommand ?? (_scribbleAnimationCommand = new RelayCommand(x => { GenerationAnimationFactory.CreateScribbleAnimationPath(); }, c => true)); }
            set { _scribbleAnimationCommand = value; }
        }

        #endregion

        #region CurveAnimationCommand
        private static RelayCommand _curveAnimationCommand;
        public static RelayCommand CurveAnimationCommand
        {
            get { return _curveAnimationCommand ?? (_curveAnimationCommand = new RelayCommand(x => { GenerationAnimationFactory.CreateCurveAnimationPath(); }, c => true)); }
            set { _curveAnimationCommand = value; }
        }

        #endregion

        #region CircleAnimationCommand
        private static RelayCommand _circleAnimationCommand;
        public static RelayCommand CircleAnimationCommand
        {
            get { return _circleAnimationCommand ?? (_circleAnimationCommand = new RelayCommand(x => { GenerationAnimationFactory.CreateCircleAnimationPath(); }, c => true)); }
            set { _circleAnimationCommand = value; }
        }
        #endregion

        #region SquareAnimationCommand
        private static RelayCommand _squareAnimationCommand;
        public static RelayCommand SquareAnimationCommand
        {
            get { return _squareAnimationCommand ?? (_squareAnimationCommand = new RelayCommand(x => { GenerationAnimationFactory.CreateSquareAnimationPath(); }, c => true)); }
            set { _squareAnimationCommand = value; }
        }
        #endregion

        #region EqualTriangleAnimationCommand
        private static RelayCommand _equalTriangleAnimationCommand;
        public static RelayCommand EqualTriangleAnimationCommand
        {
            get { return _equalTriangleAnimationCommand ?? (_equalTriangleAnimationCommand = new RelayCommand(x => { GenerationAnimationFactory.CreateEqualTriangleAnimationPath(); }, c => true)); }
            set { _equalTriangleAnimationCommand = value; }
        }
        #endregion

        #region TrapezoidAnimationCommand
        private static RelayCommand _trapezoidAnimationCommand;
        public static RelayCommand TrapezoidAnimationCommand
        {
            get { return _trapezoidAnimationCommand ?? (_trapezoidAnimationCommand = new RelayCommand(x => { GenerationAnimationFactory.CreateTrapezoidAnimationPath(); }, c => true)); }
            set { _trapezoidAnimationCommand = value; }
        }

        #endregion

        #region RemovePathAnimationCommand
        private static RelayCommand _removePathAnimationCommand;
        /// <summary>
        /// Xóa các đường Motion Path cùng Animation tương ứng
        /// </summary>
        public static RelayCommand RemovePathAnimationCommand
        {
            get { return _removePathAnimationCommand ?? (_removePathAnimationCommand = new RelayCommand(x => RemovePathAnimation(), c => true)); }
            set { _removePathAnimationCommand = value; }
        }

        public static void RemovePathAnimation()
        {

            //---------------------------------------------------------------------------
            // Lưu các đường Motion Path cần xóa
            //---------------------------------------------------------------------------
            List<IMotionPathObject> pathsToRemove = new List<IMotionPathObject>();
            foreach (var child in (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements)
            {
                if (child is IMotionPathObject motionPathObject && child.IsSelected)
                {
                    pathsToRemove.Add(motionPathObject);
                }
            }


            //---------------------------------------------------------------------------
            // Thực hiện xóa các đường Motion Path cùng Animation tương ứng
            //---------------------------------------------------------------------------
            foreach (IMotionPathObject motionPathObject in pathsToRemove)
            {

                //---------------------------------------------------------------------------
                // Xóa đường Motion Path ra khỏi Canvas, khỏi danh sách MotionPaths của 
                // đối tượng sở hữu (Owner) nó
                //---------------------------------------------------------------------------
                (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements.Remove(motionPathObject as ObjectElement);
                motionPathObject.Owner.MotionPaths.Remove(motionPathObject);


                //---------------------------------------------------------------------------
                // Xóa 'ngôi sao hiệu ứng' nếu owner không còn hiệu ứng đường path nữa
                //---------------------------------------------------------------------------
                IAnimationableObject owner = motionPathObject.Owner;
                if (owner.MotionPaths?.Count == 0)
                {
                    if (AdornerLayer.GetAdornerLayer(owner as ObjectElement) is AdornerLayer layer)
                    {
                        List<Adorner> animationAdorners = new List<Adorner>();
                        Adorner[] adorners = layer.GetAdorners(owner as ObjectElement);
                        if (adorners != null)
                            foreach (Adorner adorner in adorners)
                            {
                                if (adorner is AnimationAdorner)
                                {
                                    animationAdorners.Add(adorner);
                                }
                            }
                        if (animationAdorners.Count > 0)
                        {
                            //---------------------------------------------------------------------------
                            // Xóa tất cả các 'ngôi sao hiệu ứng' 
                            //---------------------------------------------------------------------------
                            owner.IsHasAnimation = false;                   // đặt lại cờ hiệu ứng

                            foreach (Adorner adorner in animationAdorners)
                            {
                                layer.Remove(adorner);
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #endregion

        #region AnimationCommands

        #region BounceAnimationCommand
        private static RelayCommand _bounceAnimationCommand;

        public static RelayCommand BounceAnimationCommand
        {
            get { return _bounceAnimationCommand ?? (_bounceAnimationCommand = new RelayCommand(x => { GenerationAnimationFactory.CreateBounceAnimation((eAnimationType)x); }, c => true)); }
            set { _bounceAnimationCommand = value; }
        }
        #endregion

        #region FadeAnimationCommand
        private static RelayCommand _fadeAnimationCommand;

        public static RelayCommand FadeAnimationCommand
        {
            get { return _fadeAnimationCommand ?? (_fadeAnimationCommand = new RelayCommand(x => { GenerationAnimationFactory.CreateFadeAnimation((eAnimationType)x); }, c => true)); }
            set { _fadeAnimationCommand = value; }
        }
        #endregion

        #region FloatAnimationCommand
        private static RelayCommand _floatAnimationCommand;

        public static RelayCommand FloatAnimationCommand
        {
            get { return _floatAnimationCommand ?? (_floatAnimationCommand = new RelayCommand(x => { GenerationAnimationFactory.CreateFloatAnimation((eAnimationType)x); }, c => true)); }
            set { _floatAnimationCommand = value; }
        }
        #endregion

        #region FlyAnimationCommand
        private static RelayCommand _flyAnimationCommand;

        public static RelayCommand FlyAnimationCommand
        {
            get { return _flyAnimationCommand ?? (_flyAnimationCommand = new RelayCommand(x => { GenerationAnimationFactory.CreateFlyAnimation((eAnimationType)x); }, c => true)); }
            set { _flyAnimationCommand = value; }
        }
        #endregion

        #region GrowAnimationCommand
        private static RelayCommand _growAnimationCommand;

        public static RelayCommand GrowAnimationCommand
        {
            get { return _growAnimationCommand ?? (_growAnimationCommand = new RelayCommand(x => { GenerationAnimationFactory.CreateGrowAnimation((eAnimationType)x); }, c => true)); }
            set { _growAnimationCommand = value; }
        }
        #endregion

        #region GrowSpinAnimationCommand
        private static RelayCommand _growSpinAnimationCommand;

        public static RelayCommand GrowSpinAnimationCommand
        {
            get { return _growSpinAnimationCommand ?? (_growSpinAnimationCommand = new RelayCommand(x => { GenerationAnimationFactory.CreateGrowSpinAnimation((eAnimationType)x); }, c => true)); }
            set { _growSpinAnimationCommand = value; }
        }
        #endregion

        #region MultiAnimationCommand
        private static RelayCommand _multiAnimationCommand;

        public static RelayCommand MultiAnimationCommand
        {
            get { return _multiAnimationCommand ?? (_multiAnimationCommand = new RelayCommand(x => { GenerationAnimationFactory.CreateMultiAnimation((eAnimationType)x); }, c => true)); }
            set { _multiAnimationCommand = value; }
        }
        #endregion

        #region NoneAnimationCommand
        private static RelayCommand _noneAnimationCommand;

        public static RelayCommand NoneAnimationCommand
        {
            get { return _noneAnimationCommand ?? (_noneAnimationCommand = new RelayCommand(x => { GenerationAnimationFactory.CreateNoneAnimation((eAnimationType)x); }, c => true)); }
            set { _noneAnimationCommand = value; }
        }
        #endregion

        #region RandomBarsAnimationCommand
        private static RelayCommand _randomBarsAnimationCommand;

        public static RelayCommand RandomBarsAnimationCommand
        {
            get { return _randomBarsAnimationCommand ?? (_randomBarsAnimationCommand = new RelayCommand(x => { GenerationAnimationFactory.CreateRandomBarsAnimation((eAnimationType)x); }, c => true)); }
            set { _randomBarsAnimationCommand = value; }
        }
        #endregion

        #region ShapeAnimationCommand
        private static RelayCommand _shapeAnimationCommand;

        public static RelayCommand ShapeAnimationCommand
        {
            get { return _shapeAnimationCommand ?? (_shapeAnimationCommand = new RelayCommand(x => { GenerationAnimationFactory.CreateShapeAnimation((eAnimationType)x); }, c => true)); }
            set { _shapeAnimationCommand = value; }
        }
        #endregion

        #region SpinAnimationCommand
        private static RelayCommand _spinAnimationCommand;

        public static RelayCommand SpinAnimationCommand
        {
            get { return _spinAnimationCommand ?? (_spinAnimationCommand = new RelayCommand(x => { GenerationAnimationFactory.CreateSpinAnimation((eAnimationType)x); }, c => true)); }
            set { _spinAnimationCommand = value; }
        }
        #endregion

        #region SpinGrowAnimationCommand
        private static RelayCommand _spinGrowAnimationCommand;

        public static RelayCommand SpinGrowAnimationCommand
        {
            get { return _spinGrowAnimationCommand ?? (_spinGrowAnimationCommand = new RelayCommand(x => { GenerationAnimationFactory.CreateSpinGrowAnimation((eAnimationType)x); }, c => true)); }
            set { _spinGrowAnimationCommand = value; }
        }
        #endregion

        #region SplitAnimationCommand
        private static RelayCommand _splitAnimationCommand;

        public static RelayCommand SplitAnimationCommand
        {
            get { return _splitAnimationCommand ?? (_splitAnimationCommand = new RelayCommand(x => { GenerationAnimationFactory.CreateSplitAnimation((eAnimationType)x); }, c => true)); }
            set { _splitAnimationCommand = value; }
        }
        #endregion

        #region SwivelAnimationCommand
        private static RelayCommand _swivelAnimationCommand;

        public static RelayCommand SwivelAnimationCommand
        {
            get { return _swivelAnimationCommand ?? (_swivelAnimationCommand = new RelayCommand(x => { GenerationAnimationFactory.CreateSwivelAnimation((eAnimationType)x); }, c => true)); }
            set { _swivelAnimationCommand = value; }
        }
        #endregion

        #region WheelAnimationCommand
        private static RelayCommand _wheelAnimationCommand;

        public static RelayCommand WheelAnimationCommand
        {
            get { return _wheelAnimationCommand ?? (_wheelAnimationCommand = new RelayCommand(x => { GenerationAnimationFactory.CreateWheelAnimation((eAnimationType)x); }, c => true)); }
            set { _wheelAnimationCommand = value; }
        }
        #endregion

        #region WipeAnimationCommand
        private static RelayCommand _wipeAnimationCommand;

        public static RelayCommand WipeAnimationCommand
        {
            get { return _wipeAnimationCommand ?? (_wipeAnimationCommand = new RelayCommand(x => { GenerationAnimationFactory.CreateWipeAnimation((eAnimationType)x); }, c => true)); }
            set { _wipeAnimationCommand = value; }
        }
        #endregion

        #region ZoomAnimationCommand
        private static RelayCommand _zoomAnimationCommand;

        public static RelayCommand ZoomAnimationCommand
        {
            get { return _zoomAnimationCommand ?? (_zoomAnimationCommand = new RelayCommand(x => { GenerationAnimationFactory.CreateZoomAnimation((eAnimationType)x); }, c => true)); }
            set { _zoomAnimationCommand = value; }
        }
        #endregion

        #endregion

        #region TransitionCommands

        #region BlindsTransitionCommand
        private static RelayCommand _blindsTransitionCommand;

        public static RelayCommand BlindsTransitionCommand
        {
            get { return _blindsTransitionCommand ?? (_blindsTransitionCommand = new RelayCommand(x => { GenerationAnimationFactory.CreateBounceAnimation((eAnimationType)x); }, c => true)); }
            set { _blindsTransitionCommand = value; }
        }
        #endregion

        #endregion

        #region SaveCommand
        private static RelayCommand _saveCommand;

        public static RelayCommand SaveCommand
        {
            get { return _saveCommand ?? (_saveCommand = new RelayCommand(x => Save(), c => true)); }
            set { _saveCommand = value; }
        }

        private static void Save()
        {
            //string fileName = "slide.xml";
            //List<MotionPathObjectInfo> list = new List<MotionPathObjectInfo>();
            //foreach (IStageObject item in (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements)
            //{
            //    if (item is MotionPathObject motionPathObject)
            //    {
            //        MotionPathObjectInfo motionPathObjectInfo = new MotionPathObjectInfo(motionPathObject);

            //        list.Add(motionPathObjectInfo);
            //    }
            //}
            //SaveObjectToXMLFile(list, fileName);
        }

        private static bool SaveObjectToXMLFile(object obj, string sPath)
        {
            try
            {
                if (File.Exists(sPath))//Kiểm tra tồn tại tập tin
                    File.Delete(sPath);

                using (TextWriter filestream = new StreamWriter(sPath))
                {
                    System.Xml.Serialization.XmlSerializer serialiser = new System.Xml.Serialization.XmlSerializer(obj.GetType());
                    serialiser.Serialize(filestream, obj);
                }
                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        #endregion

        #region LoadCommand
        private static RelayCommand _loadCommand;

        public static RelayCommand LoadCommand
        {
            get { return _loadCommand ?? (_loadCommand = new RelayCommand(x => Load(), c => true)); }
            set { _loadCommand = value; }
        }

        private static void Load()
        {
            //string fileName = "slide.xml";
            //List<MotionPathObjectInfo> list = LoadObjectXMLFromFile<List<MotionPathObjectInfo>>(fileName);
            //foreach (MotionPathObjectInfo item in list)
            //{
            //    MotionPathObject motionPathObject = item.ConvertToObject();
            //    ChangePathDirectionCommand.Execute(motionPathObject);
            //    (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Children.Add(motionPathObject);
            //}
        }

        /// <summary>
        /// Dựng lại đối từ từ tập tin XML
        /// </summary>
        /// <param name="sPath"></param>
        /// <returns></returns>
        private static T LoadObjectXMLFromFile<T>(string sPath)
        {
            try
            {
                if (File.Exists(sPath))
                    using (StreamReader fileStream = new StreamReader(sPath))
                    {
                        System.Xml.Serialization.XmlSerializer reader = new System.Xml.Serialization.XmlSerializer(typeof(T));
                        return (T)reader.Deserialize(fileStream);
                    }
                else
                    return default(T);
            }
            catch
            {
                return default(T);
            }
        }
        #endregion

        #region ChangePathDirectionCommand
        private static RelayCommand _changePathDirectionCommand;

        public static RelayCommand ChangePathDirectionCommand
        {
            get { return _changePathDirectionCommand ?? (_changePathDirectionCommand = new RelayCommand(x => ChangePathDirection(x), c => true)); }
            set { _changePathDirectionCommand = value; }
        }

        private static void ChangePathDirection(object para)
        {
            if (para is MotionPathObject obj)
            {
                ShapeBase path = new FreeformPath();

                MotionPathType type = default(MotionPathType);
                Type pathType = obj.Content.GetType();
                if (pathType == typeof(LinePath)) type = MotionPathType.Line;
                if (pathType == typeof(ArcsPath)) type = MotionPathType.Arc;
                if (pathType == typeof(TurnsPath)) type = MotionPathType.Turn;
                double width = 200, height = 200, minWidth = 10, minHeight = 10;
                MotionPathFactory motionPathFactory = new MotionPathFactory();
                eObjectAnimationOption direction = (obj.Animation as IDirectionableAnimation).Direction;
                switch (type)
                {
                    case MotionPathType.Line:
                        path = motionPathFactory.CreateLinePath() as LinePath;
                        switch (direction)
                        {
                            case eObjectAnimationOption.Down:
                            case eObjectAnimationOption.Up:
                                width = 1;
                                minWidth = 1;
                                break;
                            default:
                                height = 1;
                                minHeight = 1;
                                break;
                        }
                        break;
                    case MotionPathType.Arc:
                        path = motionPathFactory.CreateArcsPath() as ArcsPath;
                        switch (direction)
                        {
                            case eObjectAnimationOption.Left:
                            case eObjectAnimationOption.Right:
                                width = 300;
                                break;
                            default:
                                height = 200;
                                break;
                        }
                        break;
                    case MotionPathType.Turn:
                        path = motionPathFactory.CreateTurnsPath() as TurnsPath;
                        break;

                    default:
                        path = motionPathFactory.CreateLinePath() as LinePath;
                        break;
                }


                path.DashType = Controls.Enums.DashType.Dash;
                path.Stroke = STROKE_DYNAMIC;

                MotionPathObject motionPathObject = new MotionPathObject
                {
                    Owner = obj.Owner,
                    Animation = obj.Animation,
                    Content = path,
                    Width = width,
                    Height = height,
                    MinWidth = minWidth,
                    MinHeight = minHeight
                };

                (path as IMotionPath).Container = motionPathObject;
                (path as IMotionPath).Owner = motionPathObject.Animation;
                motionPathObject.Owner = motionPathObject.Owner;
                motionPathObject.Animation.Owner = motionPathObject.Owner;

                Binding bindWidth = new Binding()
                {
                    Path = new PropertyPath("Width"),
                    Source = motionPathObject
                };

                Binding bindHeight = new Binding()
                {
                    Path = new PropertyPath("Height"),
                    Source = motionPathObject
                };

                Binding bindIsSelected = new Binding()
                {
                    Path = new PropertyPath("IsSelected"),
                    Source = motionPathObject
                };

                BindingOperations.SetBinding(path, ShapeBase.WidthProperty, bindWidth);
                BindingOperations.SetBinding(path, ShapeBase.HeightProperty, bindHeight);
                BindingOperations.SetBinding(path, ShapeBase.IsSelectedProperty, bindIsSelected);

                motionPathObject.Content = path;
                //---------------------------------------------------------------------------
                // Thêm MotionPathObject vào Canvas
                //---------------------------------------------------------------------------
                Point? startPoint = (path as BasicsPath).GetGeometry()?.GetStartPoint();

                if (startPoint != null)
                {
                    double
                                left = motionPathObject.Owner.Left + motionPathObject.Owner.Width / 2 - startPoint.Value.X,
                                top = motionPathObject.Owner.Top + motionPathObject.Owner.Height / 2 - startPoint.Value.Y;

                    motionPathObject.Left = left;
                    motionPathObject.Top = top;

                    if (ObjectsToRemove != null)
                    {
                        ObjectsToRemove.Add(obj);
                    }

                    if (ObjectsToAdd != null)
                    {
                        ObjectsToAdd.Add(motionPathObject);
                    }
                    RememberSelectedObject.Add(motionPathObject);
                }
            }
        }
        #endregion

        #region DeleteCommand


        private static ICommand _deleteCommand;

        public static ICommand DeleteCommand
        {
            get { return _deleteCommand ?? (_deleteCommand = new RelayCommand(x => Delete(), c => true)); }
        }

        private static void Delete()
        {
            List<MotionPathObject> objectsToRemove = new List<MotionPathObject>();
            foreach (var item in (Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Children)
            {
                if (item is MotionPathObject motionPathObject && motionPathObject.IsSelected)
                {
                    objectsToRemove.Add(motionPathObject);
                }
            }

            foreach (MotionPathObject item in objectsToRemove)
            {
                if (item.Owner is ObjectElement owner)
                {
                    owner.MotionPaths.Remove(item);
                }

                (Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Children?.Remove(item);
            }
        }


        #endregion

        #region CutCommand


        private static ICommand _cutCommand;

        public static ICommand CutCommand
        {
            get { return _cutCommand ?? (_cutCommand = new RelayCommand(x => Cut(), c => true)); }
        }

        private static void Cut()
        {
            Copy();
            Delete();
        }

        #endregion

        #region CopyCommand


        private static ICommand _copyCommand;

        public static ICommand CopyCommand
        {
            get { return _copyCommand ?? (_copyCommand = new RelayCommand(x => Copy(), c => true)); }
        }

        private static void Copy()
        {
            if ((Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Children != null)
            {
                List<MotionPathObject> motionPaths = new List<MotionPathObject>();
                foreach (var item in (Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Children)
                {
                    if (item is MotionPathObject motionPathObject && motionPathObject.IsSelected)
                    {
                        motionPaths.Add(motionPathObject);
                    }
                }
                if (motionPaths.Count > 0)
                {
                    List<DataMotionPath> motionPathInfo = new List<DataMotionPath>();
                    foreach (MotionPathObject item in motionPaths)
                    {
                        if (item.GetData() is DataMotionPath dataMotionPath)
                        {
                            motionPathInfo.Add(dataMotionPath.Clone() as DataMotionPath);
                        }
                    }

                    Clipboard.SetData("Animation_Path", motionPathInfo);
                }
            }
        }

        #endregion

        #region PasteCommand


        private static ICommand _pasteCommand;

        public static ICommand PasteCommand
        {
            get { return _pasteCommand ?? (_pasteCommand = new RelayCommand(x => Paste(), c => true)); }
        }

        private static void Paste()
        {
            if (Clipboard.GetData("Animation_Path") is List<DataMotionPath> data && (Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout is LayoutBase layer)
            {
                List<MotionPathObject> motionPathObjects = new List<MotionPathObject>();
                foreach (DataMotionPath item in data)
                {
                    if (AnimationUIHelper.FindElement(layer, item.XOwner) is ObjectElement objectElement)
                    {
                        (Application.Current as IAppGlobal).ConverterDataPathToPath(item, out MotionPathObject motionPathObject, objectElement);
                        objectElement.MotionPaths.Add(motionPathObject);
                        (Application.Current as IAppGlobal)?.SelectedSlide?.SelectedLayout?.Children?.Add(motionPathObject);
                    }
                }
            }
        }

        #endregion

        #region RenameCommand


        private static ICommand _renameCommand;

        public static ICommand RenameCommand
        {
            get { return _renameCommand ?? (_renameCommand = new RelayCommand(x => Rename(), c => (Application.Current as IAppGlobal).SelectedElements?.Count == 1)); }
        }

        private static void Rename()
        {
            MotionPathObject motionPathObject = null;
            foreach (var item in (Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Children)
            {
                if (item is MotionPathObject path && path.IsSelected)
                {
                    motionPathObject = path;
                    break;
                }
            }
            if (motionPathObject != null)
            {
                RenameMotionPathWindow window = new RenameMotionPathWindow();
                //if (Application.Current?.Windows.Count > 0)
                //{
                //    window.Owner = Application.Current.Windows[Application.Current.Windows.Count - 1];
                //}
                RenameMotionPathWindowViewModel viewModel = window.DataContext as RenameMotionPathWindowViewModel;
                if (viewModel != null)
                {
                    viewModel.Title = "Rename " + motionPathObject.TargetName;
                    viewModel.Caption = "Name:";
                    viewModel.Name = motionPathObject.TargetName;
                }

                window.ShowDialog();
                if (viewModel != null && viewModel.IsHittedOkButton)
                {
                    motionPathObject.TargetName = viewModel.Name;
                }
            }
        }

        #endregion

        #endregion

        #region Contants
        //---------------------------------------------------------------------------
        // Các thông số dùng để vẽ đường Motion Path
        //---------------------------------------------------------------------------
        public static Brush STROKE_DYNAMIC = (Brush)(new BrushConverter().ConvertFrom("#4978A2"));
        public static Brush STROKE_STATIC = (Brush)(new BrushConverter().ConvertFrom("#BCBCBC"));
        public static double STROKE_THICKNESS = 1.0;


        //---------------------------------------------------------------------------
        // Các giá trị mặc định cho Animation
        //---------------------------------------------------------------------------
        public static TimeSpan DURATION = TimeSpan.FromSeconds(1);
        private static GenerationAnimationFactoryBase _generationAnimationFactory;
        #endregion
    }
}
