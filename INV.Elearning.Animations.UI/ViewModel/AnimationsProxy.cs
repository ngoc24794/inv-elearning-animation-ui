﻿using INV.Elearning.Core.Helper;
using INV.Elearning.Core.View;
using INV.Elearning.Core.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace INV.Elearning.Animations.UI.ViewModel
{
    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: AnimationsProxy.cs
    // Description: Xử lý các giá trị đang được lựa chọn của hiệu ứng
    // Develope by : Ta Khanh Thien
    // History:
    // 15/02/2018 : Create
    //---------------------------------------------------------------------------
    public class AnimationsProxy : RootViewModel
    {
        #region Hiệu ứng đang được lựa chọn (Animations)
        private Animation _animationSelected;
        /// <summary>
        /// Hiệu ứng đang được lựa chọn
        /// </summary>
        public Animation AnimationSelected
        {
            get { return _animationSelected ?? new MultiAnimation() { Name = "Multiple", Image = "/INV.Elearning.Animations.UI;component/Images/Multiple.png" }; }
            set { _animationSelected = value; OnPropertyChanged("AnimationSelected"); OnPropertyChanged("_animationSelected"); }
        }





        private Animation _animationExitSelected;
        /// <summary>
        /// Hiệu ứng Exit đang được lựa chọn
        /// </summary>
        public Animation AnimationExitSelected
        {
            get { return _animationExitSelected ?? new MultiAnimation() { Name = "Multiple", Duration = TimeSpan.FromSeconds(2), Image = "/INV.Elearning.Animations.UI;component/Images/Multiple.png" }; }
            set { _animationExitSelected = value; OnPropertyChanged("AnimationExitSelected"); }
        }

        private Animation _animationPathSelected;
        /// <summary>
        /// Hiệu ứng Path đang được lựa chọn
        /// </summary>
        public Animation AnimationPathSelected
        {
            get { return _animationPathSelected ?? new MultiAnimation() { Name = "Multiple", Image = "/INV.Elearning.Animations.UI;component/Images/Multiple.png" }; }
            set { _animationPathSelected = value; OnPropertyChanged("AnimationPathSelected"); }
        }

        private string _nameObjectPathSelected;
        /// <summary>
        /// Hiệu ứng Path đang được lựa chọn
        /// </summary>
        public string NameObjectPathSelected
        {
            get { return _nameObjectPathSelected; }
            set
            {
                if (value != "")
                {
                    var _listObject = (Application.Current as IAppGlobal)?.SelectedElements;
                    if (_listObject != null && _listObject.Count() == 1)
                    {
                        if (_listObject[0] is MotionPathObject)
                        {
                            (_listObject[0] as MotionPathObject).TargetName = value;

                        }
                        else if (_listObject[0] is ObjectElement)
                        {
                            if ((_listObject[0] as ObjectElement).MotionPaths?.Count() == 1)
                            {
                                ((_listObject[0] as ObjectElement).MotionPaths[0] as ObjectElement).TargetName = value;
                            }

                        }
                    }
                }
                _nameObjectPathSelected = value;
                OnPropertyChanged("NameObjectPathSelected");
            }
        }


        #endregion

        #region Gía trị Duration đang được chọn
        public double _durationEntranceSelected;
        /// <summary>
        /// Hiệu ứng đang được lựa chọn
        /// </summary>
        public double DurationEntranceSelected
        {
            get { return _durationEntranceSelected; }
            set
            {
                _durationEntranceSelected = value;
                if (!Double.IsNaN(value))
                {
                    //List<DurationAnimationUndoData> dataUndo = new List<DurationAnimationUndoData>();
                    TimeSpan duration = new TimeSpan(0, 0, 0, System.Convert.ToInt32(Math.Min(59.0, (double)(value - 0.5 + 0.00001))), (int)(((double)value * 1000) % 1000));
                    foreach (var item in (Application.Current as IAppGlobal).SelectedElements)
                    {
                        if (item is MotionPathObject)
                        {
                            if ((item as MotionPathObject).Owner is ObjectElement objectElement)
                            {
                                ((item as MotionPathObject).Owner as ObjectElement).EntranceAnimation.Duration = new TimeSpan();
                                //dataUndo.Add(new DurationAnimationUndoData(objectElement.EntranceAnimation, objectElement.EntranceAnimation.Duration, duration));
                                ((item as MotionPathObject).Owner as ObjectElement).EntranceAnimation.Duration = duration;
                            }
                        }
                        else if (item is ObjectElement objectElement)
                        {
                            (item as ObjectElement).EntranceAnimation.Duration = new TimeSpan();
                            //dataUndo.Add(new DurationAnimationUndoData(objectElement.EntranceAnimation, objectElement.EntranceAnimation.Duration, duration));
                            (item as ObjectElement).EntranceAnimation.Duration = duration;
                            //  (item as TestControl).EntranceAnimation = new MultiAnimation();

                        }
                    }
                    //dataUndo.Add(new DurationAnimationUndoData(ProxyAnimation as Animation, (ProxyAnimation as Animation).Duration, duration));
                    (ProxyAnimation as Animation).Duration = duration;
                    //Global.PushUndo(new DurationAnimationUndo(dataUndo));
                }

                OnPropertyChanged("DurationEntranceSelected");
            }
        }


        public  double _durationExitSelected;
        /// <summary>
        /// Hiệu ứng đang được lựa chọn
        /// </summary>
        public  double DurationExitSelected
        {
            get { return _durationExitSelected; }
            set
            {
                
                if (_durationExitSelected != value&&!Double.IsNaN(value))
                {
                    List<DurationAnimationUndoData> dataUndo = new List<DurationAnimationUndoData>();
                    foreach (var item in (Application.Current as IAppGlobal).SelectedElements)
                    {
                        if (item is MotionPathObject)
                        {
                            if ((item as MotionPathObject).Owner is ObjectElement objectElement)
                            {
                                ((item as MotionPathObject).Owner as ObjectElement).ExitAnimation.Duration = new TimeSpan();
                                TimeSpan duration = new TimeSpan(0, 0, 0, System.Convert.ToInt32(Math.Min(59.0, (double)(value - 0.5 + 0.0001))), System.Convert.ToInt32(((double)value * 1000) % 1000));
                                dataUndo.Add(new DurationAnimationUndoData(objectElement.ExitAnimation, objectElement.ExitAnimation.Duration, duration));
                                ((item as MotionPathObject).Owner as ObjectElement).ExitAnimation.Duration = duration;
                            }
                        }
                        else if (item is ObjectElement objectElement)
                        {
                            (item as ObjectElement).ExitAnimation.Duration = new TimeSpan();
                            TimeSpan duration = new TimeSpan(0, 0, 0, System.Convert.ToInt32(Math.Min(59.0, (double)(value - 0.5 + 0.0001))), System.Convert.ToInt32(((double)value * 1000) % 1000));
                            dataUndo.Add(new DurationAnimationUndoData(objectElement.ExitAnimation, objectElement.ExitAnimation.Duration, duration));
                            (item as ObjectElement).ExitAnimation.Duration = duration;

                        }
                    }
                    (ProxyAnimationExit as Animation).Duration = new TimeSpan(0, 0, 0, System.Convert.ToInt32(Math.Min(59.0, (double)(value - 0.5 + 0.0001))), System.Convert.ToInt32(((double)value * 1000) % 1000));
                    //Global.PushUndo(new DurationAnimationUndo(dataUndo));
                }
                _durationExitSelected = value;
                OnPropertyChanged("DurationExitSelected");
            }
        }


        public double _durationPathSelected;
        /// <summary>
        /// Hiệu ứng đang được lựa chọn
        /// </summary>
        public double DurationPathSelected
        {
            get { return _durationPathSelected; }
            set
            {
                if (_durationPathSelected!=value&&!Double.IsNaN(value))
                {
                    List<DurationAnimationUndoData> dataUndo = new List<DurationAnimationUndoData>();
                    foreach (var item in (Application.Current as IAppGlobal).SelectedElements)
                    {
                        if (item is MotionPathObject motionPathObject)
                        {
                            //(item as MotionPathObject).Animation.Duration = new TimeSpan();
                            TimeSpan duration = new TimeSpan(0, 0, 0, System.Convert.ToInt32(Math.Min(59.0, (double)(value - 0.5 + 0.0001))), System.Convert.ToInt32(((double)value * 1000) % 1000));
                            dataUndo.Add(new DurationAnimationUndoData(motionPathObject.Animation, motionPathObject.Animation.Duration, duration));
                            (item as MotionPathObject).Animation.Duration = duration;

                        }
                        else if (item is ObjectElement)
                        {
                            for (int k = 0; k < (item as ObjectElement).MotionPaths.Count; k++)
                            {
                                //(item as StandardElement).MotionPaths[k].Animation.Duration = new TimeSpan();
                                TimeSpan duration = new TimeSpan(0, 0, 0, System.Convert.ToInt32(Math.Min(59.0, (double)(value - 0.5 + 0.0001))), System.Convert.ToInt32(((double)value * 1000) % 1000));
                                dataUndo.Add(new DurationAnimationUndoData((item as ObjectElement).MotionPaths[k].Animation, (item as ObjectElement).MotionPaths[k].Animation.Duration, duration));
                                (item as ObjectElement).MotionPaths[k].Animation.Duration = duration;
                            }
                        }
                    }
                    (ProxyAnimationPath as Animation).Duration = new TimeSpan(0, 0, 0, System.Convert.ToInt32(Math.Min(59.0, (double)(value - 0.5 + 0.0001))), System.Convert.ToInt32(((double)value * 1000) % 1000));
                    Global.PushUndo(new DurationAnimationUndo(dataUndo));
                }
                _durationPathSelected = value;
                OnPropertyChanged("DurationPathSelected");
            }
        }


        private bool isEnableSpeed=true;
        /// <summary>
        /// Xét trạng thái của hiệu ứng Speed
        /// </summary>
        public bool IsEnableSpeed
        {
            get { return isEnableSpeed; }
            set { isEnableSpeed = value; OnPropertyChanged("IsEnableSpeed"); }
        }

        #endregion
        
        public Animation _animation;


        public object ProxyAnimation
        {
            get { return _animation; }
            set
            {
                if (value is Animation)
                {
                    _animation = value as Animation;
                    Animation _animationClone = null;
                    foreach (var item in (Application.Current as IAppGlobal).SelectedElements)
                    {
                        if (item is MotionPathObject)
                        {
                            _animationClone = CloneAnimation.GetAnimation(_animation.Name, _animation.IsSequence);

                            CloneAnimation.CopyAnimation(_animationClone, _animation);
                            (item as MotionPathObject).Owner.EntranceAnimation = _animationClone;
                        }
                        else if (item is ObjectElement)
                        {
                            _animationClone = CloneAnimation.GetAnimation(_animation.Name, _animation.IsSequence);

                            CloneAnimation.CopyAnimation(_animationClone, _animation);
                            (item as ObjectElement).EntranceAnimation = _animationClone;
                        }
                    }
                }
                OnPropertyChanged("ProxyAnimation");
            }
        }

        public Animation _animationExit;


        public object ProxyAnimationExit
        {
            get { return _animationExit; }
            set
            {
                if (value is Animation)
                {
                    _animationExit = value as Animation;
                    Animation _animationExitClone = null;
                    foreach (var item in (Application.Current as IAppGlobal).SelectedElements)
                    {
                        if (item is MotionPathObject)
                        {
                            _animationExitClone = CloneAnimation.GetAnimation(_animationExit.Name, _animationExit.IsSequence);
                            CloneAnimation.CopyAnimation(_animationExitClone, _animationExit);
                            (item as MotionPathObject).Owner.ExitAnimation = _animationExitClone;
                        }
                        else if (item is ObjectElement)
                        {
                            _animationExitClone = CloneAnimation.GetAnimation(_animationExit.Name, _animationExit.IsSequence);
                            CloneAnimation.CopyAnimation(_animationExitClone, _animationExit);
                            (item as ObjectElement).ExitAnimation = _animationExitClone;
                        }
                    }
                }
                OnPropertyChanged("ProxyAnimationExit");
            }
        }


        public Animation _animationPath;


        public object ProxyAnimationPath
        {
            get { return _animationPath; }
            set
            {
                if (value is Animation)
                {
                    _animationPath = value as Animation;
                    Animation _animationPathClone = null;
                    foreach (var item in (Application.Current as IAppGlobal).SelectedElements)
                    {
                        if (item is ObjectElement && !(item is MotionPathObject))
                        {
                            _animationPathClone = CloneAnimation.GetAnimation(_animationPath.Name, _animationPath.IsSequence);
                            CloneAnimation.CopyAnimation(_animationPathClone, _animationPath);
                           // if ((item as ObjectElement).MotionPaths.Count > 0)
                            //    CloneAnimation.CopyAnimation((item as ObjectElement).MotionPaths[(item as ObjectElement).MotionPaths.Count - 1].Animation as Animation, _animationPathClone);
                            //   (item as TestControl).MotionPaths[(item as TestControl).MotionPaths.Count-1] = _animationPathClone;
                        }
                    }
                }
                OnPropertyChanged("ProxyAnimationPath");
            }
        }
    }
}
