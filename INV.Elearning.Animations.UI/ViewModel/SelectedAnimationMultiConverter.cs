﻿using INV.Elearning.Animations.Enums;
using INV.Elearning.Animations.UI.Model;
using INV.Elearning.Core.Helper;
using INV.Elearning.Core.View;
using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace INV.Elearning.Animations.UI.ViewModel
{
    public class SelectedAnimationMultiConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {

            Animation t = new FadeAnimation();
            t = new FloatAnimation();
            t = new ShapeAnimation();

            AnimationObject animationObject = AnimationsViewModel.GetAnimationProperty();
            if (animationObject == null) return null;
            TimeSpan a = animationObject.EntranceAnimation.Duration;
            {
                if(animationObject!=null)
                {
                    Animation _animationSelected = null;
                    _animationSelected = AnimationsViewModel.SetAnimationSelected(animationObject.EntranceAnimation as Animation, _animationSelected,AnimationsViewModel.DataAnimate);
                    //Gán giá trị animation đang được lựa chọn
                    AnimationsViewModel.AnimationsProxy._animation= _animationSelected;
                    AnimationsViewModel.AnimationsProxy.AnimationSelected = _animationSelected;
                    if ((double)((TimeSpan)animationObject.EntranceAnimation.Duration).TotalSeconds > 0)
                        AnimationsViewModel.AnimationsProxy.DurationEntranceSelected = (double)((TimeSpan)animationObject.EntranceAnimation.Duration).TotalSeconds;
                    else
                        AnimationsViewModel.AnimationsProxy.DurationEntranceSelected = Double.NaN;
                 // AnimationsViewModel.AnimationsProxy.DurationEntranceSelected = Double.NaN;
                    return _animationSelected;
                }
            }
            return null;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return new object[] { Binding.DoNothing, value };
        }
        
    }

    public class SelectedAnimationExitMultiConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {

            var tc = new ShapeAnimation(TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/ExitAnimations/Shape.png", eAnimationType.Exit, true);
           // tc= new ShapeAnimation()
            AnimationObject animationObject = AnimationsViewModel.GetAnimationProperty();
            {
                if (animationObject != null)
                {
                    Animation _animationSelected = null;
                    _animationSelected = AnimationsViewModel.SetAnimationSelected(animationObject.ExitAnimation as Animation, _animationSelected,AnimationsViewModel.DataAnimateExit);
                    //Gán giá trị animation đang được lựa chọn
                    AnimationsViewModel.AnimationsProxy._animationExit = _animationSelected;
                    AnimationsViewModel.AnimationsProxy.AnimationExitSelected = _animationSelected;
                    if ((double)((TimeSpan)animationObject.ExitAnimation.Duration).TotalSeconds > 0)
                        AnimationsViewModel.AnimationsProxy.DurationExitSelected = (double)((TimeSpan)animationObject.ExitAnimation.Duration).TotalSeconds;
                    else
                        AnimationsViewModel.AnimationsProxy.DurationExitSelected = Double.NaN;
                    return _animationSelected;
                }
            }
            return null;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return new object[] { Binding.DoNothing, value };
        }
    }

    public class SelectedAnimationPathMultiConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {

            var _listObject = (Application.Current as IAppGlobal)?.SelectedElements;
            if (_listObject != null && _listObject.Count() == 1)
            {
                if (_listObject[0] is MotionPathObject)
                {
                    if ((_listObject[0] as MotionPathObject).TargetName == "")
                    {
                        AnimationsViewModel.AnimationsProxy.NameObjectPathSelected = (_listObject[0] as MotionPathObject).TargetName = (_listObject[0] as MotionPathObject).Animation.Name;
                    }
                    else
                    {
                        AnimationsViewModel.AnimationsProxy.NameObjectPathSelected = (_listObject[0] as MotionPathObject).TargetName;
                    }

                }
                else if (_listObject[0] is ObjectElement)
                {
                    if ((_listObject[0] as ObjectElement).MotionPaths?.Count() == 1)
                    {
                        if (((_listObject[0] as ObjectElement).MotionPaths[0] as ObjectElement).TargetName == "")
                        {
                            AnimationsViewModel.AnimationsProxy.NameObjectPathSelected = ((_listObject[0] as ObjectElement).MotionPaths[0] as ObjectElement).TargetName = (_listObject[0] as ObjectElement).MotionPaths[0].Animation.Name;
                        }
                        else
                        {
                            AnimationsViewModel.AnimationsProxy.NameObjectPathSelected = ((_listObject[0] as ObjectElement).MotionPaths[0] as ObjectElement).TargetName;
                        }
                    }
                    else
                    {
                        AnimationsViewModel.AnimationsProxy.NameObjectPathSelected = "";
                    }

                }
            }
            else
            {
                AnimationsViewModel.AnimationsProxy.NameObjectPathSelected = "";
            }

            AnimationObject animationObject = AnimationsViewModel.GetAnimationProperty();
            
            if (animationObject != null)
            {
                //if (animationObject.MotionPaths.Count == 0)
                //{
                //    AnimationsViewModel.AnimationsProxy._animationPath = new MultiAnimation(TimeSpan.FromSeconds(2), "/INV.Elearning.Animations.UI;component/Images/Multiple.png");
                //    AnimationsViewModel.AnimationsProxy.AnimationPathSelected = new MultiAnimation(TimeSpan.FromSeconds(2), "/INV.Elearning.Animations.UI;component/Images/Multiple.png");
                //    return null;
                //}

                //Animation _animationSelected = null;

                //_animationSelected = AnimationsViewModel.SetAnimationSelected(animationObject.MotionPaths[0].Animation as Animation, _animationSelected, AnimationsViewModel.DataAnimatePath);
                ////Gán giá trị animation đang được lựa chọn
                //AnimationsViewModel.AnimationsProxy._animationPath = _animationSelected;
                //AnimationsViewModel.AnimationsProxy.AnimationPathSelected = _animationSelected;
                //return _animationSelected;
                if(animationObject.AnimationPath is NoneAnimation)
                {
                    AnimationsViewModel.AnimationsProxy._animationPath = new MultiAnimation(TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Multiple.png");
                        AnimationsViewModel.AnimationsProxy.AnimationPathSelected = new MultiAnimation(TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Multiple.png");
                    AnimationsViewModel.AnimationsProxy.NameObjectPathSelected = "";
                    return null;
                }
                Animation _animationSelected = null;
                _animationSelected = AnimationsViewModel.SetAnimationSelected(animationObject.AnimationPath as Animation, _animationSelected, AnimationsViewModel.DataAnimatePath);
                //Gán giá trị animation đang được lựa chọn
                AnimationsViewModel.AnimationsProxy._animationPath = _animationSelected;
                AnimationsViewModel.AnimationsProxy.AnimationPathSelected = _animationSelected;
                if ((double)((TimeSpan)animationObject.AnimationPath.Duration).TotalSeconds > 0)
                    AnimationsViewModel.AnimationsProxy.DurationPathSelected = (double)((TimeSpan)animationObject.AnimationPath.Duration).TotalSeconds;
                else
                    AnimationsViewModel.AnimationsProxy.DurationPathSelected = Double.NaN;
                return _animationSelected;
            }

            return null;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return new object[] { Binding.DoNothing, value };
        }
    }

}
