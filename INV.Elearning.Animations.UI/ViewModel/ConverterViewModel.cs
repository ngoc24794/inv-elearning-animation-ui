﻿using INV.Elearning.Core.Helper;
using INV.Elearning.Core.View;
using INV.Elearning.Core.ViewModel;
using INV.Elearning.Text.Views;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace INV.Elearning.Animations.UI.ViewModel
{
    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: ConverterViewModel.cs
    // Description: Chuyển đổi các kiểu dữ liệu
    // Develope by : Ta Khanh Thien
    // History:
    // 20/02/2018 : Create
    //---------------------------------------------------------------------------

    public class ConverterViewModel:RootViewModel
    {
        private static CompareFormatValue _compareValueConverter;
        /// <summary>
        /// Hàm chuyển đổi dùng để so sánh các giá trị
        /// </summary>
        public static CompareFormatValue CompareValueConverter
        {
            get { return _compareValueConverter ?? (_compareValueConverter = new CompareFormatValue()); }
        }


        private static SelectedObject _isEnableConverter;
        /// <summary>
        /// Hàm chuyển đổi dùng để so sánh các giá trị
        /// </summary>
        public static SelectedObject IsEnableConverter
        {
            get { return _isEnableConverter ?? (_isEnableConverter = new SelectedObject()); }
        }



        private static IsEnableNamePathObject _isEnableNamePathConverter;
        /// <summary>
        /// Hàm chuyển đổi dùng để so sánh các giá trị
        /// </summary>
        public static IsEnableNamePathObject IsEnableNamePathConverter
        {
            get { return _isEnableNamePathConverter ?? (_isEnableNamePathConverter = new IsEnableNamePathObject()); }
        }

        private static DurationConverter _durationConverter;
        /// <summary>
        /// Hàm chuyển đổi dùng để so sánh các giá trị
        /// </summary>
        public static DurationConverter DurationConverter
        {
            get { return _durationConverter ?? (_durationConverter = new DurationConverter()); }
        }

        private static NameExitConverter _nameExitConverter;
        /// <summary>
        /// Hàm chuyển đổi dùng để so sánh các giá trị
        /// </summary>
        public static NameExitConverter NameExitConverter
        {
            get { return _nameExitConverter ?? (_nameExitConverter = new NameExitConverter()); }
        }


        private static IsEffectOptionAdapter _isEffectOptionAdapter;
        /// <summary>
        /// Kiểm tra xem có phải là AnimationAdapter hay không
        /// </summary>
        public static IsEffectOptionAdapter IsEffectOptionAdapter
        {
            get { return _isEffectOptionAdapter ?? (_isEffectOptionAdapter = new IsEffectOptionAdapter()); }
        }


        private static CompareAnimationPathValue _compareAnimationPathConverter;
        /// <summary>
        /// Hàm chuyển đổi dùng để so sánh các giá trị animationPath
        /// </summary>
        public static CompareAnimationPathValue CompareAnimationPathConverter
        {
            get { return _compareAnimationPathConverter ?? (_compareAnimationPathConverter = new CompareAnimationPathValue()); }
        }


        private static CompareVisibility _compareVisibilityConverter;
        /// <summary>
        /// Hàm chuyển đổi dùng để so sánh các giá trị Visibility
        /// </summary>
        public static CompareVisibility CompareVisibilityConverter
        {
            get { return _compareVisibilityConverter ?? (_compareVisibilityConverter = new CompareVisibility()); }
        }


        private static IsEnableSpeedConverter _isEnableSpeedConverter;
        /// <summary>
        /// Hàm chuyển đổi dùng để so sánh các giá trị 
        /// </summary>
        public static IsEnableSpeedConverter IsEnableSpeedConverter
        {
            get { return _isEnableSpeedConverter ?? (_isEnableSpeedConverter = new IsEnableSpeedConverter()); }
        }
        private static CompareIsSelectedPath _compareIsSelectedPath;
        /// <summary>
        /// Hàm chuyển đổi dùng để so sánh các giá trị Visibility
        /// </summary>
        public static CompareIsSelectedPath CompareIsSelectedPath
        {
            get { return _compareIsSelectedPath ?? (_compareIsSelectedPath = new CompareIsSelectedPath()); }
        }

        private static ImageConverter _imageConverter;
        /// <summary>
        /// Hàm dùng để xét image trong EffectAdapter
        /// </summary>
        public static ImageConverter ImageConverter
        {
            get { return _imageConverter ?? (_imageConverter = new ImageConverter()); }
        }

        private static NameDirectionEasingConverter _nameDirectionEasingConverter;
        /// <summary>
        /// Hàm dùng để xét image trong EffectAdapter
        /// </summary>
        public static NameDirectionEasingConverter NameDirectionEasingConverter
        {
            get { return _nameDirectionEasingConverter ?? (_nameDirectionEasingConverter = new NameDirectionEasingConverter()); }
        }



        private static CompareVisibilityEffectOption _compareVisibilityEffectOptionConverter;
        /// <summary>
        /// Hàm chuyển đổi dùng để so sánh các giá trị Visibility
        /// </summary>
        public static CompareVisibilityEffectOption CompareVisibilityEffectOptionConverter
        {
            get { return _compareVisibilityEffectOptionConverter ?? (_compareVisibilityEffectOptionConverter = new CompareVisibilityEffectOption()); }
        }



        private static CompareNotVisibility _compareNotVisibilityConverter;
        /// <summary>
        /// Hàm chuyển đổi dùng để so sánh các giá trị Visibility
        /// </summary>
        public static CompareNotVisibility CompareNotVisibilityConverter
        {
            get { return _compareNotVisibilityConverter ?? (_compareNotVisibilityConverter = new CompareNotVisibility()); }
        }
    }

    /// <summary>
    /// Hàm so sánh giá trị đầu vào và parameter
    /// </summary>
    public class CompareFormatValue : IValueConverter
    {
        /// <summary>
        /// Chuyển đổi thuận
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value is MultiAnimation|| value is NoneAnimation)
            {
                return false;
            }
            if(value is BounceAnimation)
            {
                var _listObject = (Application.Current as IAppGlobal)?.SelectedElements;
                if (_listObject != null && _listObject.Count > 0)
                {
                    foreach (var item in _listObject)
                    {
                        if ((item is TextEditor) || (item is MotionPathObject && ((item as MotionPathObject).Owner is TextEditor)))
                        {
                            return true;
                        }
                    }
                    
                }
                return false;
            }
            return true;
        }

        /// <summary>
        /// Chuyển đổi ngược
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }


    public class SelectedObject : IValueConverter
    {
        /// <summary>
        /// Chuyển đổi thuận
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is ObjectElement) return true;
            
            return false;
        }

        /// <summary>
        /// Chuyển đổi ngược
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }


    /// <summary>
    /// Hàm chuyển đổi giá trị Duration 
    /// </summary>
    public class DurationConverter : IValueConverter
    {
        /// <summary>
        /// Chuyển đổi thuận
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
          
            if (value!=null&&value is TimeSpan)
            {
               //t = (double)((TimeSpan)value).TotalSeconds;
                  return (double)((TimeSpan)value).TotalSeconds;
            }
            return 0;
        }

        /// <summary>
        /// Chuyển đổi ngược
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var _testControl = (Application.Current as IAppGlobal).SelectedElements;

            int _countObject = 0;
            _countObject = _testControl.Count();
            //Gán giá trị animation cho đối tượng
            
            int t=System.Convert.ToInt32((double)value);
            for (int i = 0; i < _countObject; i++)
            {

                if (_testControl.ElementAt(i) is StandardElement)
                {

                    (_testControl.ElementAt(i) as StandardElement).EntranceAnimation.Duration = new TimeSpan(0, 0, System.Convert.ToInt32(Math.Min(59.0,(double)value)));
                    (_testControl.ElementAt(i) as StandardElement).EntranceAnimation = null;
                    int y = 0;
                }
                
            }

            return Binding.DoNothing;
        }
    }
    /// <summary>
    /// Hàm xét giá trị đang được lựa chọn của hiệu ứng motionPath
    /// </summary>
    public class CompareIsSelectedPath : IValueConverter
    {
        /// <summary>
        /// Chuyển đổi thuận
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //return true;
            if (value == null) return false;
            if(AnimationsViewModel.AnimationsProxy.AnimationPathSelected.EffectOptions!=null)
                foreach (var item in AnimationsViewModel.AnimationsProxy.AnimationPathSelected.EffectOptions)
                {
                    if (item.Values?.Count>0)
                    {
                      
                        if (item.Values[0] is EffectOptionAdapter)
                        {
                            
                            foreach (var effectOptionAdapter in item.Values)
                            {
                                //Thay đổi giá trị IsSelected của các EffectOptions
                                if (effectOptionAdapter is EffectOptionAdapter)
                                {
                                    foreach (var effectOption in (effectOptionAdapter as EffectOptionAdapter).Values)
                                    {
                                        
                                        if ((effectOption as EffectOption).Name == value.ToString()&& (effectOption as EffectOption).IsSelected==true)
                                        {
                                            return true;
                                        }
                                    }
                                }
                            }
                        }

                        else
                        {
                            //Thay đổi giá trị IsSelected của các EffectOptions
                            for (int j = 0; j < item.Values.Count; j++)
                            {
                                if (item.Values[j].Name == value.ToString() && item.Values[j].IsSelected == true)
                                {
                                    return true;
                                }
                            }
                        }
                    }
                }


            return false;
        }

        /// <summary>
        /// Chuyển đổi ngược
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
    /// <summary>
    /// Hàm chuyển đổi giá trị Duration 
    /// </summary>
    public class NameExitConverter : IValueConverter
    {
        /// <summary>
        /// Chuyển đổi thuận
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                if (parameter?.ToString() == "Entrance")
                {
                    switch (value.ToString())
                    {
                        case "Spin&Grow":
                            return AnimationsViewModel.GetNameViewTransition("Spin")+" & "+ AnimationsViewModel.GetNameViewTransition("Grow");
                        case "Grow&Spin":
                            return AnimationsViewModel.GetNameViewTransition("Grow") + " & " + AnimationsViewModel.GetNameViewTransition("Spin");
                            //case "FlyIn":
                            //    return "Fly In";
                            //case "FloatIn":
                            //    return "Float In";
                            //case "RandomBars":
                            //    return "Randoms Bars";
                    }
                }
                else
                {
                    switch (value.ToString())
                    {
                        case "Grow":
                            return AnimationsViewModel.GetNameViewTransition("Shrink");
                        case "Spin&Grow":
                            return AnimationsViewModel.GetNameViewTransition("Spin")+" & " + AnimationsViewModel.GetNameViewTransition("Shrink");
                        case "Grow&Spin":
                            return AnimationsViewModel.GetNameViewTransition("Shrink") + " & " + AnimationsViewModel.GetNameViewTransition("Turn");
                            //case "FlyOut":
                            //    return "Fly Out";
                            //case "FloatOut":
                            //    return "Float Out";
                            //case "RandomBars":
                            //    return "Randoms Bars";
                    }
                }
                
            }
            if(value?.ToString()=="In & Out")
            {
                return AnimationsViewModel.GetNameViewTransition("In") + " & " + AnimationsViewModel.GetNameViewTransition("Out");
            }
            var name = AnimationsViewModel.GetNameViewTransition(value.ToString());
            if (name != "") return name;
            return value;
            
        }

        /// <summary>
        /// Chuyển đổi ngược
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //var _testControl = (Application.Current as IAppGlobal).SelectedElements;

            //int _countObject = 0;
            //_countObject = _testControl.Count();
            ////Gán giá trị animation cho đối tượng

            //int t = System.Convert.ToInt32((double)value);
            //for (int i = 0; i < _countObject; i++)
            //{

            //    if (_testControl.ElementAt(i) is StandardElement)
            //    {

            //        (_testControl.ElementAt(i) as StandardElement).EntranceAnimation.Duration = new TimeSpan(0, 0, System.Convert.ToInt32(Math.Min(59.0, (double)value)));
            //        (_testControl.ElementAt(i) as StandardElement).EntranceAnimation = null;
            //        int y = 0;
            //    }

            //}

            return Binding.DoNothing;
        }
    }

    public class IsEnableNamePathObject : IValueConverter
    {
        /// <summary>
        /// Chuyển đổi thuận
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var _listObject = (Application.Current as IAppGlobal)?.SelectedElements;
            if (_listObject != null && _listObject.Count() == 1)
            {
                if (_listObject[0] is MotionPathObject)
                {
                    return true;
                }
                if (_listObject[0] is ObjectElement)
                {
                    if ((_listObject[0] as ObjectElement).MotionPaths.Count() == 1) return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Chuyển đổi ngược
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }

    /// <summary>
    /// Lớp kiểm tra xem có phải AnimaitonAdapter không
    /// </summary>
    public class IsEffectOptionAdapter : IValueConverter
    {
        /// <summary>
        /// Chuyển đổi thuận
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is EffectOptionAdapter) return true;
            return false;
        }

        /// <summary>
        /// Chuyển đổi ngược
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }


    /// <summary>
    /// Hàm so sánh giá trị animationPath
    /// </summary>
    public class CompareAnimationPathValue : IValueConverter
    {
        /// <summary>
        /// Chuyển đổi thuận
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
           
            return AnimationsViewModel.IsEnabledEffectOptionPath();
        }

        /// <summary>
        /// Chuyển đổi ngược
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }

    /// <summary>
    /// Hàm kiểm tra trạng thái IsEnable trong hiệu ứng đường path
    /// </summary>
    public class IsEnableSpeedConverter : IValueConverter
    {

        private static bool checkNoneDirection=true;
        /// <summary>
        /// Chuyển đổi thuận
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is List<IEffectOption> && (value as List<IEffectOption>).Count > 0 && (value as List<IEffectOption>)[0].GroupName == "DirectionEasingPath")
            {
                if ((value as List<IEffectOption>)[0].IsSelected == true)
                    checkNoneDirection = false;
                else
                    checkNoneDirection = true;
            }
            if (value is List<IEffectOption> && (value as List<IEffectOption>).Count > 0 && (value as List<IEffectOption>)[0].GroupName == "Speed" && checkNoneDirection == false)
            {
                return false;
            }


            return true;
        }

        /// <summary>
        /// Chuyển đổi ngược
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
    /// <summary>
    /// Hàm so sánh giá trị đầu vào và parameter
    /// </summary>
    public class CompareVisibility : IValueConverter
    {
        /// <summary>
        /// Chuyển đổi thuận
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value?.ToString() == "None"&& parameter?.ToString()=="Entrance" && AnimationsViewModel.AnimationsProxy.AnimationSelected?.Name == "FlyIn") return Visibility.Collapsed;
            if (value?.ToString() == "None" && parameter?.ToString() == "Exit" && AnimationsViewModel.AnimationsProxy.AnimationExitSelected?.Name == "FlyOut") return Visibility.Collapsed;
            if (value?.ToString() != "Enter"&& value.ToString() != "Exit" && value.ToString() != "Direction" && value.ToString() != "DirectionEasingPath" && value.ToString() != "Speed"&& value.ToString() != "Sequence") return Visibility.Visible;
            
            return Visibility.Collapsed;
        }

        /// <summary>
        /// Chuyển đổi ngược
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
   


    /// <summary>
    /// Hàm dùng để xét tên DiẻctionEasing trong hiệu ứng đường path
    /// </summary>
    public class NameDirectionEasingConverter : IValueConverter
    {
        /// <summary>
        /// Chuyển đổi thuận
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return value;
            if (value.ToString() == "DirectionEasingPath")
                return AnimationsViewModel.GetNameViewTransition("Direction");
            if (value.ToString() == "Enter")
                return AnimationsViewModel.GetNameViewTransition("Exit");
            
                return AnimationsViewModel.GetNameViewTransition(value.ToString());
            
        }

        /// <summary>
        /// Chuyển đổi ngược
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }

    /// <summary>
    /// Hàm dùng để xét image trong EffectAdapter
    /// </summary>
    public class ImageConverter : IValueConverter
    {
        /// <summary>
        /// Chuyển đổi thuận
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch (value.ToString())
            {
                case "Enter":
                    return "/INV.Elearning.Animations.UI;component/Images/Enter.png";
                case "Exit":
                    return "/INV.Elearning.Animations.UI;component/Images/Enter.png";
                case "Speed":
                    return "/INV.Elearning.Animations.UI;component/Images/VeryFast.png";
                case "DirectionEasingPath":
                    return "/INV.Elearning.Animations.UI;component/Images/RelativeStartPoint.png";
              

            }
            return"/INV.Elearning.Animations.UI;component/Images/Enter.png";
        }

        /// <summary>
        /// Chuyển đổi ngược
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }



    /// <summary>
    /// Hàm so sánh giá trị đầu vào và parameter
    /// </summary>
    public class CompareVisibilityEffectOption : IValueConverter
    {
        /// <summary>
        /// Chuyển đổi thuận
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var _listObject = (Application.Current as IAppGlobal)?.SelectedElements;
            if (_listObject != null&& _listObject.Count > 0)
            {
                foreach (var item in _listObject)
                {
                    if((!(item is TextEditor)&&!(item is MotionPathObject))||((item is MotionPathObject)&&(!((item as MotionPathObject).Owner is TextEditor))))
                    {
                        if (value.ToString() == "Sequence") return Visibility.Collapsed;
                        return Visibility.Visible;
                    }
                }
                if (value.ToString() == "Sequence") return Visibility.Visible;
            }
            return Visibility.Visible;
        }

        /// <summary>
        /// Chuyển đổi ngược
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }


    /// <summary>
    /// Hàm so sánh giá trị đầu vào và parameter
    /// </summary>
    public class CompareNotVisibility : IValueConverter
    {
        /// <summary>
        /// Chuyển đổi thuận
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value.ToString() != "Enter"&& value.ToString() != "Exit" && value.ToString() != "DirectionEasingPath" && value.ToString() != "Direction" && value.ToString() != "Speed") return Visibility.Collapsed;
            return Visibility.Visible;
        }

        /// <summary>
        /// Chuyển đổi ngược
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }


    /// <summary>
    /// Hàm so sánh giá trị đầu vào và parameter<br/>
    /// Sẽ trả về <see cref="false"/> nếu 2 giá trị so sánh giống nhau và ngược lại
    /// </summary>
    public class InCompareFormatValue : IValueConverter
    {
        /// <summary>
        /// Chuyển đổi thuận
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || parameter == null)
                return true;

            return value.ToString() != parameter.ToString();
        }

        /// <summary>
        /// Chuyển đổi ngược
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
