﻿using INV.Elearing.Controls.Shapes;
using INV.Elearning.Core.Helper;
using INV.Elearning.Core.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace INV.Elearning.Animations.UI
{
    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: AnimationUIHelper.cs
    // Description: 'Ngôi sao hiệu ứng' xác định một đối tượng đang sở hữu Animation
    // Develope by : Nguyen Van Ngoc
    // History:
    // 01/01/2018 : Create
    //---------------------------------------------------------------------------
    public class AnimationUIHelper
    {
        public static MotionPathObject GenerateObject(ShapeBase shape)
        {
            MotionPathObject obj = new MotionPathObject
            {
                Content = shape,
                Width = shape.Width,
                Height = shape.Height
            };

            Binding bindWidth = new Binding()
            {
                Path = new PropertyPath("Width"),
                Source = obj
            };

            Binding bindHeight = new Binding()
            {
                Path = new PropertyPath("Height"),
                Source = obj
            };

            Binding bindIsSelected = new Binding()
            {
                Path = new PropertyPath("IsSelected"),
                Source = obj
            };

            BindingOperations.SetBinding(shape, ShapeBase.WidthProperty, bindWidth);
            BindingOperations.SetBinding(shape, ShapeBase.HeightProperty, bindHeight);
            BindingOperations.SetBinding(shape, ShapeBase.IsSelectedProperty, bindIsSelected);

            obj.Content = shape;

            return obj;
        }

        public static ObjectElement FindElement(LayoutBase layer, string Id)
        {
            foreach (ObjectElement item in layer.Elements)
            {
                if (item.ID == Id)
                {
                    return item;
                }
            }

            return null;
        }

        internal static LayoutBase GetLayerContainer(ObjectElement objectElement)
        {
            if (objectElement != null && (Application.Current as IAppGlobal).DocumentControl?.Slides != null)
            {
                foreach (SlideBase slide in (Application.Current as IAppGlobal).DocumentControl.Slides)
                {
                    if (slide.MainLayout?.Elements?.Contains(objectElement) == true)
                    {
                        return slide.MainLayout;
                    }
                    if (slide.Layouts != null)
                    {
                        foreach (LayoutBase layer in slide.Layouts)
                        {
                            if (layer.Elements?.Contains(objectElement) == true)
                            {
                                return layer;
                            }
                        }
                    }
                }
            }
            return null;
        }

        public static PathGeometry Reverse(PathGeometry pathGeometry)
        {
            if (pathGeometry.IsEmpty())
            {
                return PathGeometry.CreateFromGeometry(Geometry.Empty);
            }

            PathFigureCollection pathFigures = new PathFigureCollection();
            foreach (PathFigure pathFigure in pathGeometry.Figures)
            {
                PathFigureManager manager = new PathFigureManager(pathFigure);
                PathFigure reversedFigure = manager.Reverse();
                pathFigures.Add(reversedFigure);
            }

            PathGeometry result = new PathGeometry()
            {
                Figures = pathFigures,
                FillRule = pathGeometry.FillRule
            };
            return result;
        }


    }

    internal class PathFigureManager
    {
        private PathFigure _figure;
        public PathFigureManager(PathFigure figure)
        {
            _figure = figure;
        }

        public PathSegmentType GetType(PathSegment pathSegment)
        {
            if (pathSegment == null)
            {
                return PathSegmentType.LineSegment;
            }

            Type type = pathSegment.GetType();
            if (type == typeof(ArcSegment)) return PathSegmentType.ArcSegment;
            if (type == typeof(BezierSegment)) return PathSegmentType.BezierSegment;
            if (type == typeof(PolyLineSegment)) return PathSegmentType.PolyLineSegment;
            if (type == typeof(PolyBezierSegment)) return PathSegmentType.PolyBezierSegment;
            if (type == typeof(PolyQuadraticBezierSegment)) return PathSegmentType.PolyQuadraticBezierSegment;
            if (type == typeof(QuadraticBezierSegment)) return PathSegmentType.QuadraticBezierSegment;
            return PathSegmentType.LineSegment;
        }

        public PathFigure Reverse()
        {
            if (_figure == null || _figure.Segments == null || _figure.Segments.Count == 0)
            {
                return _figure;
            }

            Point startPoint = new Point();
            Point[] points = new Point[_figure.Segments.Count];
            int i = _figure.Segments.Count - 1;
            points[i] = _figure.StartPoint;
            while (i > -1)
            {
                PathSegment pathSegment = _figure.Segments[i];
                if (i == 0)
                {
                    startPoint = GetPoint(pathSegment);
                }
                else
                {
                    points[i] = GetPoint(pathSegment);
                }
                i--;
            }

            PathSegmentCollection pathSegments = new PathSegmentCollection();
            i = 0;
            while (i < _figure.Segments.Count)
            {
                PathSegment pathSegment = _figure.Segments[i];
                PathSegmentType type = GetType(pathSegment);
                Point point = points[i];
                switch (type)
                {
                    case PathSegmentType.LineSegment:
                        LineSegment lineSegment = new LineSegment(point, true);
                        pathSegments.Add(lineSegment);
                        break;
                    case PathSegmentType.ArcSegment:
                        ArcSegment arcSegment = pathSegment as ArcSegment;
                        pathSegments.Add(
                            new ArcSegment(
                                point,
                                arcSegment.Size,
                                arcSegment.RotationAngle,
                                arcSegment.IsLargeArc,
                                arcSegment.SweepDirection == SweepDirection.Clockwise ? SweepDirection.Counterclockwise : SweepDirection.Clockwise,
                                true));
                        break;
                    case PathSegmentType.BezierSegment:
                        BezierSegment bezierSegment = pathSegment as BezierSegment;
                        pathSegments.Add(
                            new BezierSegment(
                                bezierSegment.Point1,
                                bezierSegment.Point2,
                                point,
                                true
                                ));
                        break;
                    case PathSegmentType.PolyLineSegment:
                        PolyLineSegment polyLineSegment = pathSegment as PolyLineSegment;
                        pathSegments.Add(new PolyLineSegment(Reverse(polyLineSegment.Points), true));
                        break;
                    case PathSegmentType.PolyBezierSegment:
                        PolyBezierSegment polyBezierSegment = pathSegment as PolyBezierSegment;
                        pathSegments.Add(new PolyBezierSegment(Reverse(polyBezierSegment.Points), true));
                        break;
                    case PathSegmentType.PolyQuadraticBezierSegment:
                        PolyQuadraticBezierSegment polyQuadraticBezierSegment = pathSegment as PolyQuadraticBezierSegment;
                        pathSegments.Add(new PolyQuadraticBezierSegment(Reverse(polyQuadraticBezierSegment.Points), true));
                        break;
                    case PathSegmentType.QuadraticBezierSegment:
                        QuadraticBezierSegment quadraticBezierSegment = pathSegment as QuadraticBezierSegment;
                        pathSegments.Add(new QuadraticBezierSegment(quadraticBezierSegment.Point1, point, true));
                        break;
                }
                i++;
            }
            PathFigure result = new PathFigure()
            {
                IsFilled = _figure.IsFilled,
                IsClosed = _figure.IsClosed,
                StartPoint = startPoint,
                Segments = pathSegments
            };
            return result;
        }

        private IEnumerable<Point> Reverse(PointCollection points)
        {
            PointCollection result = new PointCollection();
            for (int i = points.Count - 1; i > -1; i--)
            {
                result.Add(points[i]);
            }
            return result;
        }

        private Point GetPoint(PathSegment pathSegment)
        {
            PathSegmentType type = GetType(pathSegment);
            switch (type)
            {
                case PathSegmentType.ArcSegment: return (pathSegment as ArcSegment).Point;
                case PathSegmentType.BezierSegment: return (pathSegment as BezierSegment).Point3;
                case PathSegmentType.PolyLineSegment: return (pathSegment as PolyLineSegment).Points.LastOrDefault();
                case PathSegmentType.PolyBezierSegment: return (pathSegment as PolyBezierSegment).Points.LastOrDefault();
                case PathSegmentType.PolyQuadraticBezierSegment: return (pathSegment as PolyQuadraticBezierSegment).Points.LastOrDefault();
                case PathSegmentType.QuadraticBezierSegment: return (pathSegment as QuadraticBezierSegment).Point2;
            }
            return (pathSegment as LineSegment).Point;
        }
    }

    internal enum PathSegmentType
    {
        LineSegment,
        ArcSegment,
        BezierSegment,
        PolyLineSegment,
        PolyBezierSegment,
        PolyQuadraticBezierSegment,
        QuadraticBezierSegment
    }

    public static class TranformExtension
    {
        public static PathGeometry Transform(this Transform transform, PathGeometry pathGeometry)
        {
            PathFigureCollection pathFigureCollection = new PathFigureCollection();
            foreach (PathFigure figure in pathGeometry.Figures)
            {
                PathSegmentCollection pathSegmentCollection = new PathSegmentCollection();
                foreach (PathSegment item in figure.Segments)
                {
                    if (item is LineSegment line)
                    {
                        pathSegmentCollection.Add(new LineSegment(transform.Transform(line.Point), line.IsStroked));
                    }
                    else if (item is PolyLineSegment polyline)
                    {
                        pathSegmentCollection.Add(new PolyLineSegment(transform.Transform(polyline.Points), polyline.IsStroked));
                    }
                    else if (item is ArcSegment arc)
                    {
                        pathSegmentCollection.Add(new ArcSegment(transform.Transform(arc.Point), arc.Size, arc.RotationAngle, arc.IsLargeArc, arc.SweepDirection, arc.IsStroked));
                    }
                    else if (item is BezierSegment curve)
                    {
                        pathSegmentCollection.Add(new BezierSegment(transform.Transform(curve.Point1), transform.Transform(curve.Point2), transform.Transform(curve.Point3), curve.IsStroked));
                    }
                    else if (item is PolyBezierSegment polyBezier)
                    {
                        pathSegmentCollection.Add(new PolyBezierSegment(transform.Transform(polyBezier.Points), polyBezier.IsStroked));
                    }
                    else if (item is PolyQuadraticBezierSegment polyQuadraticBezier)
                    {
                        pathSegmentCollection.Add(new PolyQuadraticBezierSegment(transform.Transform(polyQuadraticBezier.Points), polyQuadraticBezier.IsStroked));
                    }
                    else if (item is QuadraticBezierSegment quadraticBezier)
                    {
                        pathSegmentCollection.Add(new QuadraticBezierSegment(transform.Transform(quadraticBezier.Point1), transform.Transform(quadraticBezier.Point2), quadraticBezier.IsStroked));
                    }
                }
                PathFigure pathFigure = new PathFigure()
                {
                    StartPoint = transform.Transform(figure.StartPoint),
                    IsClosed = figure.IsClosed,
                    IsFilled = figure.IsFilled,
                    Segments = pathSegmentCollection
                };
                pathFigureCollection.Add(pathFigure);
            }
            PathGeometry result = new PathGeometry()
            {
                Figures = pathFigureCollection
            };
            return result;
        }

        public static PointCollection Transform(this Transform transform, PointCollection pointCollection)
        {
            PointCollection result = new PointCollection();
            foreach (Point point in pointCollection)
            {
                result.Add(transform.Transform(point));
            }
            return result;
        }
    }

}
