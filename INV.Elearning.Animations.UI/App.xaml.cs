﻿using INV.Elearning.Core.Helper;
using INV.Elearning.Core.Model.Theme;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows;
using INV.Elearning.Core.View;
using System.Collections.ObjectModel;

namespace INV.Elearning.Animations.UI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application, INotifyPropertyChanged, IAppGlobal
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private ETheme _selectedTheme = null;
        /// <summary>
        /// Lấy hoặc cài đặt Theme đang được lựa chọn
        /// </summary>
        public ETheme SelectedTheme
        {
            get
            {
                return _selectedTheme;
            }

            set
            {
                _selectedTheme = value;
                OnPropertyChanged("SelectedTheme");
            }
        }

        private string _elearningAppDataFolder;
        /// <summary>
        /// Đường dẫn thư mục chứa các nội dung cho chương trình<br/>
        /// Ví dụ: các tập tin theme, hình ảnh ....
        /// </summary>
        public string ElearningAppDataFolder
        {
            get
            {
                if (string.IsNullOrEmpty(_elearningAppDataFolder))
                    _elearningAppDataFolder = $@"{Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)}\Invico"; //Tạo một thư mục chứa các Resource cho chương trình

                if (!Directory.Exists(_elearningAppDataFolder))
                    Directory.CreateDirectory(_elearningAppDataFolder);

                return _elearningAppDataFolder;
            }
        }

        private string _documentTempFolder;
        /// <summary>
        /// Thư mục chứa các nội dung tạm của bài soạn thảo.<br/>
        /// Sẽ được xóa sau mỗi lần tắt chương trình
        /// </summary>
        public string DocumentTempFolder
        {
            get
            {
                if (string.IsNullOrEmpty(_documentTempFolder))
                    _documentTempFolder = $@"{Path.GetTempPath()}\Invico\{Path.GetRandomFileName()}"; //Tạo một thư mục ngẫu nhiên chứa các tập tin tạm

                if (!Directory.Exists(_documentTempFolder))
                    Directory.CreateDirectory(_documentTempFolder);
                return _documentTempFolder;
            }
        }


        private string _textureFolder;
        /// <summary>
        /// Thư mục chứa nội dung
        /// </summary>
        public string Texture
        {
            get
            {
                if (string.IsNullOrEmpty(_textureFolder))
                    _textureFolder = $@"{ElearningAppDataFolder}\Resources\Texture";

                if (!Directory.Exists(_textureFolder))
                    Directory.CreateDirectory(_textureFolder);
                return _textureFolder;
            }
        }

        private double _documentPageScale = 1.0; //Giá trị mặc định là 100%
        /// <summary>
        /// Tỷ lệ thu phóng của trang soạn thảo hiện tại
        /// </summary>
        public double DocumentPageScale
        {
            get { return _documentPageScale; }
            set { _documentPageScale = value; OnPropertyChanged("DocumentPageScale"); }
        }


        private ObservableCollection<ObjectElement> _selectedItems;
        /// <summary>
        /// Danh sách các đối tượng đang được chọn
        /// </summary>
        public ObservableCollection<ObjectElement> SelectedElements
        {
            get
            {
                if (_selectedItems == null)
                {
                    _selectedItems = new ObservableCollection<ObjectElement>();
                    _selectedItems.CollectionChanged += SelectedItems_Changed;
                }
                return _selectedItems;
            }
        }

        /// <summary>
        /// Đối tượng đang được lựa chọn. 
        /// Đây là phần tử cuối của danh sách các đối tượng đang được chọn <see cref="SelectedElements"/>
        /// </summary>
        public ObjectElement SelectedItem
        {
            get { return SelectedElements.LastOrDefault(); }
            set
            {
                if (!SelectedElements.Contains(value))
                {
                    SelectedElements.Add(value);
                }
                OnPropertyChanged("SelectedItem");
            }
        }

        /// <summary>
        /// Giá trị binding
        /// </summary>
        public object BindingSupportValue
        {
            get => SelectedElements;
            set
            {
                foreach (var item in this.SelectedElements)
                {
                    item.SetValue((value as BindingValue).DependencyProperty, (value as BindingValue).Value);
                }
                OnPropertyChanged("BindingSupportValue");
            }
        }

        /// <summary>
        /// Thay đổi nội dung danh sách
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedItems_Changed(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            OnPropertyChanged("SelectedElements");
            OnPropertyChanged("BindingSupportValue");
        }

        SlideBase _selectedSlide = null;
        /// <summary>
        /// Slide đang được chọn để thao tác
        /// </summary>
        public SlideBase SelectedSlide
        {
            get
            {
                return _selectedSlide;
            }

            set
            {
                _selectedSlide = value;
                OnPropertyChanged("SelectedSlide");
            }
        }

        Size _slideSize = new Size(1024, 576);
        /// <summary>
        /// Kích thước của trang soạn thảo
        /// </summary>
        public Size SlideSize
        {
            get
            {
                return _slideSize;
            }

            set
            {
                _slideSize = value;
                OnPropertyChanged("SlideSize");
            }
        }

        LenghtUnit _lenghtUnit = LenghtUnit.Px;
        /// <summary>
        /// Đơn vị tính kích thước
        /// </summary>
        public LenghtUnit LenghtUnit
        {
            get
            {
                return _lenghtUnit;
            }

            private set
            {
                _lenghtUnit = value;
                OnPropertyChanged("LenghtUnit");
            }
        }

        public ShowConfig ShowConfig => null;

        /// <summary>
        /// Thoát ứng dụng
        /// </summary>
        /// <param name="e"></param>
        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);
            try
            {
                Directory.Delete(DocumentTempFolder, true);
            }
            catch
            {

            }
        }

        #region Hiển thị nội dung gạch chân các Keyboard Access
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern int SystemParametersInfo(int uAction, int uParam, int lpvParam, int fuWinIni);
        private const int SPI_SETKEYBOARDCUES = 4107;
        private const int SPIF_SENDWININICHANGE = 2;
        #endregion
    }
}
