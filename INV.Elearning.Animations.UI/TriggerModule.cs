﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  TriggerModule.cs
**
** Description: Lớp mô tả các thông tin ghép nối của mô đun trigger lên chương trình chính
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
* 28 G
===========================================================*/

using INV.Elearning.Controls;
using INV.Elearning.Core;
using INV.Elearning.Core.View;
using INV.Elearning.Core.ViewModel;
using System;
using System.Collections.Generic;
using System.Windows;
using System.ComponentModel.Composition;

namespace INV.Elearning.Animations.UI
{
    /// <summary>
    /// Lớp mô tả các thông tin ghép nối của mô đun trigger lên chương trình chính
    /// </summary>
    [ModuleInfo("Mô đun trigger", "Hương Việt Group", "Quản lý trigger", "1.0.0.0")]
    [Export(typeof(ModuleBase))]
    [ExportMetadata("ModuleMetadata", null)]
    [ExportMetadata("ModuleDataType", typeof(INV.Elearning.Animations.MotionPathInfoBase))]
    public class TriggerModule : ModuleBase
    {
        private RibbonContextualTabGroup _contextureGroup;
        /// <summary>
        /// Thẻ gom nhóm các thẻ định dạng của đối tượng
        /// </summary>
        public override RibbonContextualTabGroup ContextualGroup
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// Kiểu dữ liệu quản lý
        /// </summary>
        public override Type DataType => typeof(MotionPathInfo);

        private ModuleInfo _moduleInfo;
        /// <summary>
        /// Thông tin về mô đun
        /// </summary>
        public override ModuleInfo ModuleInfo
        {
            get => _moduleInfo ?? (_moduleInfo = new ModuleInfo());
        }

        /// <summary>
        /// Các panel ở các vị trí trái hoặc phải, phục vụ cho định dạng
        /// </summary>
        public override IEnumerable<PanelItem> Panels => null;


        List<RibbonTabItem> _ribbtonTabs;
        /// <summary>
        /// Danh sách các thẻ dùng chung của hệ thống
        /// </summary>
        public override IEnumerable<RibbonTabItem> RibbonTabItems
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// Danh sách các menuItem riêng cho đối tượng
        /// </summary>
        public override IEnumerable<System.Windows.Controls.MenuItem> MenuItems => new List<MenuItem>();

        /// <summary>
        /// Đường dẫn lưu trữ ngôn ngữ
        /// </summary>
        public override string LanguagePackageUri => throw new NotImplementedException();

        /// <summary>
        /// Thông tin kiểu đối tượng mô đun quản lý
        /// </summary>
        public override Type ObjectType => null;

        private List<FrameworkElement> _insertButtons;
        /// <summary>
        /// Danh sách các nút bấm chèn đối tượng
        /// </summary>
        public override IEnumerable<FrameworkElement> InsertButtons
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// Tên nhóm chứa các nút bấm
        /// </summary>
        public override string InsertGroupBoxName => "";

        /// <summary>
        /// Hàm hủy
        /// </summary>
        public override void Dispose()
        {

        }

        /// <summary>
        /// Hàm cài đặt các thông số
        /// </summary>
        public override void Initialize()
        {

        }

        /// <summary>
        /// Hàm tải lại đối tượng quản lý
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public override ObjectElement Load(object data)
        {           
            return null;
        }
    }
}
