﻿using INV.Elearing.Controls.Shapes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;

namespace INV.Elearning.Animations.UI
{
    public class MoveThumb : ControlThumbBase
    {
        public MoveThumb(LinePath adorned) : base(adorned)
        {
            Adorned = adorned;
            DragDelta += OnDragDelta;
            Cursor = Cursors.Hand;
            Template = null;
        }
        protected override void OnDragDelta(object sender, DragDeltaEventArgs e)
        {
            Point head = Adorned.Head;
            Point end = Adorned.End;
            Adorned.Head = new Point(head.X + e.HorizontalChange, head.Y + e.VerticalChange);
            Adorned.End = new Point(end.X + e.HorizontalChange, end.Y + e.VerticalChange);
        }

        public LinePath Adorned { get; private set; }

        protected override void OnRender(DrawingContext dc)
        {
            dc.DrawGeometry(Brushes.Transparent, new Pen(null, 5), (Adorned as ShapeBase).DataToGeometry);
        }
    }


}
