﻿using INV.Elearing.Controls.Shapes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;

namespace INV.Elearning.Animations.UI
{
    public abstract class ControlThumb : ControlThumbBase
    {
        public ControlThumb(LinePath adorned) : base(adorned)
        {
            Adorned = adorned;
            Cursor = Cursors.ScrollAll;
            Template = null;
        }

        public LinePath Adorned { get; private set; }

        protected override void OnRender(DrawingContext dc)
        {
            dc.DrawEllipse(Brushes.Transparent, null, new Point(0, 0), 10, 10);
        }
    }

    public class HeadThumb : ControlThumb
    {
        public HeadThumb(LinePath adorned) : base(adorned)
        {
            Position = adorned.Head;
        }

        protected override void OnDragDelta(object sender, DragDeltaEventArgs e)
        {
            Point head = Adorned.Head;
            Adorned.Head = new Point(head.X + e.HorizontalChange, head.Y + e.VerticalChange);
        }
    }

    public class EndThumb : ControlThumb
    {
        public EndThumb(LinePath adorned) : base(adorned)
        {
            Position = adorned.End;
        }

        protected override void OnDragDelta(object sender, DragDeltaEventArgs e)
        {
            Point end = Adorned.End;
            Adorned.End = new Point(end.X + e.HorizontalChange, end.Y + e.VerticalChange);
        }
    }
}
