﻿using INV.Elearing.Controls.Shapes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace INV.Elearning.Animations.UI
{
    public class MoveThumbAdorner : ControlThumbAdorner
    {
        public MoveThumbAdorner(ShapeBase adornedShape) : base(adornedShape)
        {
        }

        /// <summary>
        /// Sắp xếp lại các nút điều khiển trên lớp này
        /// </summary>
        /// <param name="finalSize">Kích thước hình chữ nhật bao trọn lớp này</param>
        /// <returns></returns>
        protected override Size ArrangeOverride(Size finalSize)
        {
            foreach (Visual item in Visuals)
            {
                if (item is MoveThumb moveThumb)
                {
                    if (AdornedElement is LinePath linePath)
                    {
                        Point position = new Point(Math.Min(linePath.Head.X,linePath.End.X), Math.Min(linePath.Head.Y, linePath.End.Y));
                        Size size = new Size(Math.Abs(linePath.Head.X-linePath.End.X), Math.Abs(linePath.Head.Y - linePath.End.Y));
                        moveThumb.Arrange(new Rect(position, size));
                    }
                }
            }
            return finalSize;
        }

    }
}
