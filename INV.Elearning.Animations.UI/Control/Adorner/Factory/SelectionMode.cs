﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  SelectionMode.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

namespace INV.Elearning.Animations.UI
{
    /// <summary>
    /// Chế độ chọn đối tượng sở hữu đường path
    /// </summary>
    public enum SelectionMode
    {
        Find,
        Selected,
        Assigned
    }
}
