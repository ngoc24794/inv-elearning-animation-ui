﻿using INV.Elearing.Controls.Shapes;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Core.Helper;
using INV.Elearning.Core.View;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Media;

namespace INV.Elearning.Animations.UI
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: GenerationAnimationFactoryBase.cs
    // Description: Lớp cơ sở có chức năng thêm hiệu ứng cho đối tượng
    // Develope by : Nguyen Van Ngoc
    // History:
    // 09/02/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Lớp cơ sở có chức năng thêm hiệu ứng cho đối tượng
    /// </summary>
    public abstract class GenerationAnimationFactoryBase
    {
        protected GenerationAnimationFactoryBase()
        {
            AnimationFactory = new AnimationFactory();
            MotionPathFactory = new MotionPathFactory();
            DrawingMotionPathAdornerFactory = new DrawingMotionPathAdornerFactory();
        }

        public AnimationFactoryBase AnimationFactory { get; set; }
        public MotionPathFactoryBase MotionPathFactory { get; set; }
        public DrawingMotionPathAdornerFactoryBase DrawingMotionPathAdornerFactory { get; set; }
        public abstract void CreateLineAnimationPath();
        public abstract void CreateArcsAnimationPath();
        public abstract void CreateTurnsAnimationPath();
        public abstract void CreateCircleAnimationPath();
        public abstract void CreateSquareAnimationPath();
        public abstract void CreateEqualTriangleAnimationPath();
        public abstract void CreateTrapezoidAnimationPath();
        public abstract void CreateFreeformAnimationPath();
        public abstract void CreateScribbleAnimationPath();
        public abstract void CreateCurveAnimationPath();
        public abstract void CreateLineAnimationPath(string ownerId);
        public abstract void CreateArcsAnimationPath(string ownerId);
        public abstract void CreateTurnsAnimationPath(string ownerId);
        public abstract void CreateCircleAnimationPath(string ownerId);
        public abstract void CreateSquareAnimationPath(string ownerId);
        public abstract void CreateEqualTriangleAnimationPath(string ownerId);
        public abstract void CreateTrapezoidAnimationPath(string ownerId);
        public abstract void CreateFreeformAnimationPath(string ownerId);
        public abstract void CreateScribbleAnimationPath(string ownerId);
        public abstract void CreateCurveAnimationPath(string ownerId);
        public abstract void CreateBounceAnimation(eAnimationType animationType);
        public abstract void CreateFadeAnimation(eAnimationType animationType);
        public abstract void CreateFloatAnimation(eAnimationType animationType);
        public abstract void CreateFlyAnimation(eAnimationType animationType);
        public abstract void CreateGrowAnimation(eAnimationType animationType);
        public abstract void CreateGrowSpinAnimation(eAnimationType animationType);
        public abstract void CreateMultiAnimation(eAnimationType animationType);
        public abstract void CreateNoneAnimation(eAnimationType animationType);
        public abstract void CreateRandomBarsAnimation(eAnimationType animationType);
        public abstract void CreateShapeAnimation(eAnimationType animationType);
        public abstract void CreateSpinAnimation(eAnimationType animationType);
        public abstract void CreateSpinGrowAnimation(eAnimationType animationType);
        public abstract void CreateSplitAnimation(eAnimationType animationType);
        public abstract void CreateSwivelAnimation(eAnimationType animationType);
        public abstract void CreateWheelAnimation(eAnimationType animationType);
        public abstract void CreateWipeAnimation(eAnimationType animationType);
        public abstract void CreateZoomAnimation(eAnimationType animationType);
        public abstract void CreateBlindsTransition();
        public abstract void CreateCheckerboardTransition();
        public abstract void CreateCircleTransition();
        public abstract void CreateClockTransition();
        public abstract void CreateCoverTransition();
        public abstract void CreateDiamondTransition();
        public abstract void CreateDissolveTransition();
        public abstract void CreateFadeTransition();
        public abstract void CreateInTransition();
        public abstract void CreateNewsflashTransition();
        public abstract void CreateNoneTransition();
        public abstract void CreateOutTransition();
        public abstract void CreatePlusTransition();
        public abstract void CreatePushTransition();
        public abstract void CreateRandomBarsTransition();
        public abstract void CreateSplitTransition();
        public abstract void CreateUnCoverTransition();
        public abstract void CreateZoomTransition();

        #region Methods
        protected void SetAnimation(Func<IAnimationableObject, TimeSpan, eAnimationType, IAnimation> createAnimation, ICustomPath path)
        {
            IList<IAnimationableObject> owners = GetOwners();
            PathGeometry pathGeometry = (path as ShapeBase).PathGeometry;
            //---------------------------------------------------------------------------
            // Lấy đường bao
            //---------------------------------------------------------------------------
            Rect bounds = pathGeometry.GetRenderBounds(new Pen());

            SetChildrenUnSelected();

            foreach (IAnimationableObject owner in owners)
            {
                //---------------------------------------------------------------------------
                // Chỉ thêm 'ngôi sao hiệu ứng' khi ObjectElement chưa có nó
                //---------------------------------------------------------------------------
                if (!owner.IsHasAnimation)
                    if (AdornerLayer.GetAdornerLayer((Application.Current as IAppGlobal).SelectedSlide.SelectedLayout) is AdornerLayer layer)
                    {
                        layer.Add(new AnimationAdorner(owner as ObjectElement));
                    }
                owner.IsHasAnimation = true;

                //---------------------------------------------------------------------------
                // Tạo MotionPathAnimation
                //---------------------------------------------------------------------------
                IMotionPathAnimation motionPathAnimation = createAnimation(owner, GenerationAnimationViewModel.DURATION, eAnimationType.MotionPath) as IMotionPathAnimation;

                ICustomPath motionPath = path.Clone() as ICustomPath;
                if (motionPath == null) return;

                motionPath.Owner = motionPathAnimation;             // Animation là cha của motionPath

                //---------------------------------------------------------------------------
                // Tạo MotionPathObject
                //---------------------------------------------------------------------------
                ShapeBase shape = motionPath as ShapeBase;
                shape.DashType = Controls.Enums.DashType.Dash;
                shape.Stroke = STROKE_DYNAMIC;
                MotionPathObject motionPathObject = GenerateMotionPathObject(shape, bounds);
                motionPathObject.ZIndex = 9999;
                motionPathObject.Owner = owner;
                motionPathObject.Animation = motionPathAnimation;
                motionPathObject.SelectionChanged += OnSelectionChanged;
                motionPath.Container = motionPathObject;
                //---------------------------------------------------------------------------
                // Liên kết MotionPathObject với owner
                //---------------------------------------------------------------------------
                owner.MotionPaths.Add(motionPathObject);

                //---------------------------------------------------------------------------
                // Thêm MotionPathObject vào Canvas
                //---------------------------------------------------------------------------
                Point? startPoint = motionPath.VHead.Head;
                if (startPoint == null) continue;

                double
                    left = owner.Left + owner.Width / 2 - startPoint.Value.X,
                    top = owner.Top + owner.Height / 2 - startPoint.Value.Y;

                motionPathObject.Left = left;
                motionPathObject.Top = top;
                (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements.Add(motionPathObject);
                motionPathObject.IsSelected = true;
            }
        }

        protected void SetAnimation(string ownerId, Func<IAnimationableObject, TimeSpan, eAnimationType, IAnimation> createAnimation, ICustomPath path)
        {
            IAnimationableObject owner = new ObjectElement();
            bool isFounded = false;
            foreach (ObjectElement item in (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements)
            {
                if (item.ID == ownerId)
                {
                    owner = item;
                    isFounded = true;
                    break;
                }
            }
            if (!isFounded)
            {
                foreach (ObjectElement item in (Application.Current as IAppGlobal).SelectedSlide.MainLayout.Elements)
                {
                    if (item.ID == ownerId)
                    {
                        owner = item;
                        isFounded = true;
                        break;
                    }
                }
            }
            if (isFounded)
            {
                PathGeometry pathGeometry = (path as ShapeBase).PathGeometry;
                //---------------------------------------------------------------------------
                // Lấy đường bao
                //---------------------------------------------------------------------------
                Rect bounds = pathGeometry.GetRenderBounds(new Pen());

                //---------------------------------------------------------------------------
                // Chỉ thêm 'ngôi sao hiệu ứng' khi ObjectElement chưa có nó
                //---------------------------------------------------------------------------
                if (!owner.IsHasAnimation)
                    if (AdornerLayer.GetAdornerLayer((Application.Current as IAppGlobal).SelectedSlide.SelectedLayout) is AdornerLayer layer)
                    {
                        layer.Add(new AnimationAdorner(owner as ObjectElement));
                    }
                owner.IsHasAnimation = true;

                //---------------------------------------------------------------------------
                // Tạo MotionPathAnimation
                //---------------------------------------------------------------------------
                IMotionPathAnimation motionPathAnimation = createAnimation(owner, GenerationAnimationViewModel.DURATION, eAnimationType.MotionPath) as IMotionPathAnimation;

                ICustomPath motionPath = path.Clone() as ICustomPath;
                if (motionPath == null) return;

                motionPath.Owner = motionPathAnimation;             // Animation là cha của motionPath

                //---------------------------------------------------------------------------
                // Tạo MotionPathObject
                //---------------------------------------------------------------------------
                ShapeBase shape = motionPath as ShapeBase;
                shape.DashType = Controls.Enums.DashType.Dash;
                shape.Stroke = STROKE_DYNAMIC;
                MotionPathObject motionPathObject = GenerateMotionPathObject(shape, bounds);
                motionPathObject.ZIndex = 9999;
                motionPathObject.Owner = owner;
                motionPathObject.Animation = motionPathAnimation;
                motionPathObject.SelectionChanged += OnSelectionChanged;
                motionPath.Container = motionPathObject;
                //---------------------------------------------------------------------------
                // Liên kết MotionPathObject với owner
                //---------------------------------------------------------------------------
                owner.MotionPaths.Add(motionPathObject);

                //---------------------------------------------------------------------------
                // Thêm MotionPathObject vào Canvas
                //---------------------------------------------------------------------------
                Point? startPoint = motionPath.VHead.Head;
                if (startPoint == null) return;

                double
                    left = owner.Left + owner.Width / 2 - startPoint.Value.X,
                    top = owner.Top + owner.Height / 2 - startPoint.Value.Y;

                motionPathObject.Left = left;
                motionPathObject.Top = top;
                (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements.Add(motionPathObject);
                motionPathObject.IsSelected = true;
            }
        }

        private void OnSelectionChanged(object sender, ObjectElementEventArg e)
        {
            MotionPathObject motionPathObject = sender as MotionPathObject;
            if (motionPathObject?.Content is LinePath linePath)
            {
                if (AdornerLayer.GetAdornerLayer(motionPathObject) is AdornerLayer layer)
                {
                    Adorner[] adornes = layer.GetAdorners(motionPathObject);
                    INV.Elearning.Core.View.ContainerAdorner containerAdorner = default(INV.Elearning.Core.View.ContainerAdorner);
                    if (adornes != null)
                    {
                        foreach (Adorner adorner in adornes)
                        {
                            if (adorner is INV.Elearning.Core.View.ContainerAdorner)
                            {
                                containerAdorner = adorner as INV.Elearning.Core.View.ContainerAdorner;
                            }
                        }

                        layer.Remove(containerAdorner);
                    }
                }
            }
        }

        protected void SetAnimation(Func<IAnimationableObject, TimeSpan, eAnimationType, IAnimation> createAnimation, eAnimationType animationType, bool isAddStar = true)
        {
            IList<IAnimationableObject> owners = GetOwners();
            foreach (IAnimationableObject owner in owners)
            {
                //---------------------------------------------------------------------------
                // Chỉ thêm 'ngôi sao hiệu ứng' khi ObjectElement chưa có nó
                //---------------------------------------------------------------------------
                //if (!owner.IsHasAnimation && isAddStar)
                //    if (AdornerLayer.GetAdornerLayer(owner as ObjectElement) is AdornerLayer layer)
                //    {
                //        layer.Add(new AnimationAdorner(owner as ObjectElement, Canvas));
                //    }
                owner.IsHasAnimation = true;

                if (animationType == eAnimationType.Entrance)
                {
                    owner.EntranceAnimation = createAnimation(owner, GenerationAnimationViewModel.DURATION, animationType);
                }
                if (animationType == eAnimationType.Exit)
                {
                    owner.ExitAnimation = createAnimation(owner, GenerationAnimationViewModel.DURATION, animationType);
                }
            }
        }

        protected void SetPathAnimation(Adorner adorner)
        {
            if (AdornerLayer.GetAdornerLayer((Application.Current as IAppGlobal).SelectedSlide.SelectedLayout) is AdornerLayer layer)
            {
                layer.Add(adorner);
            }
        }
        /// <summary>
        /// Tạo đường Motion Path có khung bao
        /// </summary>
        /// <param name="shape"></param>
        /// <param name="rect"></param>
        /// <returns></returns>
        protected MotionPathObject GenerateMotionPathObject(ShapeBase shape, Rect rect)
        {
            MotionPathObject obj = new MotionPathObject
            {
                Content = shape,
                Width = rect.Width,
                Height = rect.Height,
                Left = rect.Left,
                Top = rect.Top
            };

            Binding bindWidth = new Binding()
            {
                Path = new PropertyPath("Width"),
                Source = obj
            };

            Binding bindHeight = new Binding()
            {
                Path = new PropertyPath("Height"),
                Source = obj
            };

            Binding bindIsSelected = new Binding()
            {
                Path = new PropertyPath("IsSelected"),
                Source = obj
            };

            BindingOperations.SetBinding(shape, ShapeBase.WidthProperty, bindWidth);
            BindingOperations.SetBinding(shape, ShapeBase.HeightProperty, bindHeight);
            BindingOperations.SetBinding(shape, ShapeBase.IsSelectedProperty, bindIsSelected);

            return obj;
        }

        /// <summary>
        /// Hủy chọn các đối tượng trên canvas
        /// </summary>
        /// <param name="canvas">Canvas chứa các đối tượng cần được hủy chọn</param>
        protected void SetChildrenUnSelected()
        {
            foreach (var child in (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements)
            {
                if (child is IAnimationableObject obj)
                {
                    obj.IsSelected = false;
                }
            }
        }

        protected List<IAnimationableObject> GetOwners()
        {
            List<IAnimationableObject> owners = new List<IAnimationableObject>();

            foreach (var child in (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements)
            {
                if (child is IAnimationableObject)
                {
                    IAnimationableObject owner = child as IAnimationableObject;
                    if (owner.IsSelected)
                    {
                        owners.Add(owner);
                    }
                }

                if (child is IMotionPathObject motionPathObject)
                {
                    if (motionPathObject.IsSelected)
                    {
                        if (motionPathObject.Owner is IAnimationableObject)
                        {
                            IAnimationableObject owner = motionPathObject.Owner as IAnimationableObject;
                            owners.Add(owner);
                        }
                    }
                }
            }

            return owners;
        }
        #endregion

        #region Contants
        //---------------------------------------------------------------------------
        // Các thông số dùng để vẽ đường Motion Path
        //---------------------------------------------------------------------------
        protected static Brush STROKE_DYNAMIC = (Brush)(new BrushConverter().ConvertFrom("#4978A2"));
        protected static Brush STROKE_STATIC = (Brush)(new BrushConverter().ConvertFrom("#BCBCBC"));
        protected static double STROKE_THICKNESS = 1.0;
        #endregion
    }

    public enum DrawingAdorner
    {
        Freeform,
        Scribble,
        Curve
    }
}
