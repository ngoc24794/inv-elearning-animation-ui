﻿namespace INV.Elearning.Animations.UI
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: DrawingMotionPathAdornerFactory.cs
    // Description: Adorner để vẽ các đường Motion Path
    // Develope by : Nguyen Van Ngoc
    // History:
    // 28/02/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Adorner để vẽ các đường Motion Path
    /// </summary>
    public class DrawingMotionPathAdornerFactory : DrawingMotionPathAdornerFactoryBase
    {
        public DrawingMotionPathAdornerFactory() : base()
        {
        }

        public override DrawingMotionPathAdorner CreateDrawingCurveAdorner()
        {
            return new DrawingCurveAdorner();
        }

        public override DrawingMotionPathAdorner CreateDrawingFreeformAdorner()
        {
            return new DrawingFreeformAdorner();
        }

        public override DrawingMotionPathAdorner CreateDrawingScribbleAdorner()
        {
            return new DrawingScribbleAdorner();
        }
    }
}
