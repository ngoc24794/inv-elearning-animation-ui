﻿namespace INV.Elearning.Animations.UI
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: DrawingMotionPathAdornerFactoryBase.cs
    // Description: Adorner để vẽ các đường Motion Path
    // Develope by : Nguyen Van Ngoc
    // History:
    // 28/02/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Adorner để vẽ các đường Motion Path
    /// </summary>
    public abstract class DrawingMotionPathAdornerFactoryBase
    {
        protected DrawingMotionPathAdornerFactoryBase()
        {
        }

        public abstract DrawingMotionPathAdorner CreateDrawingFreeformAdorner();
        public abstract DrawingMotionPathAdorner CreateDrawingScribbleAdorner();
        public abstract DrawingMotionPathAdorner CreateDrawingCurveAdorner();
    }
}
