﻿using INV.Elearning.Core.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;

namespace INV.Elearning.Animations.UI
{
    public class ContainerAdorner : Adorner
    {
        public ContainerAdorner(ObjectElement adornedElement) : base(adornedElement)
        {
        }
    }
}
