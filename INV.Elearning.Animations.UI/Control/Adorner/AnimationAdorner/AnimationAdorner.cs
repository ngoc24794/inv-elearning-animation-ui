﻿using INV.Elearning.Core.Helper;
using INV.Elearning.Core.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace INV.Elearning.Animations.UI
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: AnimationAdorner.cs
    // Description: 'Ngôi sao hiệu ứng' xác định một đối tượng đang sở hữu Animation
    // Develope by : Nguyen Van Ngoc
    // History:
    // 01/01/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// 'Ngôi sao hiệu ứng'
    /// </summary>
    public class AnimationAdorner : Adorner
    {
        public ObjectElement Owner { get; private set; }
        public bool IsHovered { get; private set; }
        public AnimationAdorner(UIElement adornedElement) : base(adornedElement)
        {
            Owner = adornedElement as ObjectElement;
            (Owner as ObjectElement).AngleChanged += OnAngleChanged;
        }

        private void OnAngleChanged(object sender, ObjectElementEventArg e)
        {
        }
        protected override void OnRender(DrawingContext dc)
        {
            Rect ownerBounds = (Owner as ObjectElement).GetBound((Application.Current as IAppGlobal).SelectedSlide.SelectedLayout);
            Vector
                v1 = new Vector(-30, 0),
                center = (Vector)ownerBounds.TopLeft + new Vector(ownerBounds.Width / 2, ownerBounds.Height / 2),
                origin = (Vector)new RotateTransform(Owner.Angle, center.X, center.Y).Transform(new Point(Owner.Left, Owner.Top)),
                v2 = (Vector)origin - (Vector)ownerBounds.TopLeft,
                v3 = (Vector)new RotateTransform(-Owner.Angle).Transform((Point)(v1 - v2));

            var bitmap = new BitmapImage(new Uri("pack://application:,,,/INV.Elearning.Animations.UI;component/Resource/AnimationIcon.png", UriKind.Absolute));
            var cropBitmap = new CroppedBitmap(bitmap, new Int32Rect(IsHovered ? 32 : 0, 0, 32, 32));
            Rect rect = new Rect(v3.X, v3.Y, 22, 22);

            dc.PushTransform(new RotateTransform(-Owner.Angle, rect.X + rect.Width / 2, rect.Y + rect.Height / 2));
            dc.DrawImage(
                cropBitmap,
                rect);
            dc.Pop();
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            Owner.IsSelected = true;
        }

        protected override void OnMouseEnter(MouseEventArgs e)
        {
            IsHovered = true;
            InvalidateVisual();
        }

        protected override void OnMouseLeave(MouseEventArgs e)
        {
            IsHovered = false;
            InvalidateVisual();
        }
    }
}
