﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Shapes;

namespace INV.Elearning.Animations.UI
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: IDrawingMotionPathAdorner.cs
    // Description: Interface cho Adorner vẽ các đường Motion Path
    // Develope by : Nguyen Van Ngoc
    // History:
    // 09/02/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Interface cho Adorner vẽ các đường Motion Path
    /// </summary>
    public interface IDrawingMotionPathAdorner
    {
        /// <summary>
        /// Tập điểm của đường Motion Path 
        /// </summary>
        PointCollection Points { get; set; }
        /// <summary>
        /// Điểm nhấn chuột đầu tiên
        /// </summary>
        Point StartPoint { get; set; }
        /// <summary>
        /// Vị trí chuột hiện tại 
        /// </summary>
        Point EndPoint { get; set; }
        /// <summary>
        /// Hình dạng đường Motion Path
        /// </summary>
        PathGeometry PathGeometry { get; set; }
        /// <summary>
        /// Đường Motion Path
        /// </summary>
        ICustomPath Path { get; set; }
    }
}
