﻿using INV.Elearing.Controls.Shapes;
using INV.Elearing.Controls.Shapes.Helpers;
using INV.Elearing.Controls.Shapes.Lines;
using INV.Elearning.Core.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace INV.Elearning.Animations.UI
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: DrawingCurveAdorner.cs
    // Description: Adorner để vẽ đường Motion Path dạng Curve
    // Develope by : Nguyen Van Ngoc
    // History:
    // 09/02/2018 : Create
    //---------------------------------------------------------------------------

    public class DrawingCurveAdorner : DrawingMotionPathAdorner
    {
        public DrawingCurveAdorner() : base()
        {
            CreateAnimation = (new AnimationFactory()).CreateCurveAnimation;
            Path = new CurvePath()
            {
                Points = Points
            };
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            //---------------------------------------------------------------------------
            // Bắt chuột
            //---------------------------------------------------------------------------
            if (IsMouseCaptured == false) CaptureMouse();


            //---------------------------------------------------------------------------
            // Cập nhật lại đường Path
            //---------------------------------------------------------------------------
            if (Points.Contains(EndPoint))
            {
                Points.Remove(EndPoint);
            }

            EndPoint = e.GetPosition(this);
            UpdatePath(EndPoint);


            //---------------------------------------------------------------------------
            // Lắng nghe sự kiện OnKeyDown
            //---------------------------------------------------------------------------
            Focus();
        }

        protected override void UpdatePath(Point point)
        {
            Points.Add(point);
            PathGeometry = CurveHelpers.GetSingleCurveGeometry(Points);
            InvalidateVisual();
        }
    }
}
