﻿using INV.Elearning.Core.Helper;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace INV.Elearning.Animations.UI
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: DrawingScribbleAdorner.cs
    // Description: Adorner để vẽ đường Motion Path dạng Scribble
    // Develope by : Nguyen Van Ngoc
    // History:
    // 09/02/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Adorner để vẽ đường Motion Path dạng Scribble
    /// </summary>
    public class DrawingScribbleAdorner : DrawingMotionPathAdorner
    {
        public DrawingScribbleAdorner() : base()
        {
            CreateAnimation = (new AnimationFactory()).CreateScribbleAnimation;
            Path = new ScribblePath()
            {
                Points = Points
            };
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            //---------------------------------------------------------------------------
            // Bắt chuột
            //---------------------------------------------------------------------------
            if (IsMouseCaptured == false) CaptureMouse();


            //---------------------------------------------------------------------------
            // Cập nhật lại đường Path
            //---------------------------------------------------------------------------
            if (e.LeftButton == MouseButtonState.Pressed)
                UpdatePath(e.GetPosition(this));


            //---------------------------------------------------------------------------
            // Lắng nghe sự kiện OnKeyDown
            //---------------------------------------------------------------------------
            Focus();
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            GenerateObject();
        }        
    }
}
