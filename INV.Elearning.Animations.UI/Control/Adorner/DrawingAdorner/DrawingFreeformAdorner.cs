﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace INV.Elearning.Animations.UI
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: DrawingFreeformAdorner.cs
    // Description: Adorner để vẽ đường Motion Path dạng Freeform
    // Develope by : Nguyen Van Ngoc
    // History:
    // 09/02/2018 : Create
    //---------------------------------------------------------------------------

    public class DrawingFreeformAdorner : DrawingMotionPathAdorner
    {
        public DrawingFreeformAdorner() : base()
        {
            CreateAnimation = (new AnimationFactory()).CreateFreeformAnimation;
            Path = new FreeformPath()
            {
                Points = Points
            };
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            //---------------------------------------------------------------------------
            // Bắt chuột
            //---------------------------------------------------------------------------
            if (IsMouseCaptured == false) CaptureMouse();


            //---------------------------------------------------------------------------
            // Cập nhật lại đường Path
            //---------------------------------------------------------------------------
            if (e.LeftButton == MouseButtonState.Pressed)
                UpdatePath(e.GetPosition(this));
            else
            {
                if (Points.Contains(EndPoint))
                {
                    Points.Remove(EndPoint);
                }

                EndPoint = e.GetPosition(this);
                UpdatePath(EndPoint);
            }


            //---------------------------------------------------------------------------
            // Lắng nghe sự kiện OnKeyDown
            //---------------------------------------------------------------------------
            Focus();
        }
    }
}
