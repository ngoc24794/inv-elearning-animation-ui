﻿using INV.Elearing.Controls.Shapes;
using INV.Elearing.Controls.Shapes.Helpers;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Core.Helper;
using INV.Elearning.Core.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace INV.Elearning.Animations.UI
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: DrawingMotionPathAdorner.cs
    // Description: Adorner để vẽ Motion Path động
    // Develope by : Nguyen Van Ngoc
    // History:
    // 09/02/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Adorner để vẽ Motion Path động
    /// </summary>
    public abstract class DrawingMotionPathAdorner : Adorner, IDrawingMotionPathAdorner
    {
        /// <summary>
        /// Hàm tạo
        /// </summary>
        /// <param name="canvas">Canvas chứa đường Motion Path sau khi vẽ xong</param>
        public DrawingMotionPathAdorner() : base((Application.Current as IAppGlobal).SelectedSlide.SelectedLayout)
        {
            Owners = GetOwners();
            Cursor = Cursors.Cross;
            Focusable = true;
            Points = new PointCollection();
            SelectionMode = SelectionMode.Selected;
            Completed += OnCompleted;
        }

        private void OnCompleted(object sender, CompletedEventArgs e)
        {

        }

        /// <summary>
        /// Sự kiện vẽ xong
        /// </summary>
        public event CompletedEventHanlder Completed;
        /// <summary>
        /// Chế độ chọn đối tượng sở hữu đường path
        /// </summary>
        public SelectionMode SelectionMode { get; set; }
        /// <summary>
        /// Id của đối tượng sở hữu đường path.
        /// Được sử dụng trong chế độ chọn <see cref="SelectionMode.Find"/>
        /// </summary>
        public string OwnerId { get; set; }
        /// <summary>
        /// Đối tượng sở hữu đường path.
        /// Được sử dụng trong chế độ chọn <see cref="SelectionMode.Assigned"/>
        /// </summary>
        public ObjectElement Owner { get; set; }

        protected List<ObjectElement> GetOwners()
        {
            List<ObjectElement> owners = new List<ObjectElement>();

            switch (SelectionMode)
            {
                case SelectionMode.Find:
                    foreach (var child in (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements)
                    {
                        if (child.ID == OwnerId)
                        {
                            owners.Add(child);
                        }
                    }
                    break;
                case SelectionMode.Selected:
                    foreach (var child in (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements)
                    {
                        if (child.IsSelected)
                        {
                            owners.Add(child);
                        }

                    }

                    foreach (var child in (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Children)
                    {
                        if (child is MotionPathObject motionPathObject)
                        {
                            if (motionPathObject.IsSelected)
                            {
                                if (motionPathObject.Owner is IAnimationableObject)
                                {
                                    ObjectElement owner = motionPathObject.Owner;
                                    owners.Add(owner);
                                }
                            }
                        }

                    }
                    break;
                case SelectionMode.Assigned:
                    if (Owner != null)
                    {
                        owners.Add(Owner);
                    }
                    break;
            }



            return owners;
        }
        /// <summary>
        /// Vẽ đường Motion Path động lên bề mặt Adorner này
        /// </summary>
        /// <param name="dc"></param>
        protected override void OnRender(DrawingContext dc)
        {
            dc.DrawRectangle(Brushes.Transparent, null, new Rect(new Point(0, 0), RenderSize));
            if (PathGeometry != null)
            {
                Pen pen = new Pen()
                {
                    Brush = STROKE_DYNAMIC,
                    Thickness = STROKE_THICKNESS
                };
                dc.DrawGeometry(null, pen, PathGeometry);
            };
        }

        /// <summary>
        /// Xử lí sự kiện nhấn chuột
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            //---------------------------------------------------------------------------
            // Bắt chuột
            //---------------------------------------------------------------------------
            if (IsMouseCaptured == false) CaptureMouse();

            //---------------------------------------------------------------------------
            // Cập nhật lại đường Path
            //---------------------------------------------------------------------------
            UpdatePath(e.GetPosition(this));
        }

        /// <summary>
        /// Cập nhật thêm điểm vào đường Motion Path đang được vẽ
        /// </summary>
        /// <param name="point">Điểm để thêm</param>
        protected virtual void UpdatePath(Point point)
        {
            Points.Add(point);
            PathGeometry = GeometryHelpers.GetPathGeometry(Points);
            InvalidateVisual();
        }

        /// <summary>
        /// Xử lí sự kiện nhấn phím
        /// </summary>
        /// <param name="e"></param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                GenerateObject();
            }
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                GenerateObject();
            }
        }
        /// <summary>
        /// Tạo Motion Path và thả vào Canvas chứa
        /// </summary>
        protected void GenerateObject()
        {
            //---------------------------------------------------------------------------
            // Thả chuột
            //---------------------------------------------------------------------------
            if (IsMouseCaptured) ReleaseMouseCapture();

            //---------------------------------------------------------------------------
            // Tự hủy
            //---------------------------------------------------------------------------
            RemoveSelf();

            if (EndPoint != null)
            {
                Points.Remove(EndPoint);
                PathGeometry = GeometryHelpers.GetPathGeometry(Points);
            }

            //---------------------------------------------------------------------------
            // Không đủ điểm thì không vẽ
            //---------------------------------------------------------------------------
            int limit = Path is CurvePath ? 2 : 3;
            if (Points.Count < limit) return;


            //---------------------------------------------------------------------------
            // Lấy đường bao
            //---------------------------------------------------------------------------
            Rect bounds = PathGeometry.GetRenderBounds(new Pen());

            //---------------------------------------------------------------------------
            // Cập nhật lại tập điểm cho vừa vặn với đường bao
            //---------------------------------------------------------------------------
            GeneratePath(bounds);

            Owners = GetOwners();
            foreach (ObjectElement owner in Owners)
            {
                //---------------------------------------------------------------------------
                // Tạo khung bao cho đường Path
                //---------------------------------------------------------------------------
                ICustomPath motionPath = Path.Clone() as ICustomPath;
                if (motionPath == null) return;

                if (CreateAnimation != null && CreateAnimation(owner, GenerationAnimationViewModel.DURATION, eAnimationType.MotionPath) is IMotionPathAnimation motionPathAnimation)
                {

                    //---------------------------------------------------------------------------
                    // Chỉ thêm 'ngôi sao hiệu ứng' khi ObjectElement chưa có nó
                    //---------------------------------------------------------------------------

                    motionPathAnimation.Owner = owner;
                    motionPath.Owner = motionPathAnimation;         // motionPath cần các thuộc tính của motionPathAnimation để vẽ hình

                    ShapeBase shape = motionPath as ShapeBase;
                    shape.DashType = Controls.Enums.DashType.Dash;
                    shape.Stroke = STROKE_DYNAMIC;
                    MotionPathObject motionPathObject = GenerateMotionPathObject(shape, bounds);
                    motionPathObject.Owner = owner;
                    motionPathObject.Animation = motionPathAnimation;
                    GenerationAnimationFactoryBase.SetIsShouldCreateNewTrigger(motionPathObject, true);
                    owner.MotionPaths.Add(motionPathObject);

                    motionPath.Container = motionPathObject;
                    //---------------------------------------------------------------------------
                    // Thêm đường Path vào Canvas
                    //---------------------------------------------------------------------------
                    Point startPoint = motionPath.Points.FirstOrDefault();
                    if (startPoint == null) continue;

                    double
                        left = owner.Left + owner.Width / 2 - startPoint.X,
                        top = owner.Top + owner.Height / 2 - startPoint.Y;

                    motionPathObject.Left = left;
                    motionPathObject.Top = top;
                    motionPathObject.TargetName = shape.ShapeName;
                    motionPathObject.Visibility = Visibility.Visible;
                    (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Children.Add(motionPathObject);
                    //(owner as ObjectElement)?.SupportElements.Add(motionPathObject);
                    motionPathObject.IsSelected = true;
                    Completed?.Invoke(this, new CompletedEventArgs(null, motionPathObject));
                }
            }
            SetChildrenUnSelected();
        }

        /// <summary>
        /// Hủy Adorner này
        /// </summary>
        protected void RemoveSelf()
        {
            if (AdornerLayer.GetAdornerLayer(this) is AdornerLayer layer)
            {
                layer.Remove(this);
            }
        }

        /// <summary>
        /// Tạo đường Motion Path được bao trọn trong khung bao
        /// </summary>
        /// <param name="bounds">Khung bao</param>
        protected void GeneratePath(Rect bounds)
        {
            double
                left = bounds.Left,
                top = bounds.Top;

            Path.Points = PointCollectionHelpers.UpdatePointCollection(Points, left, top);
        }

        /// <summary>
        /// Tạo đối tượng có khung bao
        /// </summary>
        /// <param name="shape"></param>
        /// <param name="rect"></param>
        /// <returns></returns>
        protected MotionPathObject GenerateMotionPathObject(ShapeBase shape, Rect rect)
        {
            MotionPathObject obj = new MotionPathObject
            {
                Content = shape,
                Width = rect.Width,
                Height = rect.Height,
                Left = rect.Left,
                Top = rect.Top
            };

            Binding bindWidth = new Binding()
            {
                Path = new PropertyPath("Width"),
                Source = obj
            };

            Binding bindHeight = new Binding()
            {
                Path = new PropertyPath("Height"),
                Source = obj
            };

            Binding bindIsSelected = new Binding()
            {
                Path = new PropertyPath("IsSelected"),
                Source = obj
            };

            BindingOperations.SetBinding(shape, ShapeBase.WidthProperty, bindWidth);
            BindingOperations.SetBinding(shape, ShapeBase.HeightProperty, bindHeight);
            BindingOperations.SetBinding(shape, ShapeBase.IsSelectedProperty, bindIsSelected);

            return obj;
        }

        /// <summary>
        /// Hủy chọn các đối tượng trên canvas
        /// </summary>
        /// <param name="canvas">Canvas chứa các đối tượng cần được hủy chọn</param>
        protected void SetChildrenUnSelected()
        {
            foreach (var child in (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements)
            {
                child.IsSelected = false;
            }
        }

        /// <summary>
        /// Danh sách các đối tượng sở hữu Motion Path đang xử lí
        /// </summary>
        public List<ObjectElement> Owners { get; private set; }

        /// <summary>
        /// Tập điểm của Motion Path đang được vẽ
        /// </summary>
        public PointCollection Points { get; set; }

        /// <summary>
        /// Lưu điểm nhấn chuột đầu tiên
        /// </summary>
        public Point StartPoint { get; set; }

        /// <summary>
        /// Lưu vị trí chuột hiện tại cũng là điểm cuối cùng của đường Motion Path
        /// </summary>
        public Point EndPoint { get; set; }

        /// <summary>
        /// Lưu hình dạng đường Motion Path
        /// </summary>
        public PathGeometry PathGeometry { get; set; }

        /// <summary>
        /// Đường Motion Path đang được vẽ
        /// </summary>
        public ICustomPath Path { get; set; }

        public Func<IAnimationableObject, TimeSpan, eAnimationType, IAnimation> CreateAnimation { get; set; }


        //---------------------------------------------------------------------------
        // Các thông số dùng để vẽ đường Motion Path
        //---------------------------------------------------------------------------
        protected Brush STROKE_DYNAMIC = (Brush)(new BrushConverter().ConvertFrom("#4978A2"));
        protected Brush STROKE_STATIC = (Brush)(new BrushConverter().ConvertFrom("#BCBCBC"));
        protected double STROKE_THICKNESS = 1.0;
    }
}
