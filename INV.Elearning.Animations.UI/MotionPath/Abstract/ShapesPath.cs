﻿using INV.Elearing.Controls.Shapes;
using INV.Elearing.Controls.Shapes.Helpers;
using INV.Elearning.Core.View;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using sys = System.Windows.Media;

namespace INV.Elearning.Animations.UI
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: BasicPath.cs
    // Description: Các loại đường Motion Path được tạo sẵn
    // Develope by : Nguyen Van Ngoc
    // History:
    // 22/02/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Lớp cơ sở để tạo các loại đường Motion Path có hình dạng thuộc các dạng hình cơ bản
    /// (như đường tròn, hình chữ nhật,...)
    /// </summary>
    public abstract class ShapesPath : CustomPath
    {
        public ShapesPath()
        {
            Width = 300;
            Height = 200;
            DefiningGeometry();
        }

        protected override void CustomMethodBeforeDefiningGeometry(DependencyPropertyChangedEventArgs e)
        {
        }

        protected override void OnRender(DrawingContext dc)
        {
            if (IsSelected && Owner != null)
            {
                //---------------------------------------------------------------------------
                // Vẽ các hình Preview
                //---------------------------------------------------------------------------
                ObjectElement obj = Owner.Owner as ObjectElement;

                if (obj != null)
                {
                    double
                        angleContainer = Container is ObjectElement ? (Container as ObjectElement).Angle : 0;

                    bool
                        isScaleX = Container is ObjectElement ? (Container as ObjectElement).IsScaleX : false,
                        isScaleY = Container is ObjectElement ? (Container as ObjectElement).IsScaleY : false;

                    Point startPoint = VHead.Head;

                    double
                        thickness = obj is IBorderSupport ? (obj as IBorderSupport).Thickness : 0,
                        width = obj.ActualWidth + thickness,
                        height = obj.ActualHeight + thickness,
                        cx = Math.Round(obj.ActualWidth / 2, 0),
                        cy = Math.Round(obj.ActualHeight / 2, 0),
                        sx = Math.Round(startPoint.X, 0),
                        sy = Math.Round(startPoint.Y, 0);


                    if ((obj as StandardElement)?.ShapePresent is ShapeBase shapeBase)
                    {
                        Rect bounds = shapeBase.PathGeometry.Bounds;
                        double
                            deltaX = Math.Abs(bounds.X),
                            deltaY = Math.Abs(bounds.Y);

                        cx += deltaX + thickness / 2;
                        cy += deltaY + thickness / 2;
                        width = bounds.Width + thickness;
                        height = bounds.Height + thickness;
                    }


                    VisualBrush visualBrush = new VisualBrush
                    {
                        Visual = obj.Container
                    };


                    //---------------------------------------------------------------------------
                    // Hình ở điểm đầu 
                    //---------------------------------------------------------------------------
                    dc.PushOpacity(0.5);
                    double angle1 = obj.Angle;

                    dc.PushTransform(new ScaleTransform(isScaleX ? -1 : 1, isScaleY ? -1 : 1)
                    {
                        CenterX = sx,
                        CenterY = sy
                    });

                    dc.PushTransform(new RotateTransform(angle1 - angleContainer)
                    {
                        CenterX = sx,
                        CenterY = sy
                    });

                    dc.DrawRectangle(visualBrush, null, new Rect(sx - cx, sy - cy, width, height));

                    dc.Pop();
                    dc.Pop();

                }
            }

            GetEffectOptions(out bool reversePathDirection, out bool relativeStartPoint, out bool orientShapeToPath);

            //---------------------------------------------------------------------------
            // Vẽ đường cong
            //---------------------------------------------------------------------------
            Stroke = STROKE;
            StrokeThickness = STROKE_THICKNESS;
            if (string.IsNullOrEmpty(Data))                                         // Data có rỗng không?
                return;                                                             // Có. Không vẽ.

            // Vẽ hình theo Data

            Pen pen = new Pen(Stroke, StrokeThickness)                              // Chọn bút vẽ
            {
                DashStyle = new DashStyle(new double[] { 2, 2 }, 0),
                LineJoin = JoinType,
                DashCap = CapType
            };

            dc.DrawGeometry(Fill, pen, DataToGeometry);

            //---------------------------------------------------------------------------
            // Vẽ mũi tên
            //---------------------------------------------------------------------------     
            if (!reversePathDirection)
            {
                pen = new Pen(relativeStartPoint ? BEGIN_ARROW_ST2 : BEGIN_ARROW_ST, STROKE_THICKNESS);
                dc.DrawGeometry(relativeStartPoint ? BEGIN_ARROW_BG2 : BEGIN_ARROW_BG, pen, new BeginArrow(VHead).ToGeometry());
            }
            else
            {
                pen = new Pen(relativeStartPoint ? BEGIN_ARROW_ST2 : BEGIN_ARROW_ST, STROKE_THICKNESS);
                dc.DrawGeometry(relativeStartPoint ? BEGIN_ARROW_BG2 : BEGIN_ARROW_BG, pen, new BeginArrow(-VEnd).ToGeometry());
            }
        }

        public override object Clone()
        {
            return new CirclePath()
            {
                Points = Points,
                VHead = VHead,
                VEnd = VEnd,
                Owner = Owner
            };
        }
    }
}
