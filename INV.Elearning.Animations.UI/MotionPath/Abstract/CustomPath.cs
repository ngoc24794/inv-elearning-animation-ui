﻿using INV.Elearing.Controls.Shapes;
using INV.Elearing.Controls.Shapes.Helpers;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Core.View;
using System;
using System.Windows;
using sys = System.Windows.Media;
using System.Windows.Media;
using System.Windows.Controls;
using System.Globalization;

namespace INV.Elearning.Animations.UI
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: CustomPath.cs
    // Description: Lớp cơ sở cho các đường vẽ tự do
    // Develope by : Nguyen Van Ngoc
    // History:
    // 09/02/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Lớp cơ sở cho các đường vẽ tự do
    /// </summary>
    public abstract class CustomPath : ShapeBase, ICustomPath, IMotionPath
    {
        public CustomPath()
        {
            IsLinePath = GetType() == typeof(LinePath) ? Visibility.Collapsed : Visibility.Visible;
        }

        public PointCollection Points { get; set; }
        public EVector VHead { get; set; }
        public EVector VEnd { get; set; }
        public IMotionPathAnimation Owner { get; set; }
        public IMotionPathObject Container { get; set; }
        public Visibility IsLinePath { get; set; }

        /// <summary>
        /// Tính toán lại khi thay đổi kích thước
        /// </summary>
        /// <param name="e"></param>
        protected override void CustomMethodBeforeDefiningGeometry(DependencyPropertyChangedEventArgs e)
        {
            if (e.OldValue is double && (double)e.OldValue != 0)
            {
                if (e.Property == WidthProperty)
                {
                    double factor = (double)e.NewValue / (double)e.OldValue;
                    Points = PointCollectionHelpers.UpdatePointCollectionForWidthChange(Points, factor);
                }
                if (e.Property == HeightProperty)
                {
                    double factor = (double)e.NewValue / (double)e.OldValue;
                    Points = PointCollectionHelpers.UpdatePointCollectionForHeightChange(Points, factor);
                }
            }
        }

        protected override void OnRender(DrawingContext dc)
        {
            //---------------------------------------------------------------------------
            // Lưu Data
            //---------------------------------------------------------------------------
            if (!PathGeometry.IsEmpty())
            {
                Data = PathGeometry.ToString(CultureInfo.InvariantCulture);

                //---------------------------------------------------------------------------
                // Tính VHead và VEnd
                //---------------------------------------------------------------------------

                // Lấy các giá trị tại điểm đầu đường Path.
                PathGeometry.GetPointAtFractionLength(0.0, out Point startPoint, out Point startTangent);

                // Lấy các giá trị tại điểm cuối đường Path.
                PathGeometry.GetPointAtFractionLength(1.0, out Point endPoint, out Point endTangent);

                VHead = GetEVector(startPoint, startTangent);
                VEnd = GetEVector(endPoint, endTangent);

                GetEffectOptions(out bool reversePathDirection, out bool relativeStartPoint, out bool orientShapeToPath);

                if (IsSelected && Owner != null)
                {
                    //---------------------------------------------------------------------------
                    // Vẽ các hình Preview
                    //---------------------------------------------------------------------------
                    if (Owner.Owner is ObjectElement obj)
                    {
                        double
                            angleContainer = Container is ObjectElement ? (Container as ObjectElement).Angle : 0;

                        bool
                            isScaleX = Container is ObjectElement ? (Container as ObjectElement).IsScaleX : false,
                            isScaleY = Container is ObjectElement ? (Container as ObjectElement).IsScaleY : false;

                        double
                            thickness = obj is IBorderSupport ? (obj as IBorderSupport).Thickness : 0,
                            width = obj.ActualWidth + thickness,
                            height = obj.ActualHeight + thickness,
                            cx = obj.ActualWidth / 2,
                            cy = obj.ActualHeight / 2,
                            sx = startPoint.X,
                            sy = startPoint.Y,
                            ex = endPoint.X,
                            ey = endPoint.Y;


                        if ((obj as StandardElement)?.ShapePresent is ShapeBase shapeBase)
                        {
                            Rect bounds = shapeBase.PathGeometry.Bounds;
                            double
                                deltaX = Math.Abs(bounds.X),
                                deltaY = Math.Abs(bounds.Y);

                            cx += deltaX + thickness / 2;
                            cy += deltaY + thickness / 2;
                            width = bounds.Width + thickness;
                            height = bounds.Height + thickness;
                        }


                        VisualBrush visualBrush = new VisualBrush
                        {
                            Visual = obj.Container
                        };


                        //---------------------------------------------------------------------------
                        // Hình ở điểm đầu 
                        //---------------------------------------------------------------------------
                        dc.PushOpacity(0.5);
                        double angle1 = obj.Angle;

                        dc.PushTransform(new ScaleTransform(isScaleX ? -1 : 1, isScaleY ? -1 : 1)
                        {
                            CenterX = sx,
                            CenterY = sy
                        });

                        dc.PushTransform(new RotateTransform(angle1 - angleContainer)
                        {
                            CenterX = sx,
                            CenterY = sy
                        });

                        dc.DrawRectangle(visualBrush, null, new Rect(sx - cx, sy - cy, width, height));

                        dc.Pop();
                        dc.Pop();

                        //---------------------------------------------------------------------------
                        // Hình ở điểm cuối
                        //---------------------------------------------------------------------------
                        dc.PushTransform(new ScaleTransform(isScaleX ? -1 : 1, isScaleY ? -1 : 1)
                        {
                            CenterX = ex,
                            CenterY = ey
                        });

                        dc.PushTransform(new RotateTransform(obj.Angle - angleContainer)
                        {
                            CenterX = ex,
                            CenterY = ey
                        });


                        double angle2 = VEnd.GetAngle() * 180 / Math.PI;
                        if (orientShapeToPath)
                            dc.PushTransform(new RotateTransform(angle2)
                            {
                                CenterX = ex,
                                CenterY = ey
                            });

                        dc.DrawRectangle(visualBrush, null, new Rect(ex - cx, ey - cy, width, height));

                        dc.Pop();
                        dc.Pop();
                        if (orientShapeToPath && !reversePathDirection)
                            dc.Pop();

                    }
                }


                //---------------------------------------------------------------------------
                // Vẽ đường cong
                //---------------------------------------------------------------------------
                Stroke = STROKE;
                StrokeThickness = STROKE_THICKNESS;
                DashType = Controls.Enums.DashType.Dash;
                base.OnRender(dc);

                //---------------------------------------------------------------------------
                // Vẽ mũi tên bắt đầu
                //---------------------------------------------------------------------------             
                Pen pen = new Pen(relativeStartPoint ? BEGIN_ARROW_ST2 : BEGIN_ARROW_ST, STROKE_THICKNESS);
                dc.DrawGeometry(relativeStartPoint ? BEGIN_ARROW_BG2 : BEGIN_ARROW_BG, pen, new BeginArrow(reversePathDirection ? -VEnd : VHead).ToGeometry());

                //---------------------------------------------------------------------------
                // Vẽ mũi tên kết thúc
                //---------------------------------------------------------------------------
                pen = new Pen(END_ARROW_ST, STROKE_THICKNESS);
                dc.DrawGeometry(END_ARROW_BG, pen, new EndArrow(reversePathDirection ? -VHead : VEnd).ToGeometry()); 
            }
        }

        protected EVector GetEVector(Point startPoint, Point startTangent)
        {
            Vector
                v1 = (Vector)startPoint,
                v2 = (Vector)startTangent;

            Point
                head = startPoint,
                end = (Point)v1 + v2;

            return new EVector(head, end);
        }

        protected void GetEffectOptions(out bool reversePathDirection, out bool relativeStartPoint, out bool orientShapeToPath)
        {
            reversePathDirection = false;
            relativeStartPoint = false;
            orientShapeToPath = false;

            if (Owner != null)
                foreach (IEffectOptionGroup optionGroup in Owner.EffectOptions)
                {
                    if (optionGroup.GroupName.ToUpper() == KeyLanguage.PathOption.ToUpper())
                        foreach (IEffectOption option in optionGroup.Values)
                        {
                            if (option.Name.ToUpper() == KeyLanguage.ReversePathDirection.ToUpper())
                            {
                                reversePathDirection = option.IsSelected;
                            }
                            if (option.Name.ToUpper() == KeyLanguage.RelativeStartPoint.ToUpper())
                            {
                                relativeStartPoint = option.IsSelected;
                            }
                            if (option.Name.ToUpper() == KeyLanguage.OrientShapeToPath.ToUpper())
                            {
                                orientShapeToPath = option.IsSelected;
                            }
                        }
                }
        }

        public bool GetIsLocked()
        {
            if (Owner != null)
                foreach (IEffectOptionGroup optionGroup in Owner.EffectOptions)
                {
                    if (optionGroup.GroupName.ToUpper() == KeyLanguage.OriginOption.ToUpper())
                        foreach (IEffectOption option in optionGroup.Values)
                        {
                            if (option.Name.ToUpper() == KeyLanguage.Locked.ToUpper())
                            {
                                if (option.IsSelected)
                                {
                                    return true;
                                }
                            }
                            if (option.Name.ToUpper() == KeyLanguage.UnLocked.ToUpper())
                            {
                                if (option.IsSelected)
                                {
                                    return false;
                                }
                            }
                        }
                }

            return false;
        }
        public virtual object Clone()
        {
            return null;
        }

        public void CallDefiningGeometry()
        {
            DefiningGeometry();
        }

        protected Brush BEGIN_ARROW_BG = (Brush)(new BrushConverter().ConvertFrom("#D9FAB8"));
        protected Brush BEGIN_ARROW_ST = (Brush)(new BrushConverter().ConvertFrom("#35A91B"));
        protected Brush BEGIN_ARROW_BG2 = (Brush)(new BrushConverter().ConvertFrom("#FFE873"));
        protected Brush BEGIN_ARROW_ST2 = (Brush)(new BrushConverter().ConvertFrom("#E1AC4B"));
        protected Brush END_ARROW_BG = (Brush)(new BrushConverter().ConvertFrom("#FDC8C8"));
        protected Brush END_ARROW_ST = (Brush)(new BrushConverter().ConvertFrom("#FF0000"));
        protected new Brush STROKE = Brushes.Gray;
        protected double STROKE_THICKNESS = 1;
    }


}
