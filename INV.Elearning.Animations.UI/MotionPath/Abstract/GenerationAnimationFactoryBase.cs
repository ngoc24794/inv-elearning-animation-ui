﻿using INV.Elearing.Controls.Shapes;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Core.Helper;
using INV.Elearning.Core.View;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Media;

namespace INV.Elearning.Animations.UI
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: GenerationAnimationFactoryBase.cs
    // Description: Lớp cơ sở có chức năng thêm hiệu ứng cho đối tượng
    // Develope by : Nguyen Van Ngoc
    // History:
    // 09/02/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Lớp cơ sở có chức năng thêm hiệu ứng cho đối tượng
    /// </summary>
    public abstract class GenerationAnimationFactoryBase
    {
        public event CompletedEventHanlder Completed;



        public static bool GetIsShouldCreateNewTrigger(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsShouldCreateNewTriggerProperty);
        }

        public static void SetIsShouldCreateNewTrigger(DependencyObject obj, bool value)
        {
            obj.SetValue(IsShouldCreateNewTriggerProperty, value);
        }

        // Using a DependencyProperty as the backing store for IsShouldCreateNewTrigger.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsShouldCreateNewTriggerProperty =
            DependencyProperty.RegisterAttached("IsShouldCreateNewTrigger", typeof(bool), typeof(GenerationAnimationFactoryBase), new PropertyMetadata(false));



        public void RaiseCompletedEvent(object sender, CompletedEventArgs e)
        {
            Completed?.Invoke(this, e);
        }

        protected GenerationAnimationFactoryBase()
        {
            AnimationFactory = new AnimationFactory();
            MotionPathFactory = new MotionPathFactory();
            DrawingMotionPathAdornerFactory = new DrawingMotionPathAdornerFactory();
            Completed += OnCompleted;
        }

        private void OnCompleted(object sender, CompletedEventArgs e)
        {
            // TODO: 
        }

        public AnimationFactoryBase AnimationFactory { get; set; }
        public MotionPathFactoryBase MotionPathFactory { get; set; }
        public DrawingMotionPathAdornerFactoryBase DrawingMotionPathAdornerFactory { get; set; }
        public abstract void CreateLineAnimationPath();
        public abstract void CreateArcsAnimationPath();
        public abstract void CreateTurnsAnimationPath();
        public abstract void CreateCircleAnimationPath();
        public abstract void CreateSquareAnimationPath();
        public abstract void CreateEqualTriangleAnimationPath();
        public abstract void CreateTrapezoidAnimationPath();
        public abstract void CreateFreeformAnimationPath();
        public abstract void CreateScribbleAnimationPath();
        public abstract void CreateCurveAnimationPath();
        public abstract void CreateLineAnimationPath(string ownerId);
        public abstract void CreateArcsAnimationPath(string ownerId);
        public abstract void CreateTurnsAnimationPath(string ownerId);
        public abstract void CreateCircleAnimationPath(string ownerId);
        public abstract void CreateSquareAnimationPath(string ownerId);
        public abstract void CreateEqualTriangleAnimationPath(string ownerId);
        public abstract void CreateTrapezoidAnimationPath(string ownerId);
        public abstract void CreateFreeformAnimationPath(string ownerId);
        public abstract void CreateScribbleAnimationPath(string ownerId);
        public abstract void CreateCurveAnimationPath(string ownerId);
        public abstract void CreateLineAnimationPath(ObjectElement @object);
        public abstract void CreateArcsAnimationPath(ObjectElement @object);
        public abstract void CreateTurnsAnimationPath(ObjectElement @object);
        public abstract void CreateCircleAnimationPath(ObjectElement @object);
        public abstract void CreateSquareAnimationPath(ObjectElement @object);
        public abstract void CreateEqualTriangleAnimationPath(ObjectElement @object);
        public abstract void CreateTrapezoidAnimationPath(ObjectElement @object);
        public abstract void CreateFreeformAnimationPath(ObjectElement @object);
        public abstract void CreateScribbleAnimationPath(ObjectElement @object);
        public abstract void CreateCurveAnimationPath(ObjectElement @object);
        public abstract void CreateBounceAnimation(eAnimationType animationType);
        public abstract void CreateFadeAnimation(eAnimationType animationType);
        public abstract void CreateFloatAnimation(eAnimationType animationType);
        public abstract void CreateFlyAnimation(eAnimationType animationType);
        public abstract void CreateGrowAnimation(eAnimationType animationType);
        public abstract void CreateGrowSpinAnimation(eAnimationType animationType);
        public abstract void CreateMultiAnimation(eAnimationType animationType);
        public abstract void CreateNoneAnimation(eAnimationType animationType);
        public abstract void CreateRandomBarsAnimation(eAnimationType animationType);
        public abstract void CreateShapeAnimation(eAnimationType animationType);
        public abstract void CreateSpinAnimation(eAnimationType animationType);
        public abstract void CreateSpinGrowAnimation(eAnimationType animationType);
        public abstract void CreateSplitAnimation(eAnimationType animationType);
        public abstract void CreateSwivelAnimation(eAnimationType animationType);
        public abstract void CreateWheelAnimation(eAnimationType animationType);
        public abstract void CreateWipeAnimation(eAnimationType animationType);
        public abstract void CreateZoomAnimation(eAnimationType animationType);
        public abstract void CreateBounceAnimation(string ownerId, eAnimationType animationType);
        public abstract void CreateFadeAnimation(string ownerId, eAnimationType animationType);
        public abstract void CreateFloatAnimation(string ownerId, eAnimationType animationType);
        public abstract void CreateFlyAnimation(string ownerId, eAnimationType animationType);
        public abstract void CreateGrowAnimation(string ownerId, eAnimationType animationType);
        public abstract void CreateGrowSpinAnimation(string ownerId, eAnimationType animationType);
        public abstract void CreateMultiAnimation(string ownerId, eAnimationType animationType);
        public abstract void CreateNoneAnimation(string ownerId, eAnimationType animationType);
        public abstract void CreateRandomBarsAnimation(string ownerId, eAnimationType animationType);
        public abstract void CreateShapeAnimation(string ownerId, eAnimationType animationType);
        public abstract void CreateSpinAnimation(string ownerId, eAnimationType animationType);
        public abstract void CreateSpinGrowAnimation(string ownerId, eAnimationType animationType);
        public abstract void CreateSplitAnimation(string ownerId, eAnimationType animationType);
        public abstract void CreateSwivelAnimation(string ownerId, eAnimationType animationType);
        public abstract void CreateWheelAnimation(string ownerId, eAnimationType animationType);
        public abstract void CreateWipeAnimation(string ownerId, eAnimationType animationType);
        public abstract void CreateZoomAnimation(string ownerId, eAnimationType animationType);
        public abstract void CreateBounceAnimation(ObjectElement @object, eAnimationType animationType);
        public abstract void CreateFadeAnimation(ObjectElement @object, eAnimationType animationType);
        public abstract void CreateFloatAnimation(ObjectElement @object, eAnimationType animationType);
        public abstract void CreateFlyAnimation(ObjectElement @object, eAnimationType animationType);
        public abstract void CreateGrowAnimation(ObjectElement @object, eAnimationType animationType);
        public abstract void CreateGrowSpinAnimation(ObjectElement @object, eAnimationType animationType);
        public abstract void CreateMultiAnimation(ObjectElement @object, eAnimationType animationType);
        public abstract void CreateNoneAnimation(ObjectElement @object, eAnimationType animationType);
        public abstract void CreateRandomBarsAnimation(ObjectElement @object, eAnimationType animationType);
        public abstract void CreateShapeAnimation(ObjectElement @object, eAnimationType animationType);
        public abstract void CreateSpinAnimation(ObjectElement @object, eAnimationType animationType);
        public abstract void CreateSpinGrowAnimation(ObjectElement @object, eAnimationType animationType);
        public abstract void CreateSplitAnimation(ObjectElement @object, eAnimationType animationType);
        public abstract void CreateSwivelAnimation(ObjectElement @object, eAnimationType animationType);
        public abstract void CreateWheelAnimation(ObjectElement @object, eAnimationType animationType);
        public abstract void CreateWipeAnimation(ObjectElement @object, eAnimationType animationType);
        public abstract void CreateZoomAnimation(ObjectElement @object, eAnimationType animationType);
        public abstract void CreateBlindsTransition();
        public abstract void CreateCheckerboardTransition();
        public abstract void CreateCircleTransition();
        public abstract void CreateClockTransition();
        public abstract void CreateCoverTransition();
        public abstract void CreateDiamondTransition();
        public abstract void CreateDissolveTransition();
        public abstract void CreateFadeTransition();
        public abstract void CreateInTransition();
        public abstract void CreateNewsflashTransition();
        public abstract void CreateNoneTransition();
        public abstract void CreateOutTransition();
        public abstract void CreatePlusTransition();
        public abstract void CreatePushTransition();
        public abstract void CreateRandomBarsTransition();
        public abstract void CreateSplitTransition();
        public abstract void CreateUnCoverTransition();
        public abstract void CreateZoomTransition();

        #region Methods
        protected void SetAnimation(Func<IAnimationableObject, TimeSpan, eAnimationType, IAnimation> createAnimation, ICustomPath path)
        {
            IList<ObjectElement> owners = GetOwners();
            PathGeometry pathGeometry = (path as ShapeBase).PathGeometry;
            //---------------------------------------------------------------------------
            // Lấy đường bao
            //---------------------------------------------------------------------------
            Rect bounds = pathGeometry.GetRenderBounds(new Pen());

            SetChildrenUnSelected();
            List<SetMotionPathUndoData> dataUndo = new List<SetMotionPathUndoData>();
            foreach (ObjectElement owner in owners)
            {
                //---------------------------------------------------------------------------
                // Tạo MotionPathAnimation
                //---------------------------------------------------------------------------
                IMotionPathAnimation motionPathAnimation = createAnimation(owner, GenerationAnimationViewModel.DURATION, eAnimationType.MotionPath) as IMotionPathAnimation;

                ICustomPath motionPath = path.Clone() as ICustomPath;
                if (motionPath == null) return;

                motionPath.Owner = motionPathAnimation;             // Animation là cha của motionPath

                //---------------------------------------------------------------------------
                // Tạo MotionPathObject
                //---------------------------------------------------------------------------
                ShapeBase shape = motionPath as ShapeBase;
                shape.DashType = Controls.Enums.DashType.Dash;
                shape.Stroke = STROKE_DYNAMIC;
                MotionPathObject motionPathObject = GenerateMotionPathObject(shape, bounds);
                if (shape is LinePath)
                {
                    motionPathObject.MinWidth = 1;
                    motionPathObject.Width = 1;
                }
                motionPathObject.ZIndex = 9999;
                motionPathObject.Owner = owner;
                motionPathObject.Animation = motionPathAnimation;
                motionPath.Container = motionPathObject;
                //---------------------------------------------------------------------------
                // Liên kết MotionPathObject với owner
                //---------------------------------------------------------------------------
                SetIsShouldCreateNewTrigger(motionPathObject, true);
                owner.MotionPaths.Add(motionPathObject);

                //---------------------------------------------------------------------------
                // Thêm MotionPathObject vào Canvas
                //---------------------------------------------------------------------------
                Point? startPoint = motionPath.VHead.Head;
                if (startPoint == null) continue;

                double
                    left = owner.Left + owner.Width / 2 - startPoint.Value.X,
                    top = owner.Top + owner.Height / 2 - startPoint.Value.Y;

                motionPathObject.Left = Math.Round(left, 2);
                motionPathObject.Top = Math.Round(top, 2);
                motionPathObject.TargetName = shape.ShapeName;
                motionPathObject.Visibility = Visibility.Visible;
                (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Children.Add(motionPathObject);
                motionPathObject.IsSelected = true;

                dataUndo.Add(new SetMotionPathUndoData(owner, (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout, motionPathObject));
            }
            Global.PushUndo(new SetMotionPathUndo(dataUndo));
        }       

        protected void SetAnimation(string ownerId, Func<IAnimationableObject, TimeSpan, eAnimationType, IAnimation> createAnimation, ICustomPath path)
        {
            ObjectElement owner = new ObjectElement();
            bool isFounded = false;
            foreach (ObjectElement item in (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements)
            {
                if (item.ID == ownerId)
                {
                    owner = item;
                    isFounded = true;
                    break;
                }
            }
            if (isFounded)
            {
                PathGeometry pathGeometry = (path as ShapeBase).PathGeometry;
                //---------------------------------------------------------------------------
                // Lấy đường bao
                //---------------------------------------------------------------------------
                Rect bounds = pathGeometry.GetRenderBounds(new Pen());

                //---------------------------------------------------------------------------
                // Chỉ thêm 'ngôi sao hiệu ứng' khi ObjectElement chưa có nó
                //---------------------------------------------------------------------------

                //---------------------------------------------------------------------------
                // Tạo MotionPathAnimation
                //---------------------------------------------------------------------------
                IMotionPathAnimation motionPathAnimation = createAnimation(owner, GenerationAnimationViewModel.DURATION, eAnimationType.MotionPath) as IMotionPathAnimation;

                ICustomPath motionPath = path.Clone() as ICustomPath;
                if (motionPath == null) return;

                motionPath.Owner = motionPathAnimation;             // Animation là cha của motionPath

                //---------------------------------------------------------------------------
                // Tạo MotionPathObject
                //---------------------------------------------------------------------------
                ShapeBase shape = motionPath as ShapeBase;
                shape.DashType = Controls.Enums.DashType.Dash;
                shape.Stroke = STROKE_DYNAMIC;
                MotionPathObject motionPathObject = GenerateMotionPathObject(shape, bounds);
                if (shape is LinePath)
                {
                    motionPathObject.MinWidth = 1;
                    motionPathObject.Width = 1;
                }
                motionPathObject.ZIndex = 9999;
                motionPathObject.Owner = owner;
                motionPathObject.Animation = motionPathAnimation;
                motionPath.Container = motionPathObject;
                //---------------------------------------------------------------------------
                // Liên kết MotionPathObject với owner
                //---------------------------------------------------------------------------
                owner.MotionPaths.Add(motionPathObject);

                //---------------------------------------------------------------------------
                // Thêm MotionPathObject vào Canvas
                //---------------------------------------------------------------------------
                Point? startPoint = motionPath.VHead.Head;
                if (startPoint == null) return;

                double
                    left = owner.Left + owner.Width / 2 - startPoint.Value.X,
                    top = owner.Top + owner.Height / 2 - startPoint.Value.Y;

                motionPathObject.Left = Math.Round(left, 2);
                motionPathObject.Top = Math.Round(top, 2);
                motionPathObject.TargetName = shape.ShapeName;
                motionPathObject.Visibility = Visibility.Visible;
                (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Children.Add(motionPathObject);
                //(owner as ObjectElement)?.SupportElements.Add(motionPathObject);
                motionPathObject.IsSelected = true;
                RaiseCompletedEvent(this, new CompletedEventArgs(motionPathAnimation, motionPathObject));
            }
        }

        protected void SetAnimation(ObjectElement owner, Func<IAnimationableObject, TimeSpan, eAnimationType, IAnimation> createAnimation, ICustomPath path)
        {
            if (owner != null)
            {
                PathGeometry pathGeometry = (path as ShapeBase).PathGeometry;
                //---------------------------------------------------------------------------
                // Lấy đường bao
                //---------------------------------------------------------------------------
                Rect bounds = pathGeometry.GetRenderBounds(new Pen());

                //---------------------------------------------------------------------------
                // Chỉ thêm 'ngôi sao hiệu ứng' khi ObjectElement chưa có nó
                //---------------------------------------------------------------------------

                //---------------------------------------------------------------------------
                // Tạo MotionPathAnimation
                //---------------------------------------------------------------------------
                IMotionPathAnimation motionPathAnimation = createAnimation(owner, GenerationAnimationViewModel.DURATION, eAnimationType.MotionPath) as IMotionPathAnimation;

                ICustomPath motionPath = path.Clone() as ICustomPath;
                if (motionPath == null) return;

                motionPath.Owner = motionPathAnimation;             // Animation là cha của motionPath

                //---------------------------------------------------------------------------
                // Tạo MotionPathObject
                //---------------------------------------------------------------------------
                ShapeBase shape = motionPath as ShapeBase;
                shape.DashType = Controls.Enums.DashType.Dash;
                shape.Stroke = STROKE_DYNAMIC;
                MotionPathObject motionPathObject = GenerateMotionPathObject(shape, bounds);
                motionPathObject.ZIndex = 9999;
                motionPathObject.Owner = owner;
                motionPathObject.Animation = motionPathAnimation;
                motionPath.Container = motionPathObject;
                if (shape is LinePath)
                {
                    motionPathObject.MinWidth = 1;
                    motionPathObject.Width = 1;
                }
                //---------------------------------------------------------------------------
                // Liên kết MotionPathObject với owner
                //---------------------------------------------------------------------------
                owner.MotionPaths.Add(motionPathObject);

                //---------------------------------------------------------------------------
                // Thêm MotionPathObject vào Canvas
                //---------------------------------------------------------------------------
                Point? startPoint = motionPath.VHead.Head;
                if (startPoint == null) return;

                double
                    left = owner.Left + owner.Width / 2 - startPoint.Value.X,
                    top = owner.Top + owner.Height / 2 - startPoint.Value.Y;

                motionPathObject.Left = Math.Round(left, 2);
                motionPathObject.Top = Math.Round(top, 2);
                motionPathObject.TargetName = shape.ShapeName;
                motionPathObject.Visibility = Visibility.Visible;
                (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Children.Add(motionPathObject);
                //(owner as ObjectElement)?.SupportElements.Add(motionPathObject);
                motionPathObject.IsSelected = true;
                RaiseCompletedEvent(this, new CompletedEventArgs(motionPathAnimation, motionPathObject));
            }
        }

        protected void SetAnimation(Func<IAnimationableObject, TimeSpan, eAnimationType, IAnimation> createAnimation, eAnimationType animationType, bool none = false)
        {
            IList<ObjectElement> owners = GetOwners();
            if (owners != null)
            {
                List<SetAnimationPropertyUndoData> dataUndo = new List<SetAnimationPropertyUndoData>();
                foreach (ObjectElement owner in owners)
                {
                    if (animationType == eAnimationType.Entrance)
                    {
                        IAnimation animation = createAnimation(owner, GenerationAnimationViewModel.DURATION, animationType);
                        dataUndo.Add(new SetAnimationPropertyUndoData(owner, ObjectElement.EntranceAnimationProperty, owner.EntranceAnimation, animation));
                        owner.EntranceAnimation = animation;    
                    }
                    if (animationType == eAnimationType.Exit)
                    {
                        IAnimation animation = createAnimation(owner, GenerationAnimationViewModel.DURATION, animationType);
                        dataUndo.Add(new SetAnimationPropertyUndoData(owner, ObjectElement.ExitAnimationProperty, owner.ExitAnimation, animation));
                        owner.ExitAnimation = animation;
                    }
                }
                Global.PushUndo(new SetAnimationPropertyUndo(dataUndo));
            }
        }

        protected void SetAnimation(string ownerId, Func<IAnimationableObject, TimeSpan, eAnimationType, IAnimation> createAnimation, eAnimationType animationType, bool none = false)
        {
            ObjectElement owner = new ObjectElement();
            bool isFounded = false;
            foreach (ObjectElement item in (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements)
            {
                if (item.ID == ownerId)
                {
                    owner = item;
                    isFounded = true;
                    break;
                }
            }
            if (isFounded)
            {
                //---------------------------------------------------------------------------
                // Chỉ thêm 'ngôi sao hiệu ứng' khi ObjectElement chưa có nó
                //---------------------------------------------------------------------------


                if (animationType == eAnimationType.Entrance)
                {
                    owner.EntranceAnimation = createAnimation(owner, GenerationAnimationViewModel.DURATION, animationType);
                }
                if (animationType == eAnimationType.Exit)
                {
                    owner.ExitAnimation = createAnimation(owner, GenerationAnimationViewModel.DURATION, animationType);
                }
            }
        }

        protected void SetAnimation(ObjectElement owner, Func<IAnimationableObject, TimeSpan, eAnimationType, IAnimation> createAnimation, eAnimationType animationType, bool none = false)
        {
            if (owner != null)
            {
                //---------------------------------------------------------------------------
                // Chỉ thêm 'ngôi sao hiệu ứng' khi ObjectElement chưa có nó
                //---------------------------------------------------------------------------


                if (animationType == eAnimationType.Entrance)
                {
                    owner.EntranceAnimation = createAnimation(owner, GenerationAnimationViewModel.DURATION, animationType);
                }
                if (animationType == eAnimationType.Exit)
                {
                    owner.ExitAnimation = createAnimation(owner, GenerationAnimationViewModel.DURATION, animationType);
                }
            }
        }

        protected void SetPathAnimation(Adorner adorner)
        {
            if (AdornerLayer.GetAdornerLayer((Application.Current as IAppGlobal).SelectedSlide.SelectedLayout) is AdornerLayer layer)
            {
                layer.Add(adorner);
            }
        }

        /// <summary>
        /// Tạo đường Motion Path có khung bao
        /// </summary>
        /// <param name="shape"></param>
        /// <param name="rect"></param>
        /// <returns></returns>
        protected MotionPathObject GenerateMotionPathObject(ShapeBase shape, Rect rect)
        {
            MotionPathObject obj = new MotionPathObject
            {
                Content = shape,
                Width = rect.Width,
                Height = rect.Height,
                Left = rect.Left,
                Top = rect.Top
            };

            Binding bindWidth = new Binding()
            {
                Path = new PropertyPath("Width"),
                Source = obj
            };

            Binding bindHeight = new Binding()
            {
                Path = new PropertyPath("Height"),
                Source = obj
            };

            Binding bindIsSelected = new Binding()
            {
                Path = new PropertyPath("IsSelected"),
                Source = obj
            };

            BindingOperations.SetBinding(shape, ShapeBase.WidthProperty, bindWidth);
            BindingOperations.SetBinding(shape, ShapeBase.HeightProperty, bindHeight);
            BindingOperations.SetBinding(shape, ShapeBase.IsSelectedProperty, bindIsSelected);

            return obj;
        }

        /// <summary>
        /// Hủy chọn các đối tượng trên canvas
        /// </summary>
        /// <param name="canvas">Canvas chứa các đối tượng cần được hủy chọn</param>
        protected void SetChildrenUnSelected()
        {
            foreach (var child in (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements)
            {
                child.IsSelected = false;
            }
        }

        protected List<ObjectElement> GetOwners()
        {
            List<ObjectElement> owners = new List<ObjectElement>();

            foreach (var child in (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements)
            {
                if (child.IsSelected)
                {
                    owners.Add(child);
                }
            }

            foreach (var child in (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Children)
            {
                if (child is MotionPathObject motionPathObject)
                {
                    if (motionPathObject.IsSelected)
                    {
                        if (motionPathObject.Owner is IAnimationableObject)
                        {
                            ObjectElement owner = motionPathObject.Owner;
                            owners.Add(owner);
                        }
                    }
                }

            }

            return owners;
        }
        #endregion

        #region Contants
        //---------------------------------------------------------------------------
        // Các thông số dùng để vẽ đường Motion Path
        //---------------------------------------------------------------------------
        protected static Brush STROKE_DYNAMIC = (Brush)(new BrushConverter().ConvertFrom("#4978A2"));
        protected static Brush STROKE_STATIC = (Brush)(new BrushConverter().ConvertFrom("#BCBCBC"));
        protected static double STROKE_THICKNESS = 1.0;
        #endregion
    }

    public enum DrawingAdorner
    {
        Freeform,
        Scribble,
        Curve
    }
}
