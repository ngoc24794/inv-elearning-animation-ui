﻿using INV.Elearing.Controls.Shapes.Helpers;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Controls.Geometries;
using System.Windows;
using System.Windows.Media;

namespace INV.Elearning.Animations.UI
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: CustomPath.cs
    // Description: Lớp cơ sở cho các đường vẽ tự do
    // Develope by : Nguyen Van Ngoc
    // History:
    // 09/02/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Lớp cơ sở cho các đường vẽ tự do
    /// </summary>
    public abstract class Arrow : GeometryBase
    {
        public EVector Position { get; set; }
        public bool IsAccentBar { get; set; }

        public override Geometry ToGeometry()
        {
            double
                oL = Position.GetLength();

            if (oL == 0) return Geometry.Empty;

            Vector
                o = (Vector)Position.ConvertToPoint(),
                v1 = o * height / oL,
                v2 = new Vector(-v1.Y, v1.X),
                v3 = -v2,
                oP = (Vector)Position.Head,
                p1 = oP + v1,
                p2 = oP + v2,
                p3 = oP + v3;

            PointCollection points = new PointCollection()
            {
                (Point)p1,(Point)p2,(Point)p3
            };

            PathGeometry pathGeometry = new PathGeometry();
            PathGeometry pathGeometry1 = GeometryHelpers.GetPathGeometry(points, true, true, true);
            pathGeometry.AddGeometry(pathGeometry1);


            //---------------------------------------------------------------------------
            // Đoạn thẳng chắn ngang đầu mũi tên
            //---------------------------------------------------------------------------
            if (IsAccentBar)
            {
                Vector
                    p4 = p2 + v1,
                    p5 = p3 + v1;

                points = new PointCollection()
                {
                    (Point)p4, (Point)p5
                };

                PathGeometry pathGeometry2 = GeometryHelpers.GetPathGeometry(points);
                pathGeometry.AddGeometry(pathGeometry2);
            }

            return pathGeometry;
        }        

        protected double width = 20;
        protected double height = 10;

        public Arrow(EVector position)
        {
            Position = position;
        }

    }
}
