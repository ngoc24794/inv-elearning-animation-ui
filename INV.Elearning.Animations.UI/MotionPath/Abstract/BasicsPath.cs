﻿using INV.Elearning.Animations.Enums;
using System;

namespace INV.Elearning.Animations.UI
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: BasicsPath.cs
    // Description: Các loại đường Motion Path được tạo sẵn
    // Develope by : Nguyen Van Ngoc
    // History:
    // 22/02/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Lớp cơ sở để tạo các loại đường Motion Path có hình dạng thuộc các dạng hình cơ bản
    /// (như đường tròn, hình chữ nhật,...)
    /// </summary>
    public abstract class BasicsPath : CustomPath
    {
        public BasicGeometryBase Geometry { get; set; }
        protected BasicsPath() : base()
        {
        }

        protected void GetEffectOption(out eObjectAnimationOption direction)
        {
            direction = eObjectAnimationOption.Down;
            if (Owner != null)
                foreach (IEffectOptionGroup optionGroup in Owner.EffectOptions)
                {
                    foreach (IEffectOption option in optionGroup.Values)
                    {
                        if (option.IsSelected && option.Type == eObjectAnimationOption.Direction)
                        {
                            if (Enum.TryParse(option.Value, out eObjectAnimationOption value))
                            {
                                direction = value;
                            }
                        }
                    }
                }
        }

        public BasicGeometryBase GetGeometry()
        {
            DefiningGeometry();
            return Geometry;
        }
        //public override object Clone()
        //{
        //    return new CirclePath()
        //    {
        //        Points = Points,
        //        VHead = VHead,
        //        VEnd = VEnd,
        //        Owner = Owner
        //    };
        //}
    }
}
