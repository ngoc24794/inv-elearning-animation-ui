﻿namespace INV.Elearning.Animations.UI
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: MotionPathFactory.cs
    // Description: Lớp cơ sở có chức năng tạo các đường Motion Path
    // Develope by : Nguyen Van Ngoc
    // History:
    // 09/02/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Lớp cơ sở có chức năng tạo các đường Motion Path
    /// </summary>
    public abstract class MotionPathFactoryBase
    {
        public abstract ICustomPath CreateLinePath();
        public abstract ICustomPath CreateArcsPath();
        public abstract ICustomPath CreateTurnsPath();
        public abstract ICustomPath CreateCirclePath();
        public abstract ICustomPath CreateSquarePath();
        public abstract ICustomPath CreateEqualTrianglePath();
        public abstract ICustomPath CreateTrapezoidPath();
        public abstract ICustomPath CreateFreeformPath();
        public abstract ICustomPath CreateScribblePath();
        public abstract ICustomPath CreateCurvePath();
    }
}
