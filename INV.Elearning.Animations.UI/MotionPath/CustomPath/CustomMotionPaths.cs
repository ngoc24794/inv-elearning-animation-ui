﻿using INV.Elearing.Controls.Shapes;
using INV.Elearing.Controls.Shapes.Helpers;
using System.Globalization;
using System.Windows.Media;

namespace INV.Elearning.Animations.UI
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: CustomMotionPaths.cs
    // Description: Các loại đường Motion Path được vẽ bởi người dùng
    // Develope by : Nguyen Van Ngoc
    // History:
    // 09/02/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Đường vẽ tự do
    /// </summary>
    public class FreeformPath : CustomPath
    {
        public FreeformPath() : base()
        {
            ShapeName = "Freeform Motion Path";
        }

        protected override void DefiningGeometry()
        {
            if (Points != null && Points.Count > 1)
            {
                Fill = null;
                PathGeometry.Clear();
                PathGeometry.AddGeometry(GeometryHelpers.GetPathGeometry(Points));
                Data = PathGeometry.ToString(CultureInfo.InvariantCulture);
                InvalidateVisual();
            }
        }

        public override object Clone()
        {
            return new FreeformPath()
            {
                Points = Points,
                VHead = VHead,
                VEnd = VEnd,
                Owner = Owner
            };
        }

        public override ShapeInfo GetInfo()
        {
            return new FreeformPathInfo(Points?.ToString());
        }
    }
    /// <summary>
    /// Đường vẽ tự do
    /// </summary>
    public class ScribblePath : CustomPath
    {
        public ScribblePath() : base()
        {
            ShapeName = "Scribble Motion Path";
        }

        protected override void DefiningGeometry()
        {
            if (Points != null && Points.Count > 1)
            {
                Fill = null;
                PathGeometry.Clear();
                PathGeometry.AddGeometry(GeometryHelpers.GetPathGeometry(Points));
                Data = PathGeometry.ToString(CultureInfo.InvariantCulture);
                InvalidateVisual();
            }
        }

        public override object Clone()
        {
            return new ScribblePath()
            {
                Points = Points,
                VHead = VHead,
                VEnd = VEnd,
                Owner = Owner
            };
        }

        public override ShapeInfo GetInfo()
        {
            return new ScribblePathInfo(Points?.ToString());
        }
    }
    /// <summary>
    /// Đường vẽ tự do
    /// </summary>
    public class CurvePath : CustomPath
    {
        public CurvePath() : base()
        {
            ShapeName = "Curve Motion Path";
        }

        protected override void DefiningGeometry()
        {
            if (Points != null && Points.Count > 1)
            {
                Fill = null;
                PathGeometry.Clear();
                PathGeometry = CurveHelpers.GetSingleCurveGeometry(Points);
                //PathGeometry.AddGeometry(CurveHelpers.GetCurveGeometry(Points));
                Data = PathGeometry.ToString(CultureInfo.InvariantCulture);
                InvalidateVisual();
            }
        }

        public override object Clone()
        {
            return new CurvePath()
            {
                Points = Points,
                VHead = VHead,
                VEnd = VEnd,
                Owner = Owner
            };
        }

        public override ShapeInfo GetInfo()
        {
            return new CurvePathInfo(Points?.ToString());
        }
    }
}
