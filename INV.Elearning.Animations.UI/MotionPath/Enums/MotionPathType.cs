﻿using System.Xml.Serialization;

namespace INV.Elearning.Animations.UI
{
    public enum MotionPathType
    {
        [XmlEnum("l")]
        Line,
        [XmlEnum("a")]
        Arc,
        [XmlEnum("t")]
        Turn,
        [XmlEnum("c")]
        Circle,
        [XmlEnum("s")]
        Square,
        [XmlEnum("et")]
        EqualTriangle,
        [XmlEnum("tz")]
        Trapezoid,
        [XmlEnum("ff")]
        Freeform,
        [XmlEnum("scb")]
        Scribble,
        [XmlEnum("cur")]
        Curve
    }
}
