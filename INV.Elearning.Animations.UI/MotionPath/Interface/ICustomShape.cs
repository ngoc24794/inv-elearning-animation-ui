﻿using System;
using System.Windows.Media;

namespace INV.Elearning.Animations.UI
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: ICustomPath.cs
    // Description: Interface cho các đường Motion Path
    // Develope by : Nguyen Van Ngoc
    // History:
    // 09/02/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Interface cho các đường Motion Path
    /// </summary>
    public interface ICustomPath : IMotionPath, ICloneable
    {
        PointCollection Points { get; set; }
    }
}
