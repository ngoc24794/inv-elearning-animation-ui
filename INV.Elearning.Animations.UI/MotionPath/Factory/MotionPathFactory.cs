﻿namespace INV.Elearning.Animations.UI
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: MotionPathFactory.cs
    // Description: Lớp có chức năng tạo các đường Motion Path
    // Develope by : Nguyen Van Ngoc
    // History:
    // 09/02/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Lớp có chức năng tạo các đường Motion Path
    /// </summary>
    public class MotionPathFactory : MotionPathFactoryBase
    {
        public override ICustomPath CreateArcsPath()
        {
            return new ArcsPath();
        }

        public override ICustomPath CreateCirclePath()
        {
            return new CirclePath();
        }
        
        public override ICustomPath CreateCurvePath()
        {
            return new CurvePath();
        }
        public override ICustomPath CreateEqualTrianglePath()
        {
            return new EqualTrianglePath();
        }

        public override ICustomPath CreateFreeformPath()
        {
            return new FreeformPath();
        }

        public override ICustomPath CreateLinePath()
        {
            return new LinePath();
        }

        public override ICustomPath CreateScribblePath()
        {
            return new ScribblePath();
        }

        public override ICustomPath CreateSquarePath()
        {
            return new SquarePath();
        }

        public override ICustomPath CreateTrapezoidPath()
        {
            return new TrapesoidPath();
        }

        public override ICustomPath CreateTurnsPath()
        {
            return new TurnsPath();
        }
    }
}
