﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  CompletedEventArgs.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using System;

namespace INV.Elearning.Animations.UI
{
    public class CompletedEventArgs : EventArgs
    {
        public CompletedEventArgs(object data, MotionPathObject motionPathObject)
        {
            Data = data;
            MotionPathObject = motionPathObject;
        }

        public object Data { get; set; }
        public MotionPathObject MotionPathObject { get; set; }
    }
}
