﻿using INV.Elearning.Animations.Enums;
using INV.Elearning.Core.View;
using System;

namespace INV.Elearning.Animations.UI
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: GenerationAnimationFactory.cs
    // Description: Lớp có chức năng thêm hiệu ứng cho đối tượng
    // Develope by : Nguyen Van Ngoc
    // History:
    // 09/02/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Lớp có chức năng thêm hiệu ứng cho đối tượng
    /// </summary>
    public class GenerationAnimationFactory : GenerationAnimationFactoryBase
    {
        public GenerationAnimationFactory() : base()
        {
        }

        public override void CreateArcsAnimationPath()
        {
            SetAnimation(AnimationFactory.CreateArcAnimation, MotionPathFactory.CreateArcsPath());
        }

        public override void CreateArcsAnimationPath(string ownerId)
        {
            SetAnimation(ownerId, AnimationFactory.CreateArcAnimation, MotionPathFactory.CreateArcsPath());
        }

        public override void CreateArcsAnimationPath(ObjectElement @object)
        {
            SetAnimation(@object, AnimationFactory.CreateArcAnimation, MotionPathFactory.CreateArcsPath());
        }

        public override void CreateBlindsTransition()
        {
            throw new NotImplementedException();
        }

        public override void CreateBounceAnimation(eAnimationType animationType)
        {
            SetAnimation(AnimationFactory.CreateBounceAnimation, animationType);
        }

        public override void CreateBounceAnimation(string ownerId, eAnimationType animationType)
        {
            SetAnimation(ownerId, AnimationFactory.CreateBounceAnimation, animationType);
        }

        public override void CreateBounceAnimation(ObjectElement @object, eAnimationType animationType)
        {
            SetAnimation(@object, AnimationFactory.CreateBounceAnimation, animationType);
        }

        public override void CreateCheckerboardTransition()
        {
            throw new NotImplementedException();
        }

        public override void CreateCircleAnimationPath()
        {
            SetAnimation(AnimationFactory.CreateCircleAnimation, MotionPathFactory.CreateCirclePath());
        }

        public override void CreateCircleAnimationPath(string ownerId)
        {
            SetAnimation(ownerId, AnimationFactory.CreateCircleAnimation, MotionPathFactory.CreateCirclePath());
        }

        public override void CreateCircleAnimationPath(ObjectElement @object)
        {
            SetAnimation(@object, AnimationFactory.CreateCircleAnimation, MotionPathFactory.CreateCirclePath());
        }

        public override void CreateCircleTransition()
        {
            throw new NotImplementedException();
        }

        public override void CreateClockTransition()
        {
            throw new NotImplementedException();
        }

        public override void CreateCoverTransition()
        {
            throw new NotImplementedException();
        }

        public override void CreateCurveAnimationPath()
        {
            DrawingMotionPathAdorner drawingAdorner = DrawingMotionPathAdornerFactory.CreateDrawingCurveAdorner();
            drawingAdorner.Completed += DrawingAdorner_Completed;
            SetPathAnimation(drawingAdorner);

        }

        private void DrawingAdorner_Completed(object sender, CompletedEventArgs e)
        {
            RaiseCompletedEvent(sender, e);
        }

        public override void CreateCurveAnimationPath(string ownerId)
        {
            DrawingMotionPathAdorner drawingAdorner = DrawingMotionPathAdornerFactory.CreateDrawingCurveAdorner();

            // Đặt chế độ chọn là tìm kiếm theo id là ownerId
            drawingAdorner.SelectionMode = SelectionMode.Find;
            drawingAdorner.OwnerId = ownerId;

            drawingAdorner.Completed += DrawingAdorner_Completed;
            SetPathAnimation(drawingAdorner);
        }

        public override void CreateCurveAnimationPath(ObjectElement @object)
        {
            DrawingMotionPathAdorner drawingAdorner = DrawingMotionPathAdornerFactory.CreateDrawingCurveAdorner();

            drawingAdorner.SelectionMode = SelectionMode.Assigned;
            drawingAdorner.Owner = @object;

            drawingAdorner.Completed += DrawingAdorner_Completed;
            SetPathAnimation(drawingAdorner);
        }

        public override void CreateDiamondTransition()
        {
            throw new NotImplementedException();
        }

        public override void CreateDissolveTransition()
        {
            throw new NotImplementedException();
        }

        public override void CreateEqualTriangleAnimationPath()
        {
            SetAnimation(AnimationFactory.CreateEqualTriangleAnimation, MotionPathFactory.CreateEqualTrianglePath());
        }

        public override void CreateEqualTriangleAnimationPath(string ownerId)
        {
            SetAnimation(ownerId, AnimationFactory.CreateEqualTriangleAnimation, MotionPathFactory.CreateEqualTrianglePath());
        }

        public override void CreateEqualTriangleAnimationPath(ObjectElement @object)
        {
            SetAnimation(@object, AnimationFactory.CreateEqualTriangleAnimation, MotionPathFactory.CreateEqualTrianglePath());
        }

        public override void CreateFadeAnimation(eAnimationType animationType)
        {
            SetAnimation(AnimationFactory.CreateFadeAnimation, animationType);
        }

        public override void CreateFadeAnimation(string ownerId, eAnimationType animationType)
        {
            SetAnimation(ownerId, AnimationFactory.CreateFadeAnimation, animationType);
        }

        public override void CreateFadeAnimation(ObjectElement @object, eAnimationType animationType)
        {
            SetAnimation(@object, AnimationFactory.CreateFadeAnimation, animationType);
        }

        public override void CreateFadeTransition()
        {
            throw new NotImplementedException();
        }

        public override void CreateFloatAnimation(eAnimationType animationType)
        {
            SetAnimation(AnimationFactory.CreateFloatAnimation, animationType);
        }

        public override void CreateFloatAnimation(string ownerId, eAnimationType animationType)
        {
            SetAnimation(ownerId, AnimationFactory.CreateFloatAnimation, animationType);
        }

        public override void CreateFloatAnimation(ObjectElement @object, eAnimationType animationType)
        {
            SetAnimation(@object, AnimationFactory.CreateFloatAnimation, animationType);
        }

        public override void CreateFlyAnimation(eAnimationType animationType)
        {
            SetAnimation(AnimationFactory.CreateFlyAnimation, animationType);
        }

        public override void CreateFlyAnimation(string ownerId, eAnimationType animationType)
        {
            SetAnimation(ownerId, AnimationFactory.CreateFlyAnimation, animationType);
        }

        public override void CreateFlyAnimation(ObjectElement @object, eAnimationType animationType)
        {
            SetAnimation(@object, AnimationFactory.CreateFlyAnimation, animationType);
        }

        public override void CreateFreeformAnimationPath()
        {
            SetPathAnimation(DrawingMotionPathAdornerFactory.CreateDrawingFreeformAdorner());
        }

        public override void CreateFreeformAnimationPath(string ownerId)
        {
            DrawingMotionPathAdorner drawingAdorner = DrawingMotionPathAdornerFactory.CreateDrawingFreeformAdorner();

            // Đặt chế độ chọn là tìm kiếm theo id là ownerId
            drawingAdorner.SelectionMode = SelectionMode.Find;
            drawingAdorner.OwnerId = ownerId;

            drawingAdorner.Completed += DrawingAdorner_Completed;
            SetPathAnimation(drawingAdorner);
        }

        public override void CreateFreeformAnimationPath(ObjectElement @object)
        {
            DrawingMotionPathAdorner drawingAdorner = DrawingMotionPathAdornerFactory.CreateDrawingFreeformAdorner();

            drawingAdorner.SelectionMode = SelectionMode.Assigned;
            drawingAdorner.Owner = @object;

            drawingAdorner.Completed += DrawingAdorner_Completed;
            SetPathAnimation(drawingAdorner);
        }

        public override void CreateGrowAnimation(eAnimationType animationType)
        {
            SetAnimation(AnimationFactory.CreateGrowAnimation, animationType);
        }

        public override void CreateGrowAnimation(string ownerId, eAnimationType animationType)
        {
            SetAnimation(ownerId, AnimationFactory.CreateGrowAnimation, animationType);
        }

        public override void CreateGrowAnimation(ObjectElement @object, eAnimationType animationType)
        {
            SetAnimation(@object, AnimationFactory.CreateGrowAnimation, animationType);
        }

        public override void CreateGrowSpinAnimation(eAnimationType animationType)
        {
            SetAnimation(AnimationFactory.CreateGrowSpinAnimation, animationType);
        }

        public override void CreateGrowSpinAnimation(string ownerId, eAnimationType animationType)
        {
            SetAnimation(ownerId, AnimationFactory.CreateGrowSpinAnimation, animationType);
        }

        public override void CreateGrowSpinAnimation(ObjectElement @object, eAnimationType animationType)
        {
            SetAnimation(@object, AnimationFactory.CreateGrowSpinAnimation, animationType);
        }

        public override void CreateInTransition()
        {
            throw new NotImplementedException();
        }

        public override void CreateLineAnimationPath()
        {
            SetAnimation(AnimationFactory.CreateLineAnimation, MotionPathFactory.CreateLinePath());
        }

        public override void CreateLineAnimationPath(string ownerId)
        {
            SetAnimation(ownerId, AnimationFactory.CreateLineAnimation, MotionPathFactory.CreateLinePath());
        }

        public override void CreateLineAnimationPath(ObjectElement @object)
        {
            SetAnimation(@object, AnimationFactory.CreateLineAnimation, MotionPathFactory.CreateLinePath());
        }

        public override void CreateMultiAnimation(eAnimationType animationType)
        {
            SetAnimation(AnimationFactory.CreateMultiAnimation, animationType);
        }

        public override void CreateMultiAnimation(string ownerId, eAnimationType animationType)
        {
            SetAnimation(ownerId, AnimationFactory.CreateMultiAnimation, animationType);
        }

        public override void CreateMultiAnimation(ObjectElement @object, eAnimationType animationType)
        {
            SetAnimation(@object, AnimationFactory.CreateMultiAnimation, animationType);
        }

        public override void CreateNewsflashTransition()
        {
            throw new NotImplementedException();
        }

        public override void CreateNoneAnimation(eAnimationType animationType)
        {
            SetAnimation(AnimationFactory.CreateNoneAnimation, animationType, true);
        }

        public override void CreateNoneAnimation(string ownerId, eAnimationType animationType)
        {
            SetAnimation(ownerId, AnimationFactory.CreateNoneAnimation, animationType);
        }

        public override void CreateNoneAnimation(ObjectElement @object, eAnimationType animationType)
        {
            SetAnimation(@object, AnimationFactory.CreateNoneAnimation, animationType);
        }

        public override void CreateNoneTransition()
        {
            throw new NotImplementedException();
        }

        public override void CreateOutTransition()
        {
            throw new NotImplementedException();
        }

        public override void CreatePlusTransition()
        {
            throw new NotImplementedException();
        }

        public override void CreatePushTransition()
        {
            throw new NotImplementedException();
        }

        public override void CreateRandomBarsAnimation(eAnimationType animationType)
        {
            SetAnimation(AnimationFactory.CreateRandomBarsAnimation, animationType);
        }

        public override void CreateRandomBarsAnimation(string ownerId, eAnimationType animationType)
        {
            SetAnimation(ownerId, AnimationFactory.CreateRandomBarsAnimation, animationType);
        }

        public override void CreateRandomBarsAnimation(ObjectElement @object, eAnimationType animationType)
        {
            SetAnimation(@object, AnimationFactory.CreateRandomBarsAnimation, animationType);
        }

        public override void CreateRandomBarsTransition()
        {
            throw new NotImplementedException();
        }

        public override void CreateScribbleAnimationPath()
        {
            SetPathAnimation(DrawingMotionPathAdornerFactory.CreateDrawingScribbleAdorner());
        }

        public override void CreateScribbleAnimationPath(string ownerId)
        {
            DrawingMotionPathAdorner drawingAdorner = DrawingMotionPathAdornerFactory.CreateDrawingScribbleAdorner();

            // Đặt chế độ chọn là tìm kiếm theo id là ownerId
            drawingAdorner.SelectionMode = SelectionMode.Find;
            drawingAdorner.OwnerId = ownerId;

            drawingAdorner.Completed += DrawingAdorner_Completed;
            SetPathAnimation(drawingAdorner);
        }

        public override void CreateScribbleAnimationPath(ObjectElement @object)
        {
            DrawingMotionPathAdorner drawingAdorner = DrawingMotionPathAdornerFactory.CreateDrawingScribbleAdorner();

            drawingAdorner.SelectionMode = SelectionMode.Assigned;
            drawingAdorner.Owner = @object;

            drawingAdorner.Completed += DrawingAdorner_Completed;
            SetPathAnimation(drawingAdorner);
        }

        public override void CreateShapeAnimation(eAnimationType animationType)
        {
            SetAnimation(AnimationFactory.CreateShapeAnimation, animationType);
        }

        public override void CreateShapeAnimation(string ownerId, eAnimationType animationType)
        {
            SetAnimation(ownerId, AnimationFactory.CreateShapeAnimation, animationType);
        }

        public override void CreateShapeAnimation(ObjectElement @object, eAnimationType animationType)
        {
            SetAnimation(@object, AnimationFactory.CreateShapeAnimation, animationType);
        }

        public override void CreateSpinAnimation(eAnimationType animationType)
        {
            SetAnimation(AnimationFactory.CreateSpinAnimation, animationType);
        }

        public override void CreateSpinAnimation(string ownerId, eAnimationType animationType)
        {
            SetAnimation(ownerId, AnimationFactory.CreateSpinAnimation, animationType);
        }

        public override void CreateSpinAnimation(ObjectElement @object, eAnimationType animationType)
        {
            SetAnimation(@object, AnimationFactory.CreateSpinAnimation, animationType);
        }

        public override void CreateSpinGrowAnimation(eAnimationType animationType)
        {
            SetAnimation(AnimationFactory.CreateSpinGrowAnimation, animationType);
        }

        public override void CreateSpinGrowAnimation(string ownerId, eAnimationType animationType)
        {
            SetAnimation(ownerId, AnimationFactory.CreateSpinGrowAnimation, animationType);
        }

        public override void CreateSpinGrowAnimation(ObjectElement @object, eAnimationType animationType)
        {
            SetAnimation(@object, AnimationFactory.CreateSpinGrowAnimation, animationType);
        }

        public override void CreateSplitAnimation(eAnimationType animationType)
        {
            SetAnimation(AnimationFactory.CreateSplitAnimation, animationType);
        }

        public override void CreateSplitAnimation(string ownerId, eAnimationType animationType)
        {
            SetAnimation(ownerId, AnimationFactory.CreateSplitAnimation, animationType);
        }

        public override void CreateSplitAnimation(ObjectElement @object, eAnimationType animationType)
        {
            SetAnimation(@object, AnimationFactory.CreateSplitAnimation, animationType);
        }

        public override void CreateSplitTransition()
        {
            throw new NotImplementedException();
        }

        public override void CreateSquareAnimationPath()
        {
            SetAnimation(AnimationFactory.CreateSquareAnimation, MotionPathFactory.CreateSquarePath());
        }

        public override void CreateSquareAnimationPath(string ownerId)
        {
            SetAnimation(ownerId, AnimationFactory.CreateSquareAnimation, MotionPathFactory.CreateSquarePath());
        }

        public override void CreateSquareAnimationPath(ObjectElement @object)
        {
            SetAnimation(@object, AnimationFactory.CreateSquareAnimation, MotionPathFactory.CreateSquarePath());
        }

        public override void CreateSwivelAnimation(eAnimationType animationType)
        {
            SetAnimation(AnimationFactory.CreateSwivelAnimation, animationType);
        }

        public override void CreateSwivelAnimation(string ownerId, eAnimationType animationType)
        {
            SetAnimation(ownerId, AnimationFactory.CreateSwivelAnimation, animationType);
        }

        public override void CreateSwivelAnimation(ObjectElement @object, eAnimationType animationType)
        {
            SetAnimation(@object, AnimationFactory.CreateSwivelAnimation, animationType);
        }

        public override void CreateTrapezoidAnimationPath()
        {
            SetAnimation(AnimationFactory.CreateTrapesoidAnimation, MotionPathFactory.CreateTrapezoidPath());
        }

        public override void CreateTrapezoidAnimationPath(string ownerId)
        {
            SetAnimation(ownerId, AnimationFactory.CreateTrapesoidAnimation, MotionPathFactory.CreateTrapezoidPath());
        }

        public override void CreateTrapezoidAnimationPath(ObjectElement @object)
        {
            SetAnimation(@object, AnimationFactory.CreateTrapesoidAnimation, MotionPathFactory.CreateTrapezoidPath());
        }

        public override void CreateTurnsAnimationPath()
        {
            SetAnimation(AnimationFactory.CreateTurnsAnimation, MotionPathFactory.CreateTurnsPath());
        }

        public override void CreateTurnsAnimationPath(string ownerId)
        {
            SetAnimation(ownerId, AnimationFactory.CreateTurnsAnimation, MotionPathFactory.CreateTurnsPath());
        }

        public override void CreateTurnsAnimationPath(ObjectElement @object)
        {
            SetAnimation(@object, AnimationFactory.CreateTurnsAnimation, MotionPathFactory.CreateTurnsPath());
        }

        public override void CreateUnCoverTransition()
        {
            throw new NotImplementedException();
        }

        public override void CreateWheelAnimation(eAnimationType animationType)
        {
            SetAnimation(AnimationFactory.CreateWheelAnimation, animationType);
        }

        public override void CreateWheelAnimation(string ownerId, eAnimationType animationType)
        {
            SetAnimation(ownerId, AnimationFactory.CreateWheelAnimation, MotionPathFactory.CreateTrapezoidPath());
        }

        public override void CreateWheelAnimation(ObjectElement @object, eAnimationType animationType)
        {
            throw new NotImplementedException();
        }

        public override void CreateWipeAnimation(eAnimationType animationType)
        {
            SetAnimation(AnimationFactory.CreateWipeAnimation, animationType);
        }

        public override void CreateWipeAnimation(string ownerId, eAnimationType animationType)
        {
            SetAnimation(ownerId, AnimationFactory.CreateWipeAnimation, MotionPathFactory.CreateTrapezoidPath());
        }

        public override void CreateWipeAnimation(ObjectElement @object, eAnimationType animationType)
        {
            SetAnimation(@object, AnimationFactory.CreateWipeAnimation, animationType);
        }

        public override void CreateZoomAnimation(eAnimationType animationType)
        {
            SetAnimation(AnimationFactory.CreateZoomAnimation, animationType);
        }

        public override void CreateZoomAnimation(string ownerId, eAnimationType animationType)
        {
            SetAnimation(ownerId, AnimationFactory.CreateZoomAnimation, animationType);
        }

        public override void CreateZoomAnimation(ObjectElement @object, eAnimationType animationType)
        {
            SetAnimation(@object, AnimationFactory.CreateZoomAnimation, animationType);
        }

        public override void CreateZoomTransition()
        {
            throw new NotImplementedException();
        }
    }
}
