﻿using INV.Elearing.Controls.Shapes;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Controls.Geometries;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Media;

namespace INV.Elearning.Animations.UI
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: BasicMotionPaths.cs
    // Description: Các loại đường Motion Path có hình dạng sẵn
    // Develope by : Nguyen Van Ngoc
    // History:
    // 22/02/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Đường Motion Path đoạn thẳng
    /// </summary>
    public class LinePath : BasicsPath
    {
        public LinePath() : base()
        {
            Width = 0;
            Height = 200;
            DefiningGeometry();
            ShapeName = "Line  Motion Path";
        }

        protected override void CustomMethodBeforeDefiningGeometry(DependencyPropertyChangedEventArgs e)
        {
        }


        protected override void DefiningGeometry()
        {
            Fill = null;
            GetEffectOption(out eObjectAnimationOption pathDirection);
            PathGeometry.Clear();
            Geometry = new LineGeometry(Width, Height, pathDirection);
            PathGeometry.AddGeometry(Geometry.ToGeometry());
            Data = PathGeometry.ToString(CultureInfo.InvariantCulture);

            //---------------------------------------------------------------------------
            // Tính VHead và VEnd
            //---------------------------------------------------------------------------

            // Lấy các giá trị tại điểm đầu đường Path.
            if (Width != 0 && Height != 0)
            {
                PathGeometry.GetPointAtFractionLength(0.0, out Point startPoint, out Point startTangent);

                // Lấy các giá trị tại điểm cuối đường Path.
                PathGeometry.GetPointAtFractionLength(1.0, out Point endPoint, out Point endTangent);

                VHead = GetEVector(startPoint, startTangent);
                VEnd = GetEVector(endPoint, endTangent);
            }
            else
            {
                VHead = VEnd = new EVector(new Point(0, 0), new Point(Width, Height));
            }

            InvalidateVisual();
        }

        public override object Clone()
        {
            return new LinePath()
            {
                Points = Points,
                VHead = VHead,
                VEnd = VEnd,
                Owner = Owner,
                PathGeometry = PathGeometry
            };
        }

        public override ShapeInfo GetInfo()
        {
            return new LinePathInfo();
        }

    }

    /// <summary>
    /// Đường Motion Path đoạn thẳng
    /// </summary>
    public class ArcsPath : BasicsPath
    {
        public ArcsPath() : base()
        {
            Width = 200;
            Height = 110;
            ShapeName = "Arcs  Motion Path";
            Data = Arc();
        }

        protected override void CustomMethodBeforeDefiningGeometry(DependencyPropertyChangedEventArgs e)
        {
        }

        protected override void DefiningGeometry()
        {
            if (Width > 0 && Height > 0)
            {
                double
                    width = Width,
                    height = Height;
                GetEffectOption(out eObjectAnimationOption pathDirection);

                Fill = null;
                PathGeometry.Clear();
                Geometry = new ArcGeometry(width, height, pathDirection);
                PathGeometry pathGeometry = PathGeometry.CreateFromGeometry(System.Windows.Media.Geometry.Parse(Arc()));
                //switch (pathDirection)
                //{
                //    case eObjectAnimationOption.Up:
                //        pathGeometry = new ScaleTransform(1, -1).Transform(pathGeometry);
                //        break;
                //    case eObjectAnimationOption.Left:
                //        pathGeometry = new RotateTransform(90).Transform(pathGeometry);
                //        break;
                //    case eObjectAnimationOption.Right:
                //        pathGeometry = new RotateTransform(-90).Transform(pathGeometry);
                //        break;
                //}

                PathGeometry.AddGeometry(pathGeometry);
                //PathGeometry.AddGeometry(Geometry.ToGeometry());
                Data = PathGeometry.ToString(CultureInfo.InvariantCulture);

                //---------------------------------------------------------------------------
                // Tính VHead và VEnd
                //---------------------------------------------------------------------------

                // Lấy các giá trị tại điểm đầu đường Path.
                PathGeometry.GetPointAtFractionLength(0.0, out Point startPoint, out Point startTangent);

                // Lấy các giá trị tại điểm cuối đường Path.
                PathGeometry.GetPointAtFractionLength(1.0, out Point endPoint, out Point endTangent);

                VHead = GetEVector(startPoint, startTangent);
                VEnd = GetEVector(endPoint, endTangent);

                InvalidateVisual();
            }
        }

        private string Arc()
        {
            StaticShape staticShape = new StaticShape()
            {
                Width = Width,
                Height = Height,
                PathData = "M 0.1302 0.1199 L 0.19726 0.28889 C 0.21119 0.32708 0.23216 0.34768 0.25416 0.34768 C 0.27916 0.34768 0.29921 0.32708 0.31315 0.28889 L 0.3802 0.1199"
            };

            return staticShape.Data;
        }



        public override object Clone()
        {
            return new ArcsPath()
            {
                VHead = VHead,
                VEnd = VEnd,
                Owner = Owner,
                PathGeometry = PathGeometry
            };
        }

        public override ShapeInfo GetInfo()
        {
            return new ArcsPathInfo();
        }
    }

    public class TurnsPath : BasicsPath
    {
        public TurnsPath() : base()
        {
            Width = 200;
            Height = 120;
            ShapeName = "Turns  Motion Path";
        }
        protected override void CustomMethodBeforeDefiningGeometry(DependencyPropertyChangedEventArgs e)
        {
        }

        protected override void DefiningGeometry()
        {
            if (Width > 0 && Height > 0)
            {
                double
                    width = Width,
                    height = Height;
                GetEffectOption(out eObjectAnimationOption turnDirection);
                Fill = null;
                PathGeometry.Clear();
                Geometry = new TurnsGeometry(width, height, turnDirection);
                PathGeometry pathGeometry = PathGeometry.CreateFromGeometry(System.Windows.Media.Geometry.Parse(Turn()));
                PathGeometry.AddGeometry(pathGeometry);
                Data = PathGeometry.ToString(CultureInfo.InvariantCulture);

                //---------------------------------------------------------------------------
                // Tính VHead và VEnd
                //---------------------------------------------------------------------------

                // Lấy các giá trị tại điểm đầu đường Path.
                PathGeometry.GetPointAtFractionLength(0.0, out Point startPoint, out Point startTangent);

                // Lấy các giá trị tại điểm cuối đường Path.
                PathGeometry.GetPointAtFractionLength(1.0, out Point endPoint, out Point endTangent);

                VHead = GetEVector(startPoint, startTangent);
                VEnd = GetEVector(endPoint, endTangent);

                InvalidateVisual();
            }
        }

        private string Turn()
        {
            StaticShape shape = new StaticShape()
            {
                Width = Width,
                Height = Height,
                PathData = "M 0 0 L 0.125 0 C 0.181 0 0.25 0.069 0.25 0.125 L 0.25 0.25"
            };
            return shape.Data;
        }

        public override object Clone()
        {
            return new TurnsPath()
            {
                Points = Points,
                VHead = VHead,
                VEnd = VEnd,
                Owner = Owner,
                PathGeometry = PathGeometry
            };
        }

        public override ShapeInfo GetInfo()
        {
            return new TurnsPathInfo();
        }
    }
}
