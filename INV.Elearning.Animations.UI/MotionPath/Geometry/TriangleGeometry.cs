﻿using INV.Elearing.Controls.Shapes.Helpers;
using INV.Elearning.Controls.Geometries;
using System.Windows;
using System.Windows.Media;

namespace INV.Elearning.Animations.UI
{
    public class TriangleGeometry : GeometryBase
    {
        public TriangleGeometry(double width, double height)
        {
            Width = width;
            Height = height;
        }
        public override Geometry ToGeometry()
        {
            PointCollection points = new PointCollection()
            {
                new Point(Width / 2, 0),
                new Point(Width, Height),
                new Point(0, Height)
            };

            PathGeometry pathGeometry = GeometryHelpers.GetPathGeometry(points, true);

            return pathGeometry;
        }
    }
}
