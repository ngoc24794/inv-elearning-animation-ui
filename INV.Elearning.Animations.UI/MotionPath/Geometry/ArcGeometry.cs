﻿using INV.Elearning.Animations.Enums;
using System.Windows;
using System.Windows.Media;

namespace INV.Elearning.Animations.UI
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: ArcGeometry.cs
    // Description: Cung tròn
    // Develope by : Nguyen Van Ngoc
    // History:
    // 23/02/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Cung tròn
    /// </summary>
    public class ArcGeometry : BasicGeometryBase
    {
        public ArcGeometry(double width, double height, eObjectAnimationOption direction) : base(width, height, direction)
        {
        }

        public override Geometry ToGeometry()
        {

            //---------------------------------------------------------------------------
            // Mặc định Direciton == Down
            //---------------------------------------------------------------------------
            SweepDirection sweepDirection = SweepDirection.Counterclockwise;
            Size size = new Size(Width / 2, Height);
            Point point = new Point(Width / 2, Height);
            EndPoint = new Point(Width, 0);
            switch (Direction)
            {
                case eObjectAnimationOption.Up:
                    StartPoint = new Point(0, Height);
                    EndPoint = new Point(Width, Height);
                    point = new Point(Width / 2, 0);
                    sweepDirection = SweepDirection.Clockwise;
                    break;
                case eObjectAnimationOption.Left:
                    StartPoint = new Point(Width, 0);
                    EndPoint = new Point(Width, Height);
                    size = new Size(Width, Height / 2);
                    point = new Point(0, Height / 2);
                    break;
                case eObjectAnimationOption.Right:
                    StartPoint = new Point(0, 0);
                    EndPoint = new Point(0, Height);
                    size = new Size(Width, Height / 2);
                    point = new Point(Width, Height / 2);
                    sweepDirection = SweepDirection.Clockwise;
                    break;
            }

            PathGeometry pathGeometry = new PathGeometry()
            {
                Figures = new PathFigureCollection()
                {
                    new PathFigure()
                    {
                        StartPoint = StartPoint,
                        Segments = new PathSegmentCollection()
                        {
                            new ArcSegment()
                            {
                                Point = point,
                                Size = size,
                                SweepDirection = sweepDirection,
                                IsLargeArc = false
                            },
                            new ArcSegment()
                            {
                                Point = EndPoint,
                                Size = size,
                                SweepDirection = sweepDirection,
                                IsLargeArc = false
                            }
                        }
                    }
                }
            };

            return pathGeometry;
        }
    }
}
