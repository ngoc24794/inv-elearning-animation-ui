﻿using INV.Elearning.Animations.Enums;
using System.Windows;
using System.Windows.Media;

namespace INV.Elearning.Animations.UI
{
    public class TurnsGeometry : BasicGeometryBase
    {
        public TurnsGeometry(double width, double height, eObjectAnimationOption direction) : base(width, height, direction)
        {
        }

        public override Geometry ToGeometry()
        {
            switch (Direction)
            {
                case eObjectAnimationOption.Up:
                case eObjectAnimationOption.UpRight:
                    StartPoint = new Point(0, Height);
                    EndPoint = new Point(Width, 0);
                    break;
                case eObjectAnimationOption.Down:
                case eObjectAnimationOption.DownRight:
                    StartPoint = new Point(0, 0);
                    EndPoint = new Point(Width, Height);
                    break;
                default:
                    StartPoint = new Point(0, 0);
                    EndPoint = new Point(Width, Height);
                    break;
            }


            //---------------------------------------------------------------------------
            // Mặc định Direction == Down
            //---------------------------------------------------------------------------
            SweepDirection sweepDirection = SweepDirection.Clockwise;
            Point
                point1 = new Point(Width / 2, 0),
                point2 = new Point(Width, Height / 2);
            switch (Direction)
            {
                case eObjectAnimationOption.UpRight:
                    point1 = new Point(0, Height / 2);
                    point2 = new Point(Width / 2, 0);
                    sweepDirection = SweepDirection.Clockwise;
                    break;

                case eObjectAnimationOption.DownRight:
                    point1 = new Point(0, Height / 2);
                    point2 = new Point(Width / 2, Height);
                    sweepDirection = SweepDirection.Counterclockwise;
                    break;
                case eObjectAnimationOption.Up:
                    point1 = new Point(Width / 2, Height);
                    point2 = new Point(Width, Height / 2);
                    sweepDirection = SweepDirection.Counterclockwise;
                    break;
            }

            System.Windows.Media.LineGeometry
                line1 = new System.Windows.Media.LineGeometry(StartPoint, point1),
                line2 = new System.Windows.Media.LineGeometry(point2, EndPoint);

            PathGeometry arcGeometry = new PathGeometry()
            {
                Figures = new PathFigureCollection()
                {
                    new PathFigure()
                    {
                        StartPoint=StartPoint,
                        Segments=new PathSegmentCollection()
                        {
                            new LineSegment(point1, true),
                            new ArcSegment()
                            {
                                Point = point2,
                                Size = new Size(Width/2, Height/2),
                                SweepDirection = sweepDirection,
                                IsLargeArc = false
                            },
                            new LineSegment(EndPoint, true)
                        }
                    }
                }
            };

            PathGeometry pathGeometry = new PathGeometry();
            //pathGeometry.AddGeometry(line1);
            pathGeometry.AddGeometry(arcGeometry);
            //pathGeometry.AddGeometry(line2);

            return arcGeometry;
        }
    }
}
