﻿using INV.Elearning.Animations.Enums;
using System.Windows;
using System.Windows.Media;

namespace INV.Elearning.Animations.UI
{
    public class LineGeometry : BasicGeometryBase
    {
        public LineGeometry(double width, double height, eObjectAnimationOption direction) : base(width, height, direction)
        {
        }

        public override Geometry ToGeometry()
        {
            //---------------------------------------------------------------------------
            // Mặc định Direction == Down == Right
            //---------------------------------------------------------------------------
            StartPoint = new Point(0, 0);
            Point point2 = new Point(Width, Height);
            switch (Direction)
            {
                case eObjectAnimationOption.Up:
                case eObjectAnimationOption.Left:
                    StartPoint = new Point(Width, Height);
                    point2 = new Point(0, 0);
                    break;
            }

            System.Windows.Media.LineGeometry pathGeometry = new System.Windows.Media.LineGeometry(StartPoint, point2);

            return pathGeometry;
        }
    }
}
