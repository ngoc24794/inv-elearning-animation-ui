﻿using INV.Elearning.Animations.Enums;
using INV.Elearning.Controls.Geometries;
using System.Windows;
using System.Windows.Media;

namespace INV.Elearning.Animations.UI
{
    public abstract class BasicGeometryBase : GeometryBase
    {
        public BasicGeometryBase(double width, double height, eObjectAnimationOption direction)
        {
            Width = width;
            Height = height;
            Direction = direction;
            StartPoint = new Point(0, 0);
        }

        public eObjectAnimationOption Direction { get; set; }
        public Point StartPoint { get; protected set; }
        public Point EndPoint { get; protected set; }
        public override Geometry ToGeometry()
        {
            return Geometry.Empty;
        }

        public Point GetStartPoint()
        {
            ToGeometry();
            return StartPoint;
        }
    }
}
