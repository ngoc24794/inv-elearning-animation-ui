﻿using System.Globalization;
using System.Windows;
using System.Windows.Media;
using INV.Elearing.Controls.Shapes;

namespace INV.Elearning.Animations.UI
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: ShapesMotionPaths.cs
    // Description: Các loại đường Motion Path có hình dạng sẵn
    // Develope by : Nguyen Van Ngoc
    // History:
    // 22/02/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Đường Motion Path hình tròn
    /// </summary>
    public class CirclePath : ShapesPath
    {
        public CirclePath() : base()
        {
            ShapeName = "Circle Motion Path";
        }

        protected override void DefiningGeometry()
        {
            if (Width > 0 && Height > 0)
            {
                Fill = null;
                PathGeometry.Clear();
                PathGeometry.AddGeometry(
                    new EllipseGeometry(
                        center: new Point(Width / 2, Height / 2),
                        radiusX: Width / 2,
                        radiusY: Height / 2
                    ));
                Data = PathGeometry.ToString(CultureInfo.InvariantCulture);

                //---------------------------------------------------------------------------
                // Tính VHead và VEnd
                //---------------------------------------------------------------------------

                // Lấy các giá trị tại điểm đầu đường Path.
                PathGeometry.GetPointAtFractionLength(0.0, out Point startPoint, out Point startTangent);

                // Lấy các giá trị tại điểm cuối đường Path.
                PathGeometry.GetPointAtFractionLength(1.0, out Point endPoint, out Point endTangent);

                VHead = GetEVector(startPoint, startTangent);
                VEnd = GetEVector(endPoint, endTangent);

                InvalidateVisual();
            }
        }

        public override object Clone()
        {
            return new CirclePath()
            {
                Points = Points,
                VHead = VHead,
                VEnd = VEnd,
                Owner = Owner,
                PathGeometry = PathGeometry
            };
        }

        public override ShapeInfo GetInfo()
        {
            return new CircleInfo();
        }
    }

    /// <summary>
    /// Đường Motion Path hình vuông
    /// </summary>
    public class SquarePath : ShapesPath
    {
        public SquarePath() : base()
        {
            ShapeName = "Square  Motion Path";
        }

        protected override void DefiningGeometry()
        {
            if (Width > 0 && Height > 0)
            {
                Fill = null;
                PathGeometry.Clear();
                PathGeometry.AddGeometry(
                    new RectangleGeometry(
                        new Rect(0, 0, Width, Height)
                    ));
                Data = PathGeometry.ToString(CultureInfo.InvariantCulture);

                //---------------------------------------------------------------------------
                // Tính VHead và VEnd
                //---------------------------------------------------------------------------

                // Lấy các giá trị tại điểm đầu đường Path.
                PathGeometry.GetPointAtFractionLength(0.0, out Point startPoint, out Point startTangent);

                // Lấy các giá trị tại điểm cuối đường Path.
                PathGeometry.GetPointAtFractionLength(1.0, out Point endPoint, out Point endTangent);

                VHead = GetEVector(startPoint, startTangent);
                VEnd = GetEVector(endPoint, endTangent);


                InvalidateVisual();
            }
        }

        public override object Clone()
        {
            return new SquarePath()
            {
                Points = Points,
                VHead = VHead,
                VEnd = VEnd,
                Owner = Owner,
                PathGeometry = PathGeometry
            };
        }

        public override ShapeInfo GetInfo()
        {
            return new SquareInfo();
        }
    }

    /// <summary>
    /// Đường Motion Path hình tam giác
    /// </summary>
    public class EqualTrianglePath : ShapesPath
    {
        public EqualTrianglePath() : base()
        {
            ShapeName = "Equal Triangle  Motion Path";
        }
        protected override void DefiningGeometry()
        {
            if (Width > 0 && Height > 0)
            {
                Fill = null;
                PathGeometry.Clear();
                PathGeometry pathGeometry = (new TriangleGeometry(Width, Height)).ToGeometry() as PathGeometry;
                PathGeometry.AddGeometry(pathGeometry);
                Data = PathGeometry.ToString(CultureInfo.InvariantCulture);

                //---------------------------------------------------------------------------
                // Tính VHead và VEnd
                //---------------------------------------------------------------------------

                // Lấy các giá trị tại điểm đầu đường Path.
                PathGeometry.GetPointAtFractionLength(0.0, out Point startPoint, out Point startTangent);

                // Lấy các giá trị tại điểm cuối đường Path.
                PathGeometry.GetPointAtFractionLength(1.0, out Point endPoint, out Point endTangent);

                VHead = GetEVector(startPoint, startTangent);
                VEnd = GetEVector(endPoint, endTangent);

                InvalidateVisual();
            }
        }

        public override object Clone()
        {
            return new EqualTrianglePath()
            {
                Points = Points,
                VHead = VHead,
                VEnd = VEnd,
                Owner = Owner,
                PathGeometry = PathGeometry
            };
        }

        public override ShapeInfo GetInfo()
        {
            return new EqualTrianglePathInfo();
        }
    }

    /// <summary>
    /// Đường Motion Path hình thang
    /// </summary>
    public class TrapesoidPath : ShapesPath
    {
        public TrapesoidPath() : base()
        {
            ShapeName = "Trapezoid  Motion Path";
        }

        protected override void DefiningGeometry()
        {
            if (Width > 0 && Height > 0)
            {
                Fill = null;
                PathGeometry.Clear();
                PathGeometry pathGeometry = (new TrapezoidGeometry(Width, Height)).ToGeometry() as PathGeometry;
                PathGeometry.AddGeometry(pathGeometry);
                Data = PathGeometry.ToString(CultureInfo.InvariantCulture);

                //---------------------------------------------------------------------------
                // Tính VHead và VEnd
                //---------------------------------------------------------------------------

                // Lấy các giá trị tại điểm đầu đường Path.
                PathGeometry.GetPointAtFractionLength(0.0, out Point startPoint, out Point startTangent);

                // Lấy các giá trị tại điểm cuối đường Path.
                PathGeometry.GetPointAtFractionLength(1.0, out Point endPoint, out Point endTangent);

                VHead = GetEVector(startPoint, startTangent);
                VEnd = GetEVector(endPoint, endTangent);

                InvalidateVisual();
            }
        }
        public override object Clone()
        {
            return new TrapesoidPath()
            {
                Points = Points,
                VHead = VHead,
                VEnd = VEnd,
                Owner = Owner,
                PathGeometry = PathGeometry
            };
        }

        public override ShapeInfo GetInfo()
        {
            return new TrapesoidPathInfo();
        }
    }
}
