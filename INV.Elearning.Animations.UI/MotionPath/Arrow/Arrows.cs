﻿using INV.Elearning.Animations.Enums;
using System.Windows;
using System.Windows.Media;

namespace INV.Elearning.Animations.UI
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: CustomPath.cs
    // Description: Lớp cơ sở cho các đường vẽ tự do
    // Develope by : Nguyen Van Ngoc
    // History:
    // 09/02/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Lớp cơ sở cho các đường vẽ tự do
    /// </summary>
    public class BeginArrow : Arrow
    {
        public BeginArrow(EVector position) : base(position)
        {
            IsAccentBar = false;
        }
    }
    public class EndArrow : Arrow
    {
        public EndArrow(EVector position) : base(position)
        {
            IsAccentBar = true;
        }
    }

    public class CircleArrow : Arrow
    {
        public CircleArrow(EVector position) : base(position)
        {
            IsAccentBar = false;
        }

        public override Geometry ToGeometry()
        {
            return new EllipseGeometry(new Rect(0, 0, Width, Height));
        }
    }

}
