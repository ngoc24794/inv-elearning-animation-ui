﻿using INV.Elearing.Controls.Shapes;
using System;
using System.Xml.Serialization;

namespace INV.Elearning.Animations.UI
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: CircleInfo.cs
    // Description: Lớp dữ liệu cho hình CircleShape
    // Develope by : Nguyen Van Ngoc
    // History:
    // 14/4/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Lớp dữ liệu cho hình CircleShape
    /// </summary>
    [Serializable, XmlRoot("circlePathInfo")]
    public class CircleInfo : MotionPathInfo
    {
        public CircleInfo() { }
        public override ShapeBase GetShape()
        {
            return new CirclePath();
        }
    }
}
