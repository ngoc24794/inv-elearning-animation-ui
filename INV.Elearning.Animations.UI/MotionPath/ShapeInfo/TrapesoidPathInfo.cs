﻿using INV.Elearing.Controls.Shapes;
using System;
using System.Xml.Serialization;

namespace INV.Elearning.Animations.UI
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: TrapesoidPathInfo.cs
    // Description: Lớp dữ liệu cho hình TrapesoidPath
    // Develope by : Nguyen Van Ngoc
    // History:
    // 14/4/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Lớp dữ liệu cho hình TrapesoidPath
    /// </summary>
    [Serializable, XmlRoot("trapesoidPathInfo")]
    public class TrapesoidPathInfo : MotionPathInfo
    {
        public TrapesoidPathInfo() { }
        public override ShapeBase GetShape()
        {
            return new TrapesoidPath();
        }
    }
}
