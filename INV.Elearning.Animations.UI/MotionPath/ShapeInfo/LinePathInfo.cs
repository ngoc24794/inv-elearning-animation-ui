﻿using INV.Elearing.Controls.Shapes;
using System;
using System.Xml.Serialization;

namespace INV.Elearning.Animations.UI
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: LinePathInfo.cs
    // Description: Lớp dữ liệu cho hình LinePath
    // Develope by : Nguyen Van Ngoc
    // History:
    // 14/4/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Lớp dữ liệu cho hình LinePath
    /// </summary>
    [Serializable, XmlRoot("linePathInfo")]
    public class LinePathInfo : MotionPathInfo
    {
        public LinePathInfo() { }
        public override ShapeBase GetShape()
        {
            return new LinePath();
        }
    }
}
