﻿using INV.Elearing.Controls.Shapes;
using System;
using System.Xml.Serialization;

namespace INV.Elearning.Animations.UI
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: EqualTrianglePathInfo.cs
    // Description: Lớp dữ liệu cho hình EqualTrianglePath
    // Develope by : Nguyen Van Ngoc
    // History:
    // 14/4/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Lớp dữ liệu cho hình EqualTrianglePath
    /// </summary>
    [Serializable, XmlRoot("equalTrianglePathInfo")]
    public class EqualTrianglePathInfo : MotionPathInfo
    {
        public EqualTrianglePathInfo() { }
        public override ShapeBase GetShape()
        {
            return new EqualTrianglePath();
        }
    }
}
