﻿using INV.Elearing.Controls.Shapes;
using System;
using System.Xml.Serialization;

namespace INV.Elearning.Animations.UI
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: SquareInfo.cs
    // Description: Lớp dữ liệu cho hình SquareShape
    // Develope by : Nguyen Van Ngoc
    // History:
    // 14/4/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Lớp dữ liệu cho hình SquareShape
    /// </summary>
    [Serializable, XmlRoot("squarePathInfo")]
    public class SquareInfo : MotionPathInfo
    {
        public SquareInfo() { }
        public override ShapeBase GetShape()
        {
            return new SquarePath();
        }
    }
}
