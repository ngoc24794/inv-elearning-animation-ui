﻿using INV.Elearing.Controls.Shapes;
using INV.Elearing.Controls.Shapes.Helpers;
using System;
using System.Xml.Serialization;

namespace INV.Elearning.Animations.UI
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: CurvePathInfo.cs
    // Description: Lớp dữ liệu cho đường CurvePath
    // Develope by : Nguyen Van Ngoc
    // History:
    // 14/4/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Lớp dữ liệu cho đường CurvePath
    /// </summary>
    [Serializable, XmlRoot("curvePathInfo")]
    public class CurvePathInfo : ePathInfo
    {
        public CurvePathInfo() { }
        public CurvePathInfo(string points) : base(points)
        {
        }

        public override ShapeBase GetShape()
        {
            CurvePath path = new CurvePath() { Points = Points.ToPointCollection() };
            path.CallDefiningGeometry();
            return path;
        }
    }
}
