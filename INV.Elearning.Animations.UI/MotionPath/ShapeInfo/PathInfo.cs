﻿using System;
using System.Xml.Serialization;

namespace INV.Elearning.Animations.UI
{
    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: PathInfo.cs
    // Description: Các lớp dữ liệu cho các đường được vẽ bằng chuột
    // Develope by : Nguyen Van Ngoc
    // History:
    // 13/4/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Lớp cơ sở của các lớp dữ liệu cho các đường được vẽ bằng chuột
    /// </summary>
    [Serializable, XmlRoot("pInfo")]
    public abstract class ePathInfo : MotionPathInfo
    {
        public ePathInfo() { }
        protected ePathInfo(string points)
        {
            Points = points;
        }

        [XmlAttribute("pts")]
        public string Points { get; set; }
    }

}
