﻿using INV.Elearing.Controls.Shapes;
using INV.Elearing.Controls.Shapes.Helpers;
using System;
using System.Xml.Serialization;

namespace INV.Elearning.Animations.UI
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: FreeformPathInfo.cs
    // Description: Lớp dữ liệu cho đường FreeformPath
    // Develope by : Nguyen Van Ngoc
    // History:
    // 14/4/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Lớp dữ liệu cho đường FreeformPath
    /// </summary>
    [Serializable, XmlRoot("freeformPathInfo")]
    public class FreeformPathInfo : ePathInfo
    {
        public FreeformPathInfo() : base(null) { }
        public FreeformPathInfo(string points) : base(points)
        {
        }

        public override ShapeBase GetShape()
        {
            FreeformPath path = new FreeformPath() { Points = Points.ToPointCollection() };
            path.CallDefiningGeometry();
            return path;
        }
    }
}
