﻿using INV.Elearing.Controls.Shapes;
using System;
using System.Xml.Serialization;

namespace INV.Elearning.Animations.UI
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: ArcsPathInfo.cs
    // Description: Lớp dữ liệu cho hình ArcsPath
    // Develope by : Nguyen Van Ngoc
    // History:
    // 14/4/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Lớp dữ liệu cho hình ArcsPath
    /// </summary>
    [Serializable, XmlRoot("arcsPathInfo")]
    public class ArcsPathInfo : MotionPathInfo
    {
        public ArcsPathInfo()
        {
        }

        public override ShapeBase GetShape()
        {
            return new ArcsPath();
        }
    }
}
