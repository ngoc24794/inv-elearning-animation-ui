﻿using INV.Elearing.Controls.Shapes;
using System;
using System.Xml.Serialization;

namespace INV.Elearning.Animations.UI
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: MotionPathInfo.cs
    // Description: Lớp dữ liệu cho các đường cong chuyển động
    // Develope by : Nguyen Van Ngoc
    // History:
    // 16/4/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Lớp dữ liệu cho các đường cong chuyển động
    /// </summary>
    [Serializable, XmlRoot("motionPathInfo")]
    [XmlInclude(typeof(ArcsPathInfo)), XmlInclude(typeof(CircleInfo)), XmlInclude(typeof(CurvePathInfo)), 
        XmlInclude(typeof(EqualTrianglePathInfo)), XmlInclude(typeof(FreeformPathInfo)), 
        XmlInclude(typeof(LinePathInfo)), XmlInclude(typeof(ePathInfo)), XmlInclude(typeof(ScribblePathInfo)),
        XmlInclude(typeof(SquareInfo)), XmlInclude(typeof(TrapesoidPathInfo)), XmlInclude(typeof(TurnsPathInfo))]
    public abstract class MotionPathInfo : INV.Elearning.Animations.MotionPathInfoBase
    {
        public MotionPathInfo() { }
    }
}
