﻿using INV.Elearing.Controls.Shapes;
using System;
using System.Xml.Serialization;

namespace INV.Elearning.Animations.UI
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: TurnsPathInfo.cs
    // Description: Lớp dữ liệu cho hình TurnsPath
    // Develope by : Nguyen Van Ngoc
    // History:
    // 14/4/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Lớp dữ liệu cho hình TurnsPath
    /// </summary>
    [Serializable, XmlRoot("turnsPathInfo")]
    public class TurnsPathInfo : MotionPathInfo
    {
        public TurnsPathInfo() { }
        public override ShapeBase GetShape()
        {
            return new TurnsPath();
        }
    }
}
