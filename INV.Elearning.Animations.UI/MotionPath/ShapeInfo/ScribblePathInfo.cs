﻿using INV.Elearing.Controls.Shapes;
using INV.Elearing.Controls.Shapes.Helpers;
using System;
using System.Xml.Serialization;

namespace INV.Elearning.Animations.UI
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: ScribblePathInfo.cs
    // Description: Lớp dữ liệu cho đường ScribblePath
    // Develope by : Nguyen Van Ngoc
    // History:
    // 14/4/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Lớp dữ liệu cho đường ScribblePath
    /// </summary>
    [Serializable, XmlRoot("scribblePathInfo")]
    public class ScribblePathInfo : ePathInfo
    {
        public ScribblePathInfo() { }
        public ScribblePathInfo(string points) : base(points)
        {
        }

        public override ShapeBase GetShape()
        {
            ScribblePath path = new ScribblePath() { Points = Points.ToPointCollection() };
            path.CallDefiningGeometry();
            return path;
        }
    }
}
