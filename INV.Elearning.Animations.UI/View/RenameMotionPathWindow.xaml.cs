﻿using INV.Elearning.Controls;
using System.Windows;

namespace INV.Elearning.Animations.UI
{
    /// <summary>
    /// Interaction logic for RenameMotionPathWindow.xaml
    /// </summary>
    public partial class RenameMotionPathWindow : ElearningWindow
    {
        public RenameMotionPathWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
