﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Threading;
using INV.Elearning.Animations.UI.ViewModel;
using INV.Elearning.Controls;
using INV.Elearning.Core.AppCommands;
using INV.Elearning.Core.Helper;
using INV.Elearning.Core.View;

namespace INV.Elearning.Animations.UI.View
{
    /// <summary>
    /// Interaction logic for UCAnimations.xaml
    /// </summary>
    public partial class UCAnimations : RibbonTabItem
    {
        public static UCAnimations CurrentUCAnimations = null;
        static UCAnimations()
        {
            IsSelectedProperty.OverrideMetadata(typeof(UCAnimations), new FrameworkPropertyMetadata(false, IsSelectedPropertyChanged));
        }

        private static void IsSelectedPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue is bool boolean)
            {
                GenerationAnimationViewModel.MotionPathVisibleCommand.Execute(boolean);
            }
        }

        public UCAnimations()
        {
            CurrentUCAnimations = this;
            if (!Application.Current.Resources.Contains("ANIMATION_None"))
            {
                ResourceDictionary resource = new ResourceDictionary();
                resource.Source = new System.Uri("pack://application:,,,/INV.Elearning.Animations.UI;Component/Resources/Languages/en-US/Language.xaml");
                Application.Current.Resources.MergedDictionaries.Add(resource);
            }

            InitializeComponent();
            Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                if ((Application.Current as IAppGlobal).DocumentControl?.Slides is ObservableCollection<SlideBase> slides)
                {
                    slides.CollectionChanged += Slides_CollectionChanged;
                    foreach (SlideBase slide in slides)
                    {
                        FindInSlide(slide);
                    }
                }                
            }));
        }

        private void Slides_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (SlideBase slide in e.NewItems)
                {
                    FindInSlide(slide);
                }
            }
        }

        private void FindInSlide(SlideBase slide)
        {
            FindInLayer(slide.MainLayout);
            slide.Layouts.CollectionChanged += Layouts_CollectionChanged;
            foreach (LayoutBase layer in slide.Layouts)
            {
                FindInLayer(layer);
            }
        }

        private void Layouts_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (LayoutBase layer in e.NewItems)
                {
                    FindInLayer(layer);
                }
            }
        }

        private void FindInLayer(LayoutBase layer)
        {
            layer.Elements.CollectionChanged += Elements_CollectionChanged;
            foreach (ObjectElement item in layer.Elements)
            {
                FindInElement(item);
            }
        }

        private void Elements_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (ObjectElement item in e.NewItems)
                {
                    FindInElement(item);
                }
            }
            if (e.OldItems != null)
            {
                foreach (ObjectElement item in e.OldItems)
                {
                    item.Loaded += Item_Loaded;
                    if (item.MotionPaths != null)
                    {
                        foreach (MotionPathObject path in item.MotionPaths)
                        {
                            if (path.Parent is System.Windows.Controls.Panel panel)
                            {
                                panel.Children.Remove(path);
                            }
                        }
                    }
                }
            }
        }

        private void Item_Loaded(object sender, RoutedEventArgs e)
        {
            if (e.Source is ObjectElement objectElement && objectElement.MotionPaths != null)
            {
                foreach (MotionPathObject path in objectElement.MotionPaths)
                {
                    if (objectElement.Parent is System.Windows.Controls.Panel panel && panel.Children?.Contains(path) == false)
                    {
                        panel.Children.Add(path);
                    }
                }
            }
        }

        private void FindInElement(ObjectElement item)
        {
            item.LocationChanged -= Item_LocationChanged;
            item.LocationChanged += Item_LocationChanged;
            item.SizeChanged -= Item_SizeChanged;
            item.SizeChanged += Item_SizeChanged;
            if (item.MotionPaths != null)
            {
                item.MotionPaths.CollectionChanged += MotionPaths_CollectionChanged;
                foreach (MotionPathObject path in item.MotionPaths)
                {
                    path.PreviewMouseRightButtonDown += Item_PreviewMouseRightButtonDown;
                }
            }
        }

        private void Item_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Vector g1 = new Vector(Math.Round(e.PreviousSize.Width / 2, 0), Math.Round(e.PreviousSize.Height / 2, 0)),
                g2 = new Vector(Math.Round(e.NewSize.Width / 2, 0), Math.Round(e.NewSize.Height / 2, 0)),
                delta = g2 - g1;

            if (g1.X != 0 && g1.Y != 0)
            {
                MoveMotionPath(sender, delta);
            }
        }

        private void Item_LocationChanged(object sender, ObjectElementEventArg e)
        {
            if (e.PreviousValue is Point previousPoint && e.NewValue is Point newPoint)
            {
                Vector o1 = new Vector(Math.Round(previousPoint.X, 0), Math.Round(previousPoint.Y, 0)),
                    o2 = new Vector(Math.Round(newPoint.X, 0), Math.Round(newPoint.Y, 0)),
                    delta = o2 - o1;

                MoveMotionPath(sender, delta);
            }
        }

        private void MoveMotionPath(object sender, Vector delta)
        {
            if (sender is ObjectElement objectElement && objectElement.MotionPaths != null)
            {
                foreach (MotionPathObject item in objectElement.MotionPaths)
                {
                    if (item.Content is CustomPath path)
                    {
                        if (!path.GetIsLocked() && !item.IsSelected)
                        {
                            Global.BeginInit();
                            item.Left += delta.X;
                            item.Top += delta.Y;
                            Global.EndInit();
                        }
                    }
                }
            }
        }

        private void MotionPaths_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (MotionPathObject item in e.NewItems)
                {
                    item.PreviewMouseRightButtonDown += Item_PreviewMouseRightButtonDown;
                }
            }
            if (e.OldItems != null)
            {
                foreach (MotionPathObject item in e.OldItems)
                {
                    item.PreviewMouseRightButtonDown += Item_PreviewMouseRightButtonDown;
                }
            }
        }

        private void Item_PreviewMouseRightButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            ContextMenu menu = new ContextMenu();

            MenuItem cut = new MenuItem()
            {
                Icon = Application.Current.TryFindResource("CutIcon"),
                Header = Application.Current.TryFindResource("Cut"),
                Command = ObjectCommands.CutObjectCommand
            };

            MenuItem copy = new MenuItem()
            {
                Icon = Application.Current.TryFindResource("CopyIcon"),
                Header = Application.Current.TryFindResource("Copy"),
                Command = ObjectCommands.CopyObjectCommand
            };

            MenuItem paste = new MenuItem()
            {
                Icon = Application.Current.TryFindResource("PasteIcon"),
                Header = Application.Current.TryFindResource("Paste"),
                Command = ObjectCommands.PasteNormalObjectCommand
            };

            MenuItem rename = new MenuItem()
            {
                Header = "Rename",
                Command = GenerationAnimationViewModel.RenameCommand
            };

            MenuItem reversePathDirection = new MenuItem()
            {
                Header = "ReversePathDirection",
                Command = AnimationsViewModel.PathEffectCommand,
                CommandParameter = new EffectOption("Reverse Path Direction", Enums.eObjectAnimationOption.ReversePathDirection.ToString()) { Type = Enums.eObjectAnimationOption.ReversePathDirection, GroupName = "Path" }
            };

            MenuItem relativeStartPoint = new MenuItem()
            {
                Header = "RelativeStartPoint",
                Command = AnimationsViewModel.PathEffectCommand,
                CommandParameter = new EffectOption("Relative Start Point", Enums.eObjectAnimationOption.RelativeStartPoint.ToString()) { Type = Enums.eObjectAnimationOption.RelativeStartPoint, GroupName = "Path" }
            };

            MenuItem orientShapeToPath = new MenuItem()
            {
                Header = "OrientShapeToPath",
                Command = AnimationsViewModel.PathEffectCommand,
                CommandParameter = new EffectOption("Orient Shape To Path", Enums.eObjectAnimationOption.OrientShapeToPath.ToString()) { Type = Enums.eObjectAnimationOption.OrientShapeToPath, GroupName = "Path" }
            };

            menu.Items.Add(cut);
            menu.Items.Add(copy);
            menu.Items.Add(paste);
            menu.Items.Add(new System.Windows.Controls.Separator());
            menu.Items.Add(rename);
            menu.Items.Add(new System.Windows.Controls.Separator());
            menu.Items.Add(reversePathDirection);
            menu.Items.Add(relativeStartPoint);
            menu.Items.Add(orientShapeToPath);

            menu.IsOpen = true;
        }

        private void MenuItem_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            MenuItem mi = sender as MenuItem;
            mi.IsSubmenuOpen = true;
        }
        private void MenuItem_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            MenuItem mi = sender as MenuItem;
            mi.IsSubmenuOpen = false;
        }
        //Danh sách đối tượng đang được chọn
        public static ObservableCollection<ObjectElement> SelectedElement { get; set; }

        private void Duration_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SelectedElement = null;
            if ((Application.Current as IAppGlobal).SelectedElements?.Count > 0)
            {
                SelectedElement = new ObservableCollection<ObjectElement>();
                foreach (var item in (Application.Current as IAppGlobal).SelectedElements)
                {
                    SelectedElement.Add(item);
                }
            }


        }

        private void Spinner_LostKeyboardFocus(object sender, System.Windows.Input.KeyboardFocusChangedEventArgs e)
        {
            if (SelectedElement?.Count > 0)
            {
                if (!Double.IsNaN(spDurationEntrance.Value))
                {
                    TimeSpan duration = new TimeSpan(0, 0, 0, System.Convert.ToInt32(Math.Min(59.0, (double)(spDurationEntrance.Value - 0.5 + 0.00001))), (int)(((double)spDurationEntrance.Value * 1000) % 1000));
                    foreach (var item in SelectedElement)
                    {
                        if (item is MotionPathObject)
                        {
                            if ((item as MotionPathObject).Owner is ObjectElement objectElement)
                            {
                                ((item as MotionPathObject).Owner as ObjectElement).EntranceAnimation.Duration = new TimeSpan();
                                ((item as MotionPathObject).Owner as ObjectElement).EntranceAnimation.Duration = duration;
                            }
                        }
                        else if (item is ObjectElement objectElement)
                        {
                            (item as ObjectElement).EntranceAnimation.Duration = new TimeSpan();
                            (item as ObjectElement).EntranceAnimation.Duration = duration;

                        }
                    }

                    (AnimationsViewModel.AnimationsProxy.ProxyAnimation as Animation).Duration = duration;
                }
                if ((Application.Current as IAppGlobal).SelectedElements?.Count > 0)
                {
                    AnimationsViewModel.AnimationsProxy._durationEntranceSelected = spDurationEntrance.Value;
                }
            }
        }

        private void SpinnerExit_LostKeyboardFocus(object sender, System.Windows.Input.KeyboardFocusChangedEventArgs e)
        {
            if (SelectedElement?.Count > 0)
            {
                if (!Double.IsNaN(spDurationExit.Value))
                {

                    foreach (var item in SelectedElement)
                    {
                        if (item is MotionPathObject)
                        {
                            if ((item as MotionPathObject).Owner is ObjectElement objectElement)
                            {
                                ((item as MotionPathObject).Owner as ObjectElement).ExitAnimation.Duration = new TimeSpan();
                                TimeSpan duration = new TimeSpan(0, 0, 0, System.Convert.ToInt32(Math.Min(59.0, (double)(spDurationExit.Value - 0.5 + 0.0001))), System.Convert.ToInt32(((double)spDurationExit.Value * 1000) % 1000));

                                ((item as MotionPathObject).Owner as ObjectElement).ExitAnimation.Duration = duration;
                            }
                        }
                        else if (item is ObjectElement objectElement)
                        {
                            (item as ObjectElement).ExitAnimation.Duration = new TimeSpan();
                            TimeSpan duration = new TimeSpan(0, 0, 0, System.Convert.ToInt32(Math.Min(59.0, (double)(spDurationExit.Value - 0.5 + 0.0001))), System.Convert.ToInt32(((double)spDurationExit.Value * 1000) % 1000));
                            (item as ObjectElement).ExitAnimation.Duration = duration;

                        }
                    }
                    (AnimationsViewModel.AnimationsProxy.ProxyAnimationExit as Animation).Duration = new TimeSpan(0, 0, 0, System.Convert.ToInt32(Math.Min(59.0, (double)(spDurationExit.Value - 0.5 + 0.0001))), System.Convert.ToInt32(((double)spDurationExit.Value * 1000) % 1000));

                }
                if ((Application.Current as IAppGlobal).SelectedElements?.Count > 0)
                {
                    AnimationsViewModel.AnimationsProxy._durationExitSelected = spDurationExit.Value;
                }
            }
        }

        private void SpinnerPath_LostKeyboardFocus(object sender, System.Windows.Input.KeyboardFocusChangedEventArgs e)
        {
            if (SelectedElement?.Count > 0)
            {
                if (!Double.IsNaN(spDurationPath.Value))
                {
                    foreach (var item in (Application.Current as IAppGlobal).SelectedElements)
                    {
                        if (item is MotionPathObject motionPathObject)
                        {

                            TimeSpan duration = new TimeSpan(0, 0, 0, System.Convert.ToInt32(Math.Min(59.0, (double)(spDurationPath.Value - 0.5 + 0.0001))), System.Convert.ToInt32(((double)spDurationPath.Value * 1000) % 1000));

                            (item as MotionPathObject).Animation.Duration = duration;

                        }
                        else if (item is ObjectElement)
                        {
                            for (int k = 0; k < (item as ObjectElement).MotionPaths.Count; k++)
                            {

                                TimeSpan duration = new TimeSpan(0, 0, 0, System.Convert.ToInt32(Math.Min(59.0, (double)(spDurationPath.Value - 0.5 + 0.0001))), System.Convert.ToInt32(((double)spDurationPath.Value * 1000) % 1000));

                                (item as ObjectElement).MotionPaths[k].Animation.Duration = duration;
                            }
                        }
                    }
                    (AnimationsViewModel.AnimationsProxy.ProxyAnimationPath as Animation).Duration = new TimeSpan(0, 0, 0, System.Convert.ToInt32(Math.Min(59.0, (double)(spDurationPath.Value - 0.5 + 0.0001))), System.Convert.ToInt32(((double)spDurationPath.Value * 1000) % 1000));

                }
                if ((Application.Current as IAppGlobal).SelectedElements?.Count > 0)
                {
                    AnimationsViewModel.AnimationsProxy._durationPathSelected = spDurationPath.Value;
                }
            }

        }


    }



}
